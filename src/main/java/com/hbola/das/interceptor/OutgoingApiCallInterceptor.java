package com.hbola.das.interceptor;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

import com.hbola.das.service.TokenServiceI;


//this is for outgoing api calls ie calls to BNC-API 
@ComponentScan
public class OutgoingApiCallInterceptor implements ClientHttpRequestInterceptor {

	@Autowired 
	private TokenServiceI tokenService;
	
	private static final Logger logger = LogManager.getLogger(OutgoingApiCallInterceptor.class);
	
	@Override
	public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution) throws IOException {
	  
		ClientHttpResponse response = null;
		try {
			
			String token = tokenService.generateToken("das-api", "bnc-caller");
			request.getHeaders().add("Authorization", token);			
	        response = execution.execute(request, body);	        
		}
		catch (Exception e) {
			logger.error("Problem on out going api call interceptor: " + e.getMessage());
			response = execution.execute(request, body);
		}
	        
		return response;
	}
	
}
