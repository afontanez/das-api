

package com.hbola.das.interceptor;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.hbola.das.service.TokenServiceI;

//this is for incoming api calls to the DAS-API including controller calls
@ComponentScan
public class IncomingApiCallInterceptor extends HandlerInterceptorAdapter {

	@Autowired 
	private TokenServiceI tokenService;
	
	@SuppressWarnings("serial")
	private List<String> freePass = new ArrayList<String>(){{add("user_post");}};
	
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {	
		
		try{
			
			String token = request.getHeader("Authorization");
			String route = request.getRequestURI().toLowerCase();
			String method = request.getMethod().toLowerCase();
	        if( isFreePass(route, method) || tokenService.verify(token)) {
	        	response.setStatus(HttpStatus.I_AM_A_TEAPOT.value());	        	
	        	response.setHeader("Authorization", tokenService.generateToken("DAS-API", "Extended-Token"));	        	
	            return true;
	        }
	        else {
	            response.setStatus(HttpStatus.UNAUTHORIZED.value());
	            return false;
	        }
		}catch(Exception e){
            response.setStatus(HttpStatus.BAD_REQUEST.value());
		}
		return false;
	}
	
	private boolean isFreePass(String route, String method){
		if(route.contains("login") || route.contains("logout")) // || route.contains("token/refresh"))
			return true;
		
		return freePass.contains(getRoutePlusMethod(route, method));
	}
	
	private String getRoutePlusMethod(String route, String method){
		String[] routes = route.split("/");
		if(routes.length > 1){
			String newRoute = routes[2];
			return newRoute+"_"+method;
		}
		return "";
	}
	
}
