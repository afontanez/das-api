package com.hbola.das.service;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hbola.das.dal.CableOperatorDALI;
import com.hbola.das.dto.CableOperatorDTO;
import com.hbola.das.dto.HeadendDTO;
@Service
public class CableOperatorServiceImpl implements CableOperatorServiceI{


	private static final Logger logger = LogManager.getLogger(CableOperatorServiceImpl.class);
	
	@Autowired
	private CableOperatorDALI cableOperatorDal;
	
	@Autowired
	private HeadendServiceI headendService;
	
	@Override
	public void completeCableOperatorWithHeadends(List<CableOperatorDTO> cableOperators) throws Exception {

		List<HeadendDTO> headends = headendService.getList();
		cableOperators.forEach(co -> {	
			headends.forEach(h -> {			
				if(co.getCode().equals(h.getCableCode()))
				{
					co.getHeadEnds().add(h);
					//headends.remove(h);
				}
			});
		});
	}
	
	@Override
	public int create(CableOperatorDTO co) throws Exception {
		try {
			return cableOperatorDal.create(co);
		}
		catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	@Override
	public int edit(CableOperatorDTO co) throws Exception {
		try {
			return cableOperatorDal.edit(co);
		}
		catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	@Override
	public List<CableOperatorDTO> getList() throws Exception {
		try {
			List<CableOperatorDTO> cos =  cableOperatorDal.getList();
			this.completeCableOperatorWithHeadends(cos);			
			return cos;
		}
		catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	@Override
	public CableOperatorDTO getOneByCode(String code) throws Exception{
		try {
			CableOperatorDTO co = cableOperatorDal.getOneByCode(code);
			co.setHeadEnds(headendService.getListByCableCode(code));	
			return co;
		}
		catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

}
