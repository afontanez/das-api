package com.hbola.das.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.hbola.das.dto.CableOperatorDTO;

@Service
public interface CableOperatorServiceI {

	public void completeCableOperatorWithHeadends(List<CableOperatorDTO> cableOperators) throws Exception;

	public int create(CableOperatorDTO co) throws Exception;

	public int edit(CableOperatorDTO co) throws Exception;

	public List<CableOperatorDTO> getList() throws Exception;

	public CableOperatorDTO getOneByCode(String code) throws Exception;
}
