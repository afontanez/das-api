package com.hbola.das.service;

import org.springframework.web.multipart.MultipartFile;

public interface InventoryServiceI {

    String PALLET_ID = "palletId";
    String CARTON_ID = "cartonId";
    String ITEM_NUMBER = "itemNumber";
    String DESCRIPTION = "description";
    String SERIAL_NUMBER = "serialNumber";
    String CUSTOMER_PO = "customerPO";
    String SALES_ORDER = "salesOrder";
    String DELIVERY_ID = "deliveryId";
    String ORDER_LINE_ID = "orderLineId";
    String SHIP_ADDRESS = "shipAddress";
    String MANUFACTURE_DATE = "manufactureDate";
    String UNIT_ADDRESS = "unitAddress";
	
	int process(MultipartFile file, String user) throws Exception;
	
}
