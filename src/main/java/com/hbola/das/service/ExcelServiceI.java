package com.hbola.das.service;

import java.io.InputStream;
import java.util.List;
import java.util.Properties;

public interface ExcelServiceI {

	List<Properties> extract(InputStream excelFile) throws Exception;

}