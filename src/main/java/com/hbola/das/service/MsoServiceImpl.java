package com.hbola.das.service;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hbola.das.dal.MsoDALI;
import com.hbola.das.dto.MsoDTO;

@Service
public class MsoServiceImpl implements MsoServiceI {

	private static final Logger logger = LogManager.getLogger(MsoServiceImpl.class);
	@Autowired 
	private MsoDALI msoDal;
	
	@Override
	public int create(MsoDTO mso) throws Exception {
		try {
			return msoDal.create(mso);
		}
		catch (Exception e) {
			logger.warn("Problem creating MSO: " + e.getMessage());
			throw e;//propogate
		}
	}
	
	@Override
	public int edit(MsoDTO mso) throws Exception {
		try {
			return msoDal.edit(mso);
		}
		catch (Exception e) {
			logger.warn("Problem updating MSO: " + e.getMessage());
			throw e;//propogate
		}
	}

	@Override
	public MsoDTO getOneByCode(String code) throws Exception {
		try {
			return msoDal.getOneByCode(code);
		}
		catch (Exception e) {
			logger.warn("Problem getting one MSO: " + e.getMessage());
			throw e;//propogate
		}
	}

	@Override
	public List<MsoDTO> getList() throws Exception {
		try {
			return msoDal.getList();
		}
		catch (Exception e) {
			logger.warn("Problem getting MSO: " + e.getMessage());
			throw e;//propogate
		}
	}

}
