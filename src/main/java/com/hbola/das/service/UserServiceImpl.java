package com.hbola.das.service;

import javax.naming.AuthenticationException;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.xml.bind.PropertyException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hbola.das.config.ConfigCache;
import com.hbola.das.dal.UserDALI;
import com.hbola.das.dto.UserDTO;
import com.hbola.das.dto.UserLoginDTO;
import com.hbola.das.helper.LDAPHelper;
import com.hbola.das.helper.TextHelper;

@Service
public class UserServiceImpl implements UserServiceI{

	private static final Logger logger = LogManager.getLogger(UserServiceImpl.class);

	@Autowired
	private TokenServiceI tokenService;
	
	@Autowired
	private UserDALI userDAL;
	
	@Override
	public UserLoginDTO login(UserLoginDTO login) throws Exception{
        DirContext context = null;
        try {
        	
        	UserDTO user = userDAL.getUserByUsername(login.getUsername());
        	if(user == null) throw new Exception("Database authentication failed: User not found!");
        	
        	String userRole = userDAL.getRoleByUsername(login.getUsername());
        	if(userRole == null || userRole.isEmpty()) throw new Exception("Database authentication failed: User has not a role!");        	
        	login.setRoleName(userRole);

        	String password = TextHelper.decryptAES(login.getPassword().getBytes(), ConfigCache.getSaltPassword());
        	
    		context = LDAPHelper.checkCredential(login.getUsername(), password);
    		
    		login.setPassword(null);//user is authenticated. login page only needs username and rolename at this point. token is sent in header via interceptor: OutgoingApiCallIntercepter
    		
    		//these 2 lines are needed for the old UI - the new ui gets it from a header. remove this code once old ui is a distant memeory
    		String token = tokenService.generateToken(login.getUsername(), login.getRoleName());
    		login.setToken(token);
    		
        } catch (AuthenticationException a) {
        	String messageException = "Authentication failed: " + a.getMessage();
            logger.error(messageException);
            throw new AuthenticationException(messageException);
        } catch (NamingException e) {
        	String messageException = "Failed to bind to LDAP / get account information: " + e.getMessage();
            logger.error(messageException);
            throw new NamingException(messageException);
        }
        catch (PropertyException e){
        	String messageException = "Property LDAP issue: " + e.getMessage();
            logger.error(messageException);
            throw new PropertyException(messageException);
        } catch (Exception e) {            
        	String messageException = "General exception: " + e.getMessage();
            logger.error(messageException);
            throw new Exception(messageException);
		}
        finally{
        	if(context != null)
				try {
					context.close();
				} catch (NamingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
        }
        
		return login;
	}

	@Override
	public String getRoleByUsername(String username) throws Exception{
        try {
        	String userRole = userDAL.getRoleByUsername(username);
        	if(userRole == null)
                throw new Exception("UserRole null");
        	return userRole;
        }catch (Exception e){
        	String messageException = "Get rol exception: " + e.getMessage();
            logger.error(messageException);
            throw new Exception(messageException);
        }
	}
}
