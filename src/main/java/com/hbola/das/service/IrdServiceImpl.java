package com.hbola.das.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.MultiValuedMap;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.hbola.das.dal.IrdDALI;
import com.hbola.das.dto.ApiResponse;
import com.hbola.das.dto.FatTierDTO;
import com.hbola.das.dto.IrdEditDTO;
import com.hbola.das.dto.IrdSummaryDTO;
import com.hbola.das.dto.NewRequestDecodersFilteredDTO;
import com.hbola.das.dto.OperationDTO;
import com.hbola.das.dto.OwnershipDTO;
import com.hbola.das.dto.RequestDecodersFiltered;
import com.hbola.das.dto.ServiceDTO;
import com.hbola.das.dto.TranscoderDTO;
import com.hbola.das.utils.StringUtils;

@Service
public class IrdServiceImpl implements IrdServiceI {
	

	private static final Logger logger = LogManager.getLogger(IrdServiceImpl.class);
	
	@Autowired
	private IrdDALI irdDal;
		
	@Autowired
	private BNCServiceI bncService;
	
	@Autowired
	CheckSumServiceI checkSumService;
	
	private static final Gson GSON = new GsonBuilder().disableHtmlEscaping().create();
	
	@Override
	public List<IrdSummaryDTO> readIrdsFromDB(RequestDecodersFiltered filter)throws Exception {

		List<IrdSummaryDTO> irds = new ArrayList<IrdSummaryDTO>();
		try {
			if (filter != null) {
				
				if (filter.getEndDate() == null) {
					filter.setEndDate(new Date());
				}
				
				Map<String, Object> map = irdDal.getIrds(filter);
				
				irds = addTranscodersToIrdsWithUnitaddresses(map);
				
			}
			else {
				throw new Exception("No filter when searching");
			}
		}
		catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
		return irds;
	}
	
	//public for unit tests
	@Override
	public List<IrdSummaryDTO> addTranscodersToIrdsWithUnitaddresses(Map<String, Object> map) {
		
		List<IrdSummaryDTO> irds = new ArrayList<IrdSummaryDTO>();
		try {
			if (map != null && !map.isEmpty()) {
				
				irds = (List<IrdSummaryDTO>) map.get("irds");
				MultiValuedMap<String, TranscoderDTO> transcodersMap = (MultiValuedMap<String, TranscoderDTO>) map.get("transcodersMap");
				
				for (int i = 0; i < irds.size(); i++) { 	
					
					List<TranscoderDTO> tdtoList = (List<TranscoderDTO>) transcodersMap.get(Integer.toString(irds.get(i).getDecoderId()));
					String operationsString = "";
					String authorizedString = "";
					
					for (int j = 0; j < tdtoList.size(); j++){
						
						irds.get(i).getServices().add(tdtoList.get(j).getTranscoderId() + "-" + tdtoList.get(j).getService().getTierName());
						irds.get(i).getOperationsList().add(tdtoList.get(j).getOperation().getDescription());
						
						//irds.get(i).getServices().put(tdtoList.get(j).getTranscoderId() + "-" + tdtoList.get(j).getService().getTierName(), tdtoList.get(j).getOperation().getDescription());						
						irds.get(i).getTranscoders().add(tdtoList.get(j));
						
						operationsString += "~" + tdtoList.get(j).getOperation().getDescription();
						authorizedString += "~" + tdtoList.get(j).getAuthorized();
					}
					irds.get(i).setOperations(operationsString + "~");
					irds.get(i).setAuthorizations(authorizedString + "~");
					
					transcodersMap.remove(Integer.toString(irds.get(i).getDecoderId()));
					
					String unitAddress = verifyUnitAddress(irds.get(i).getUnitAddress().get(0));
					List<String> unitaddresses = new ArrayList<String>();
					unitaddresses.add(unitAddress);
					unitaddresses.add(null);
					irds.get(i).setUnitAddress(unitaddresses);
					
				}
			}
		}
		catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
		
		return irds;
	}
	
	private void nullHDSDPassThru(List<TranscoderDTO> transcoders) {
		
		for (TranscoderDTO dto : transcoders) {
			
			dto.setHdEnabled(null);
			dto.setSdEnabled(null);
			dto.setPassthruEnabled(null);			
		}
	}
	
	private void nullPassThru(List<TranscoderDTO> transcoders) {
		
		for (TranscoderDTO dto : transcoders) {			
			dto.setPassthruEnabled(null);			
		}
	}
	
	@Override
	public int createIrd(IrdEditDTO ird) throws Exception {
				
		try {			
			switch (ird.getModelName()) {
				case "DSR-4201" :
				case "DSR-4410MD" :
				{
					ird.setTransportStreamOutput(null);
					ird.setFrontPanelEnabled(null);
				
					nullHDSDPassThru(ird.getTranscoders());
					
					ird.setPresetId(0);
					ird.setSecondaryUnitAddress(null);
					
					if (ird.getUnitAddress() == null || ird.getUnitAddress().isEmpty()) {
						
						//if no anchor UA then set anchor to the same as acp 0
						if (ird.getTranscoders() != null && !ird.getTranscoders().isEmpty()) {
							ird.setUnitAddress(ird.getTranscoders().get(0).getUnitAddress());
						}
						else {
							throw new Exception("Problem creating 4410MD missing Anchor UA");
						}
					}
					
					break;					
				}
				case "DSR-4460" :
				case "DSR-6050" :
				case "DSR-6300" :
				{
					ird.setTransportStreamOutput(ird.getTransportStreamOutput()==null ? 0:ird.getTransportStreamOutput());
					ird.setFrontPanelEnabled(ird.getFrontPanelEnabled()==null ? false : (ird.getFrontPanelEnabled()?true:false));
					
					nullHDSDPassThru(ird.getTranscoders());
					
					ird.setPresetId(0);
					ird.setSecondaryUnitAddress(null);
					break;
				}				
				case "DSR-6401" :
				case "DSR-6402" :
				case "DSR-6403" :
				case "DSR-6404" :
				{
					ird.setTransportStreamOutput(ird.getTransportStreamOutput()==null ? 0:ird.getTransportStreamOutput());
					ird.setFrontPanelEnabled(ird.getFrontPanelEnabled()==null ? false : (ird.getFrontPanelEnabled()?true:false));
					
					nullPassThru(ird.getTranscoders());
					
					ird.setPresetId(0);
					ird.setSecondaryUnitAddress(null);
					break;					
				}																				
				case "DSR-4470" : 
				{
					ird.setTransportStreamOutput(null);
					ird.setFrontPanelEnabled(null);

					ird.setSecondaryUnitAddress(null);
					break;
				}
				case "DSR-7401" :
				case "DSR-7403" :
				case "DSR-7406" :
				case "DSR-7409" :
				case "DSR-7412" :
				{
					ird.setTransportStreamOutput(null);
					ird.setFrontPanelEnabled(null);
					break;
				}				
			}
				
			if (!ird.getModelName().equalsIgnoreCase("DSR-6300") && !ird.getModelName().equalsIgnoreCase("DSR-4460") && !ird.getModelName().equalsIgnoreCase("DSR-4201") &&  !ird.getModelName().equalsIgnoreCase("DSR-4410MD")) {//6300 use templates and ignore vchNumber and vchTable
				for (TranscoderDTO transcoderDTO : ird.getTranscoders()) {
					if (transcoderDTO.getService() != null) {//if null then this transcoder was left empty
						if (!transcoderDTO.getService().getTierNumber().equalsIgnoreCase("-1") && (transcoderDTO.getService().getVchNumber() == null || transcoderDTO.getService().getVchTable() == null)) {
							throw new Exception("Transcoder " + transcoderDTO.getTranscoderId() + " service is not setup correctly in the database. vchNumber: " + transcoderDTO.getService().getVchNumber() + " vchTable: " + transcoderDTO.getService().getVchTable());
						}
					}
				}
			}
			
			//get the encoder id from the first non blackout service. if no service found then the default is the decoder level encoder id which is the case for inventory.
			for (TranscoderDTO dto : ird.getTranscoders()) {					
				if (dto.getService() != null && dto.getService().getEncoderId() > -1) { //-1 is blackout
					ird.setEncoderId(dto.getService().getEncoderId());
				}					
			}
			
			irdDal.checkCableCodeHeadend(ird);
			
			int decoderId = 0;
			if (ird.getModelName().equalsIgnoreCase("DSR-4410MD")) {
				List<IrdEditDTO> irds = setupMDs(ird);				
				irdDal.saveMD(irds);
			}
			else {
				decoderId = irdDal.saveIrd(ird);
			}
			
			return decoderId;
		}
		catch (Exception e) {
			logger.warn("Problem creating IRD: " + e.getMessage());
			throw e;//propogate
		}
	}

	private List<IrdEditDTO> setupMDs(IrdEditDTO irdInput) {
		
		List<IrdEditDTO> irds = new ArrayList<IrdEditDTO>();
		
		if (irdInput.getSerialNumber() == null) {
			if (irdInput.getUnitAddress() != null) {
				irdInput.setSerialNumber(irdInput.getUnitAddress().replaceAll("-", ""));  
			}
		}

		if (irdInput.isInventory()) {
			IrdEditDTO ird = new IrdEditDTO(irdInput.getLoggedInUser(), 
					irdInput.getModelName(), irdInput.getSerialNumber(), irdInput.getUnitAddress(), 
					irdInput.getHeadendCode(), irdInput.getCableOperatorCode(), 
					irdInput.getEncoderId(), irdInput.isInventory(), irdInput.getComments());
						
			ird.setFirmwareVersion(irdInput.getFirmwareVersion());
			ird.setIRTUnitAddress(StringUtils.truncateUA(irdInput.getUnitAddress()));//needed by BNC
			ird.setIRTSerialNumber(irdInput.getSerialNumber());//needed by BNC
			
			ird.setId(irdInput.getId());
			
			irds.add(ird);			
		}
		else {
			irdInput.getTranscoders().forEach(tInput -> {
				
				IrdEditDTO ird = new IrdEditDTO(irdInput.getLoggedInUser(), 
						irdInput.getModelName(), tInput.getSerialNumber(), tInput.getUnitAddress(), 
						irdInput.getHeadendCode(), irdInput.getCableOperatorCode(), 
						irdInput.getEncoderId(), irdInput.isInventory(), irdInput.getComments());
				
				ird.setId(tInput.getDecoderId());
				ird.setFirmwareVersion(irdInput.getFirmwareVersion());
				ird.setIRTUnitAddress(StringUtils.truncateUA(irdInput.getUnitAddress()));//needed by BNC
				ird.setIRTSerialNumber(irdInput.getSerialNumber());//needed by BNC
				
				TranscoderDTO t = new TranscoderDTO(tInput.getDecoderId(), 1, tInput.getService(), tInput.getComments(), 
						tInput.getOperation(), tInput.isHdEnabled(), tInput.isSdEnabled(), tInput.isPassthruEnabled(),
						tInput.getDecoderServicesIdentity(), tInput.getAuthorized(), tInput.getUnitAddress(), tInput.getIsUpdated()!=null ? tInput.getIsUpdated() : false);
				
				List<TranscoderDTO> tList = new ArrayList<TranscoderDTO>();
				tList.add(t);
				ird.setTranscoders(tList);
				
				irds.add(ird);
			});
		}		
		return irds;
	}
	
	@Override
	public void tripIrd(String modelId, String unitAddress) throws Exception {
				
		try {
			
			if (!modelId.equalsIgnoreCase("DSR-4410MD")) {			
				bncService.tripIRDOnBNC(modelId, unitAddress);
			}
			else {	
				String truncatedUA = StringUtils.truncateUA(unitAddress);
				List<String> uas = irdDal.getACPsForIRT(truncatedUA);
				for (String ua : uas) {					
					bncService.tripIRDOnBNC(modelId, ua);					
				}
				
			}
		}
		catch (Exception e) {
			logger.warn("Problem tripping IRD: " + e.getMessage());
			throw e;//propogate
		}
	}
	
	@Override
	public IrdEditDTO readIrdById(String modelId, Integer id)throws Exception  {
		IrdEditDTO ird = null;
		
		try {
			
			if (!modelId.equalsIgnoreCase("DSR-4410MD")) {
				
				ird = irdDal.getIrd(id);
				
				String unitAddress = verifyUnitAddress(ird.getUnitAddress());
				ird.setUnitAddress(unitAddress);
				
				String secondaryUnitAddress = verifyUnitAddress(ird.getSecondaryUnitAddress());
				ird.setSecondaryUnitAddress(secondaryUnitAddress);
				
				ird.getTranscoders().forEach(transcoderDTO -> {
					if (transcoderDTO.getService() != null && (transcoderDTO.getService().getTierName() == null || transcoderDTO.getService().getTierName().isEmpty())) {
						String tierName = irdDal.getTierName(transcoderDTO.getService().getTierNumber());
						transcoderDTO.getService().setTierName(tierName);
					}					
				});
				
			}
			else {
				
				ird = irdDal.getMD(id);
				
				String unitAddress = verifyUnitAddress(ird.getUnitAddress());
				ird.setUnitAddress(unitAddress);
								
				List<TranscoderDTO> transcoders = this.setupMDUAsForMigrated(ird);
				transcoders = this.processACPsWithoutServices(ird.getEncoderId(), transcoders);
				ird.setTranscoders(transcoders);
			}			

			
			List<TranscoderDTO> transcoderList = ird.getTranscoders();
			List<TranscoderDTO> transcoderBlackedOutList = insertTranscoderBlackouts(transcoderList);
			ird.setTranscoders(transcoderBlackedOutList);
		}
		catch (Exception e) {
			logger.warn("Problem creating IRD: " + e.getMessage());
			throw e;//propogate
		}
		return ird;
	}

	@Override
	//public for unit testing. An ACP without a service is the same as a blackout except it has a decoderid, unitaddress, and serialnumber 
	public List<TranscoderDTO> processACPsWithoutServices(Integer encoderId, List<TranscoderDTO> transcoders) {
				
		if (transcoders != null && !transcoders.isEmpty()) {
			
			for (int i = 0; i < transcoders.size(); i++) {
				
				if (transcoders.get(i).getDecoderServicesIdentity() == 0) {
					
					ServiceDTO serviceDTO = new ServiceDTO(encoderId, null, "-1", "Blackout", "0", 0);
					transcoders.get(i).setService(serviceDTO);
										
					OperationDTO operationDTO = new OperationDTO(0, "Desconexion de Señal", false, null, null);
					transcoders.get(i).setOperation(operationDTO);

					transcoders.get(i).setComments("Blackout");
					transcoders.get(i).setHdEnabled(false);
					transcoders.get(i).setSdEnabled(false);
					transcoders.get(i).setPassthruEnabled(false);
					transcoders.get(i).setAuthorized(false);					
				}
				
			}
			
		}
		
		return transcoders;
		
	}
	
	private String verifyUnitAddress(String unitAddress) {
		
		String returnValue = unitAddress;
		if (unitAddress != null && unitAddress.length() < 16) {
			String checkSum = checkSumService.crc8(unitAddress);
			returnValue = unitAddress + checkSum; 			
		}
		
		return returnValue;		
	}
	
	
	
	@Override
	public List<OwnershipDTO> getIrdSetupOwnerships() throws Exception {
		List<OwnershipDTO> ownerships = new ArrayList<OwnershipDTO>();
		try {
			ownerships = irdDal.getOwnerships();
		}
		catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
		return ownerships;
	}
	
	private String[] setupMDUAsNoFormat(String anchorUA) {
		String[] uAs = setupMDUAs(anchorUA);
		
		for (int i = 0; i < uAs.length; i++) {
			uAs[i] = uAs[i].replaceAll("-", "");
		}
		return uAs;
	}
	
	@Override
	//given the 4410MD anchor UA this method creates the other 15 UAs
	public String[] setupMDUAs(String anchorUA) {//anchorUA format -> 000-11415-97424-123
	
		String[] uas = new String[0];
		if (anchorUA != null && !anchorUA.isEmpty()) {
			try {
				
				//first convert to number
				String anchorUANoFormat = anchorUA.replaceAll("-", "");
				anchorUANoFormat = anchorUANoFormat.substring(3);
				
				if (anchorUANoFormat.length() > 10) {//BNC only requires 13 digits andything greater are for financial reasons and should be excluded
					anchorUANoFormat = anchorUANoFormat.substring(0, anchorUANoFormat.length()-3); 
				}
				
				long anchorUALong = Long.parseLong(anchorUANoFormat);
				
				uas = new String[16];
				for (int i = 0; i < 16; i++) {//16 acps for a 4410MD
					
					long ua = anchorUALong + i;// * 1000;//we want to the change the value in the 1000s place by 1
					
					String uaString = String.valueOf(ua);
					
					//it has to be 10 chars at this point so prepend 0 until we hit 13
					while (uaString.length() < 10) {
						uaString = "0" + uaString;
					}
					
					uaString = uaString.substring(0, 5) + "-" + uaString.substring(5, 10) + "-" + checkSumService.crc8(uaString);
					
					//put the final touches
					uaString = "000-" + uaString;
					
					uas[i] = uaString;
					
				}
				
			}
			catch (Exception e) {
				uas = new String[0];
			}
		}
		
		return uas;
	}
	
	@Override
	public List<TranscoderDTO> setupMDUAsForMigrated(IrdEditDTO dto) {//migrated decoders may have less than 16 transcoders in that case we automatically set the remainder UAs here

		List<TranscoderDTO> transcoders = new ArrayList<TranscoderDTO>();		
		if (dto != null) {
			
			if (dto.getTranscoders() != null && !dto.getTranscoders().isEmpty()) {
			
				TranscoderDTO transcoderDTO = dto.getTranscoders().get(0);
				if (transcoderDTO != null) {
					
					String anchorUA = dto.getIRTUnitAddress();
					if (anchorUA == null || anchorUA.isEmpty()) anchorUA = dto.getUnitAddress();
					if (anchorUA != null && !anchorUA.isEmpty()) {
								
						anchorUA = anchorUA.replace("-", "");
						if (anchorUA.length() > 13) { 
							anchorUA = anchorUA.substring(0, 13);
						}
						
						String[] generatedUAs = this.setupMDUAsNoFormat(anchorUA);//this.setupMDUAs(anchorUA);
						int i = 0;
						for (int y = 0; y < generatedUAs.length; y++) 
						{
							if (i < dto.getTranscoders().size()) {//just use the acp that is already there
								
								transcoderDTO = dto.getTranscoders().get(i);
								String acpUA = transcoderDTO.getUnitAddress();
								String checkSumAcpUa = acpUA;
								if (acpUA != null && acpUA.length() <= 13) {
									checkSumAcpUa = acpUA + checkSumService.crc8(acpUA);									
								}

								if (checkSumAcpUa != null && !checkSumAcpUa.isEmpty() && generatedUAs[y].equals(checkSumAcpUa)) {
									
									transcoderDTO.setUnitAddress(checkSumAcpUa);
									if (transcoderDTO.getService() != null && (transcoderDTO.getService().getTierName() == null || transcoderDTO.getService().getTierName().isEmpty())) {
										String tierName = irdDal.getTierName(transcoderDTO.getService().getTierNumber());
										transcoderDTO.getService().setTierName(tierName);
									}
										
									transcoders.add(transcoderDTO);									

									i++;
								}
								else {//create new acp with generated UA
									
									transcoderDTO = new TranscoderDTO();
									transcoderDTO.setUnitAddress(generatedUAs[y]);
									//transcoderDTO.setSerialNumber(generatedUAs[i].replaceAll("-", ""));
										
									transcoderDTO.setDecoderServicesIdentity(0);
									transcoderDTO = addTranscoderDefaults(dto.getEncoderId(), transcoderDTO);
									transcoders.add(transcoderDTO);
								}
							}
							else {//just appened any generatedUAs that are left
								transcoderDTO = new TranscoderDTO();
								transcoderDTO.setUnitAddress(generatedUAs[y]);
									
								transcoderDTO.setDecoderServicesIdentity(0);
								transcoderDTO = addTranscoderDefaults(dto.getEncoderId(), transcoderDTO);
								transcoders.add(transcoderDTO);
							}							
						}
					}
				}
			}
		}
		
		return transcoders;		
	}
	
	
	private TranscoderDTO addTranscoderDefaults(Integer encoderId, TranscoderDTO transcoderDTO) {
		
		try {
			ServiceDTO service = new ServiceDTO(encoderId, "-1", "Blackout", null, 0, null, null);
			transcoderDTO.setService(service);
			
			OperationDTO operation = new OperationDTO(-1, "Blackout", false, null, null);
			transcoderDTO.setOperation(operation);
			
			transcoderDTO.setHdEnabled(false);
			transcoderDTO.setSdEnabled(false);
			transcoderDTO.setPassthruEnabled(false);
			transcoderDTO.setAuthorized(false);
			transcoderDTO.setComments("");
		}
		catch (Exception e) {			
		}
		
		return transcoderDTO;
	}
	
	@Override
	public List<TranscoderDTO> insertTranscoderBlackouts(List<TranscoderDTO> transcoderList) {
		
		if (transcoderList == null || transcoderList.isEmpty()) return new ArrayList<TranscoderDTO>();
		
		try {
			TranscoderDTO dto = transcoderList.get(transcoderList.size()-1);
			int numLoops = dto.getTranscoderId();
			
			for (int i = 0; i < numLoops; i++) {
				
				dto = transcoderList.get(i);
				int transcoderId = dto.getTranscoderId();
				
				if (transcoderId != i + 1) {					
					transcoderList.add(i, getBlackoutTranscoder(i + 1));
				}
			}
		}
		catch (Exception e) {
		}
		
		return transcoderList;
	}
	
	//public for junit
	@Override
	public TranscoderDTO getBlackoutTranscoder(int transcoderId) {
				
		ServiceDTO serviceDTO = new ServiceDTO(1, null, "-1", "Blackout", "0", 0);
		OperationDTO operationDTO = new OperationDTO(0, "Desconexion de Señal", false, null, null);

		TranscoderDTO returnDTO = new TranscoderDTO(transcoderId, serviceDTO, "Blackout", operationDTO, false, false, false, 0, false, null);
		return returnDTO;
	}
	
	@Override
	public List<TranscoderDTO> removeTranscoderBlackout(List<TranscoderDTO> list) {

		if (list == null || list.isEmpty()) return new ArrayList<TranscoderDTO>();

		for (int i = 0; i < list.size(); i++) {
			try {
				ServiceDTO serviceDTO = list.get(i).getService();
				if (serviceDTO != null &&  serviceDTO.getTierName() != null && serviceDTO.getTierName().toLowerCase().contains("blackout")) {
					list.remove(i);
					removeTranscoderBlackout(list);//i = 0; //the array shifted so index doesn't mean anything anymore best to start at beginning again
				}
			}
			catch (Exception e) {
			}
		}
		
		return list;		
	}

	@Override
	public void loadBncDecodersToDB() throws Exception {
		
		ApiResponse apiResponse = bncService.getBNCDecoders();
		
		List<Map<String, Object>> decoders = (List<Map<String, Object>>)apiResponse.getData();

		irdDal.clearBncDecoders();
		
		irdDal.saveBncDecoders(decoders);		
	}

	@Override
	public void loadParsedFatTiersToDB() throws Exception {
		
		List<FatTierDTO> fatTiers = irdDal.getAllFatTiers();
		List<FatTierDTO> completeFatTiers = bncService.parseFatTiers(fatTiers);
		irdDal.deleteFatTiers();
		irdDal.saveParsedFatTiers(completeFatTiers);
	}

	@Override
	public List<IrdSummaryDTO> newReadIrdsFromDB(NewRequestDecodersFilteredDTO filter) throws Exception {
		
		//TODO - use the filter object to do the search and make sure it uses pagination and all that good stuff
		return null;				
	}
	
}









