package com.hbola.das.service;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hbola.das.dal.EncoderDALI;
import com.hbola.das.dto.EncoderDTO;
import com.hbola.das.dto.ServiceDTO;

@Service
public class EncoderServiceImpl implements EncoderServiceI{

	private static final Logger logger = LogManager.getLogger(EncoderServiceImpl.class);
	
	@Autowired
	private EncoderDALI encoderDal;
	
	@Override
	public void completeEncodersWithServices(List<EncoderDTO> encoders, List<ServiceDTO> services, int implementsTemplate) {
	
		if (implementsTemplate == 1) {
		
			encoders.forEach(e -> {			
				services.forEach(s -> {			
					
					if(s.getTemplateId() > 0 && (e.getId() == s.getEncoderId() || s.getEncoderId() == -1))
					{
						e.getServices().add(s);
					}			
				});
			});
		}		
		else {
		
			encoders.forEach(e -> {			
				services.forEach(s -> {			
					
					if(s.getTemplateId() == 0 && (e.getId() == s.getEncoderId() || s.getEncoderId() == -1))
					{
						e.getServices().add(s);
					}			
				});
			});
		}
		
	}

	@Override
	public List<EncoderDTO> getList(Boolean implementsTemplate) throws Exception {
		try {
			return encoderDal.getEncoders(implementsTemplate);
		}
		catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	@Override
	public List<ServiceDTO> getServices() throws Exception {
		try {
			return encoderDal.getServices();
		}
		catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}
}
