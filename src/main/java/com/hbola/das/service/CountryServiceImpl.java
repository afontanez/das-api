package com.hbola.das.service;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hbola.das.dal.CountryDALI;
import com.hbola.das.dto.CountryDTO;

@Service
public class CountryServiceImpl implements CountryServiceI {

	private static final Logger logger = LogManager.getLogger(CountryServiceImpl.class);
	@Autowired 
	private CountryDALI countryDal;

	@Override
	public List<CountryDTO> getList() throws Exception {
		try {
			return countryDal.getList();
		}
		catch (Exception e) {
			logger.warn("Problem getting Country: " + e.getMessage());
			throw e;//propogate
		}
	}

}
