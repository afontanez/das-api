package com.hbola.das.service;

import java.util.List;

import com.hbola.das.dto.EncoderDTO;
import com.hbola.das.dto.ServiceDTO;

public interface EncoderServiceI {
	
	public void completeEncodersWithServices(List<EncoderDTO> encoders, List<ServiceDTO> services, int implementsTemplate); 
	
	public List<EncoderDTO> getList(Boolean implementsTemplate) throws Exception;
	public List<ServiceDTO> getServices() throws Exception;
}
