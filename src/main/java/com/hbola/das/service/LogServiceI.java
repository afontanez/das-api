package com.hbola.das.service;

import java.sql.Connection;
import java.util.List;

import com.hbola.das.dto.HistoryLogDTO;
import com.hbola.das.dto.TransactionLogDTO;

public interface LogServiceI {

	List<HistoryLogDTO> getIrdHistoryLog(String modelId, Integer decoderId) throws Exception;
	List<TransactionLogDTO> getIrdTransactionLog(Integer decoderId) throws Exception;
	void saveTransactionLog(String action, String status, String loggedInUser, 
	int rollback, int decoderId, int decoderServiceId, Connection conn) throws Exception;
}
