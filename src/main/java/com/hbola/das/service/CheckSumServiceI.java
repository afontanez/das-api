package com.hbola.das.service;

public interface CheckSumServiceI {

	String crc8(String unitAddress);

}