package com.hbola.das.service;

import java.util.List;
import java.util.Map;

import com.hbola.das.dto.ApiResponse;
import com.hbola.das.dto.FatTierDTO;
import com.hbola.das.dto.IrdBNCDTO;
import com.hbola.das.dto.IrdEditDTO;

public interface BNCServiceI {

	String createBNCJson(List<IrdBNCDTO> bncDTOList);

	void createIRDOnBNC(IrdEditDTO dto) throws Exception;
	
	void createIRDOnBNCBatch(List<IrdEditDTO> irds) throws Exception;

	void createIRDOnBNC(List<IrdBNCDTO> bncDTOList) throws Exception;

	void tripIRDOnBNC(String modelNumber, String unitAddress) throws Exception;
	
	ApiResponse getPresets() throws Exception;
	
	ApiResponse getBNCDecoders() throws Exception;

	List<FatTierDTO> parseFatTiers(List<FatTierDTO> fatTiers) throws Exception;

}