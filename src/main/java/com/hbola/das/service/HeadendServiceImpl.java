package com.hbola.das.service;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hbola.das.dal.HeadendDALI;
import com.hbola.das.dto.HeadendDTO;
@Service
public class HeadendServiceImpl implements HeadendServiceI {


	private static final Logger logger = LogManager.getLogger(HeadendServiceImpl.class);
	
	@Autowired
	private HeadendDALI headendDal;
	
	@Override
	public int create(HeadendDTO headend)  throws Exception{
		try {
			return headendDal.create(headend);
		}
		catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	@Override
	public int edit(HeadendDTO headend)  throws Exception{
		try {
			return headendDal.edit(headend);
		}
		catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	@Override
	public List<HeadendDTO> getList() throws Exception{
		try {
			return headendDal.getList();
		}
		catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	@Override
	public HeadendDTO getOneByCode(int code, String cableCode)  throws Exception{
		try {
			return headendDal.getOneByCode(code, cableCode);
		}
		catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	@Override
	public List<HeadendDTO> getListByCableCode(String cableCode) throws Exception {
		try {
			return headendDal.getListByCableCode(cableCode);
		}
		catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}
}
