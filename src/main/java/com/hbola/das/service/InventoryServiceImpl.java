package com.hbola.das.service;

import java.io.InputStream;
import java.util.List;
import java.util.Properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.hbola.das.config.ConfigCache;
import com.hbola.das.dal.IrdDALI;
import com.hbola.das.dto.IrdEditDTO;
import com.hbola.das.utils.StringUtils;

@Service
public class InventoryServiceImpl implements InventoryServiceI {

	@Autowired
	ExcelServiceI excelService;
	
	@Autowired
	IrdDALI irdDal;
	
	@Override
	public int process(MultipartFile file, String user) throws Exception {
		
		InputStream inputStream = file.getInputStream();
		List<Properties> list = excelService.extract(inputStream);
		inputStream.close();

		String headEndCode = ConfigCache.INSTANCE.getProperties().getProperty("INVENTORY_DEFAULT_HEADEND");
		String cableOperatorCode = ConfigCache.INSTANCE.getProperties().getProperty("INVENTORY_DEFAULT_CABLECODE");
		String encoderIdString = ConfigCache.INSTANCE.getProperties().getProperty("INVENTORY_DEFAULT_ENCODER_ID");
		
		int encoderId = 0;
		try {
			encoderId = Integer.parseInt(encoderIdString);
		}
		catch (Exception e) {
			encoderId = 0;
		}
		
		int numDecodersInventoried = 0;
		for (Properties prop : list) {
		
			String description = prop.getProperty(InventoryServiceI.DESCRIPTION);
			String modelName = StringUtils.parseInventoryDescriptionForModel(description);
			
			IrdEditDTO dto = new IrdEditDTO(user, modelName, prop.getProperty(InventoryServiceI.SERIAL_NUMBER), prop.getProperty(InventoryServiceI.UNIT_ADDRESS), headEndCode, cableOperatorCode, encoderId, true, "");			
			irdDal.saveIrd(dto);			
			
			numDecodersInventoried++;
		}
				
		return numDecodersInventoried;
	}
	
	

}
