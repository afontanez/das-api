package com.hbola.das.service;



import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.quartz.CronScheduleBuilder;
import org.quartz.CronTrigger;
import org.quartz.Job;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.Scheduler;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

/**
 * Extracts all the BNC decoders and loads them into the DB at 1am everyday
 * 
 * @author afontanez
 *
 */
@Service
public class LoadBNCDecodersScheduler implements Job {

	@Autowired
	private IrdServiceI irdService;
	
	private static final Logger logger = LogManager.getLogger(LoadBNCDecodersScheduler.class);
		
	public void execute(JobExecutionContext context) throws JobExecutionException {
		
		logger.debug("start loading BNC decdoers into DB");
		
		//quartz doesn't know about spring and will not load @autowried services without this
		SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
		
		try {
			irdService.loadBncDecodersToDB();
			context.getScheduler().shutdown(true);
		} catch (Exception e) {
			logger.warn("Problem loading BNC decoders into DB. e.getMessage: " + e.getMessage());
		}
		
		System.out.println("end loading BNC decdoers into DB");
		logger.debug("end loading BNC decdoers into DB");
	}
	
	public void job() {
		
		try {
			JobDetail job = JobBuilder.newJob(LoadBNCDecodersScheduler.class).withIdentity("LoadBNCJob", "LoadBNCGroup").build();
	
			CronTrigger trigger = TriggerBuilder
					.newTrigger()
					.withIdentity("LoadBNCDecodersSchedulerTrigger", "LoadBNCGroup")
					.withSchedule(CronScheduleBuilder.cronSchedule("0 1 * * * ?"))//1 am 
					.build();
	    	
	    	//schedule it
	    	Scheduler scheduler = new StdSchedulerFactory().getScheduler();
	    	scheduler.start();
	    	scheduler.scheduleJob(job, trigger);
		}
		catch (Exception e) {
			logger.warn("Could not start job to load BNC decoders into DB. e.getMessage: " + e.getMessage());
		}
	}
	
	
}
