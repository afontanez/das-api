package com.hbola.das.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.hbola.das.config.ConfigCache;
import com.hbola.das.dto.ApiResponse;
import com.hbola.das.dto.FatTierDTO;
import com.hbola.das.dto.IrdBNCDTO;
import com.hbola.das.dto.IrdEditDTO;
import com.hbola.das.utils.StringUtils;

@Service
public class BNCServiceImpl implements BNCServiceI {
	
	private static final Logger logger = LogManager.getLogger(BNCServiceImpl.class);
	
	//Thread safe as per Using Gson section of documentation: https://sites.google.com/site/gson/gson-user-guide#TOC-Using-Gson
	private static final Gson GSON = new GsonBuilder().disableHtmlEscaping().create();
	
	@Autowired 
	RestTemplate restTemplate;
	
	@Override
	public String createBNCJson(List<IrdBNCDTO> bncDTOList) {
	
		String json = "";
		try {
			if (bncDTOList != null) {
				json = GSON.toJson(bncDTOList);
			}
		}
		catch (Exception e) {
			logger.warn("Problem creating json from ird dto: e.getMessage: " + e.getMessage());
		}
		
		return json;
		
	}
	
	@Override
	public void createIRDOnBNC(IrdEditDTO dto) throws Exception {
		
		List<IrdBNCDTO> bncDTOList = new ArrayList<IrdBNCDTO>();
		IrdBNCDTO bncDTO = new IrdBNCDTO(dto);				
		bncDTOList.add(bncDTO);
		
		this.createIRDOnBNC(bncDTOList);
	}

	@Override
	public void createIRDOnBNCBatch(List<IrdEditDTO> dtoList) throws Exception {
		
		List<IrdBNCDTO> bncDTOList = new ArrayList<IrdBNCDTO>();
		dtoList.forEach( ird -> {
		
			IrdBNCDTO bncDTO = new IrdBNCDTO(ird);
			bncDTOList.add(bncDTO);
		});
		
		this.createIRDOnBNC(bncDTOList);
	}
	
	@Override
	public void createIRDOnBNC(List<IrdBNCDTO> bncDTOList) throws Exception {
									
		try {			
			String json = this.createBNCJson(bncDTOList);
			
			logger.info("json before calling bnc: "+json);
			
			ApiResponse apiResponse = restTemplate.postForObject(ConfigCache.getBNCAPIEndPoint()+"/ird", json, ApiResponse.class);
			if (apiResponse!= null && apiResponse.getStatus() != null && !apiResponse.getStatus().equalsIgnoreCase(ApiResponse.OK)) {
				throw new Exception("BNC returned error: " + apiResponse.getData());
			}
		}
		catch (Exception e) {
			logger.warn(e.getMessage());
			throw new Exception(e.getMessage());
		}
	}

	@Override
	public void tripIRDOnBNC(String modelNumber, String unitAddress) throws Exception {
						
		try {			
			
			String truncatedUnitAddress = StringUtils.truncateUA(unitAddress); 
			
			ApiResponse apiResponse = restTemplate.getForObject(ConfigCache.getBNCAPIEndPoint()+"/ird/trip/" + modelNumber + "/" + truncatedUnitAddress, ApiResponse.class);
			if (apiResponse!= null && apiResponse.getStatus() != null && !apiResponse.getStatus().equalsIgnoreCase(ApiResponse.OK)) {
				throw new Exception("BNC returned error: " + apiResponse.getData());
			}
		}
		catch (Exception e) {
			logger.warn(e.getMessage());
			throw new Exception(e.getMessage());
		}
	}

	@Override
	public ApiResponse getPresets() throws Exception {
		try {			
			ApiResponse apiResponse = restTemplate.getForObject(ConfigCache.getBNCAPIEndPoint()+"/presets", ApiResponse.class);
			if (apiResponse!= null && apiResponse.getStatus() != null && !apiResponse.getStatus().equalsIgnoreCase(ApiResponse.OK)) {
				throw new Exception("BNC returned error: " + apiResponse.getData());
			}
			return apiResponse;
		}
		catch (Exception e) {
			logger.warn(e.getMessage());
			throw new Exception(e.getMessage());
		}
	}

	@Override
	public ApiResponse getBNCDecoders() throws Exception {
		
		ApiResponse apiResponse = null;
		try {			
			apiResponse = restTemplate.getForObject(ConfigCache.getBNCAPIEndPoint()+"/bnc-decoders", ApiResponse.class);
			if (apiResponse!= null && apiResponse.getStatus() != null && !apiResponse.getStatus().equalsIgnoreCase(ApiResponse.OK)) {
				throw new Exception("BNC returned error: " + apiResponse.getData());
			}						
		}
		catch (Exception e) {
			logger.warn(e.getMessage());
			throw new Exception(e.getMessage());
		}
		
		return apiResponse;
	}

	@Override
	public List<FatTierDTO> parseFatTiers(List<FatTierDTO> fatTiers) throws Exception {
		
		ApiResponse apiResponse = null;
		try {			
			
			for (FatTierDTO t : fatTiers) {
				
				String command = "/parseExtendedTier/" + t.getFatTier() + "/" + t.getIsTierExtended(); 			
				apiResponse = restTemplate.getForObject(ConfigCache.getBNCAPIEndPoint() + command, ApiResponse.class);
				if (apiResponse!= null && apiResponse.getStatus() != null && !apiResponse.getStatus().equalsIgnoreCase(ApiResponse.OK)) {
					//throw new Exception("BNC returned error: " + apiResponse.getData());
					
					//error make parsed tier the same as fat tier
					List<Integer> tiers = new ArrayList<Integer>();
					tiers.add(t.getFatTier());
					t.setTiers(tiers);
				}
				else {
				
					List<Integer> parsedTiers = (List<Integer>) apiResponse.getData();
					t.setTiers(parsedTiers);
				}
			}
			
		}
		catch (Exception e) {
			logger.warn(e.getMessage());
			throw new Exception(e.getMessage());
		}
		
		return fatTiers;
		
	}

}
