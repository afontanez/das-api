package com.hbola.das.service;

import java.util.List;

import com.hbola.das.dto.FirmwareVersionDTO;
import com.hbola.das.dto.IrdSetupEncodersOperations;
import com.hbola.das.dto.IrdSetupModelsCableOperators;
import com.hbola.das.dto.ModelDTO;
import com.hbola.das.dto.PresetDTO;
import com.hbola.das.dto.TranscoderDTO;

public interface SetupIrdServiceI {

	public void completeModelWithFirmwareVersions(List<ModelDTO> models, List<FirmwareVersionDTO> firmwareVersions);
	public void completeModelWithPresets(List<ModelDTO> models, List<PresetDTO> presets);

	public List<FirmwareVersionDTO> getFirmwareVersions() throws Exception;
	public List<ModelDTO> getModels() throws Exception;
	public List<PresetDTO> getDBPresets() throws Exception;
	
	List<PresetDTO> getBNCPresets()throws Exception;
	
	IrdSetupModelsCableOperators getIrdSetupModelsCableOperators() throws Exception;
	
	IrdSetupEncodersOperations getIrdSetupEncodersOperations(int implementsTemplate)throws Exception;

}
