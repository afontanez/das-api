package com.hbola.das.service;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Service;

@Service
public class ExcelServiceImpl implements ExcelServiceI {

	@Override
	public List<Properties> extract(InputStream excelFile) throws Exception {
		
		List<Properties> propertiesList = new ArrayList<Properties>();
		if (excelFile != null) {
			
			Workbook workbook = new HSSFWorkbook(excelFile);
			
	        Sheet datatypeSheet = workbook.getSheetAt(0);
	        Iterator<Row> iterator = datatypeSheet.iterator();
	        	
	        int arrisDataRow = 1;
	        while (iterator.hasNext() && arrisDataRow++ < 9) {//the arris spread sheet data starts on row 9	        	
	        	iterator.next();
	        }
	        	        
	        while (iterator.hasNext()) { 
	        	
	        	Row currentRow = iterator.next();
	            Iterator<Cell> cellIterator = currentRow.iterator();
	            
	            Properties props = new Properties();	            
	            props.put(InventoryServiceI.PALLET_ID, cellIterator.next().getStringCellValue());
	            props.put(InventoryServiceI.CARTON_ID, cellIterator.next().getStringCellValue());
	            props.put(InventoryServiceI.ITEM_NUMBER, cellIterator.next().getStringCellValue());
	            props.put(InventoryServiceI.DESCRIPTION, cellIterator.next().getStringCellValue());
	            props.put(InventoryServiceI.SERIAL_NUMBER, cellIterator.next().getStringCellValue());
	            props.put(InventoryServiceI.CUSTOMER_PO, cellIterator.next().getStringCellValue());
	            props.put(InventoryServiceI.SALES_ORDER, cellIterator.next().getStringCellValue());
	            props.put(InventoryServiceI.DELIVERY_ID, cellIterator.next().getStringCellValue());
	            props.put(InventoryServiceI.ORDER_LINE_ID, cellIterator.next().getStringCellValue());
	            props.put(InventoryServiceI.SHIP_ADDRESS, cellIterator.next().getStringCellValue());
	            props.put(InventoryServiceI.MANUFACTURE_DATE, cellIterator.next().getStringCellValue());
	            props.put(InventoryServiceI.UNIT_ADDRESS, cellIterator.next().getStringCellValue());

	            propertiesList.add(props);
	        }
	        
	        workbook.close();
		}
        
		return propertiesList;
	}	
	
}
