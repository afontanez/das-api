package com.hbola.das.service;

import org.springframework.stereotype.Service;

@Service
public class CheckSumServiceImpl implements CheckSumServiceI {

	@Override
	public String crc8(String unitAddress) {
		
		if (unitAddress == null || unitAddress.isEmpty()) return "000";
		
		String returnValue = "000";
		try {
			long uiUnitAddressLo = Long.parseLong(unitAddress);//316233301L;
			long uiUnitAddressHi = uiUnitAddressLo >>> 32; 

			long[] unitAddr = new long[5];
			unitAddr[0] = uiUnitAddressHi;
			unitAddr[1] = ((uiUnitAddressLo & 0xff000000L) >>> (3*8));
			unitAddr[2] = ((uiUnitAddressLo & 0x00ff0000L) >>> (2*8));
			unitAddr[3] = ((uiUnitAddressLo & 0x0000ff00L) >>> (1*8));
			unitAddr[4] = ((uiUnitAddressLo & 0x000000ffL) >>> (0*8));

			// Eight Bit CRC is required to compute checksum of unit address.
			// G(x)= x^8 + x^7 +x^4 + x^3 + x + 1
	
			// Legacy algo
			// This routine is copied from delphi/auth/unitauth.cxx
			long[] bits = new long[40];
	
			//## Convert the five bytes to 40 1-bit numbers
			int b = 0;
			for (int i = 0; i < 5; i++)
			{
				for(int j = 0; j < 8; j++)
				{
					bits[b++] = (unitAddr[i] >>> (7-j)) & 0x01L;
				}
			}
	
			//## Calculate the CRC
			long[] reg = new long[8];
			for (int j = 0; j <= 7; j++)
			{
				reg[j] = 1;
			}
			for (b = 0; b < 40; b++)
			{
				long fb = bits[b] ^ reg[7];
				reg[7] = reg[6] ^ fb;
				reg[6] = reg[5];
				reg[5] = reg[4];
				reg[4] = reg[3] ^ fb;
				reg[3] = reg[2] ^ fb;
				reg[2] = reg[1];
				reg[1] = reg[0] ^ fb;
				reg[0] = fb;
			}
			
			long crc = 0;
			for (int j = 0; j < 8; j++)
			{
				crc |= (reg[j] & 1) << j;
			}

			returnValue = Long.toString(crc);			
			while (returnValue.length() < 3) {
				returnValue = "0" + returnValue;
			}
		}
		catch (Exception e) {
			returnValue = "000";
		}
		
		return returnValue;
	}
		
}
