package com.hbola.das.service;

import java.util.List;

import com.hbola.das.dto.CountryDTO;

public interface CountryServiceI {

	List<CountryDTO> getList() throws Exception;

}
