package com.hbola.das.service;

import java.util.List;

import com.hbola.das.dto.MsoDTO;

public interface MsoServiceI {

	public int create(MsoDTO mso) throws Exception;
	public int edit(MsoDTO mso) throws Exception;
	public MsoDTO getOneByCode(String code) throws Exception;
	public List<MsoDTO> getList() throws Exception;

}
