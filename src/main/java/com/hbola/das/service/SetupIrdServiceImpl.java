package com.hbola.das.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hbola.das.dal.IrdDALI;
import com.hbola.das.dal.ModelDALI;
import com.hbola.das.dto.ApiResponse;
import com.hbola.das.dto.CableOperatorDTO;
import com.hbola.das.dto.EncoderDTO;
import com.hbola.das.dto.FirmwareVersionDTO;
import com.hbola.das.dto.IrdSetupEncodersOperations;
import com.hbola.das.dto.IrdSetupModelsCableOperators;
import com.hbola.das.dto.ModelDTO;
import com.hbola.das.dto.OperationDTO;
import com.hbola.das.dto.PresetDTO;
import com.hbola.das.dto.ServiceDTO;
import com.hbola.das.dto.TranscoderDTO;

@Service
public class SetupIrdServiceImpl implements SetupIrdServiceI {


	private static final Logger logger = LogManager.getLogger(SetupIrdServiceImpl.class);

	@Autowired 
	private IrdDALI irdDal;
	
	@Autowired
	private ModelDALI modelDal;
	
	@Autowired
	private BNCServiceI bncService;
	
	@Autowired
	private CableOperatorServiceI cableOperatorService;
	
	@Autowired
	private EncoderServiceI encoderService;
	
	@Override
	public void completeModelWithFirmwareVersions(List<ModelDTO> models,
			List<FirmwareVersionDTO> firmwareVersions) {
		
		models.forEach(m -> {			
			firmwareVersions.forEach(fv -> {			
				if(m.getName().equals(fv.getModelName()))
				{
					m.getFirmwareVersions().add(fv);
				}
			});
		});
	}
	
	@Override
	public void completeModelWithPresets(List<ModelDTO> models, List<PresetDTO> presets) {
		
		models.forEach(m -> {			
			presets.forEach(p -> {			
				if(m.getName().equals(p.getModelName()))
				{
					m.getPresets().add(p);
				}
			});
		});
	}

	@Override
	public List<FirmwareVersionDTO> getFirmwareVersions() throws Exception {
		try {
			return modelDal.getFirmwareVersions();
		}
		catch (Exception e) {
			logger.warn("Problem getting FirmwareVersions: " + e.getMessage());
			throw e;//propogate
		}
	}

	@Override
	public List<ModelDTO> getModels() throws Exception {
		try {
			return modelDal.getModels();
		}
		catch (Exception e) {
			logger.warn("Problem getting models: " + e.getMessage());
			throw e;//propogate
		}
	}

	@Override
	public List<PresetDTO> getDBPresets() throws Exception {
		try {
			return modelDal.getPresets();
		}
		catch (Exception e) {
			logger.warn("Problem getting presets: " + e.getMessage());
			throw e;//propogate
		}
	}
	
	@Override
	public List<PresetDTO> getBNCPresets() throws Exception {
		List<PresetDTO> presets = new ArrayList<PresetDTO>();
		try {
			ApiResponse response = bncService.getPresets();
			presets = (List<PresetDTO>)response.getData();
		}catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
		return presets;
	}
	
	@Override
	public IrdSetupModelsCableOperators getIrdSetupModelsCableOperators() throws Exception{
		List<ModelDTO> models = null;
		List<CableOperatorDTO> cableOperators = null;
		
		try {
			// Model Section
			models = this.getModels();
			List<FirmwareVersionDTO> firmwareVersions = this.getFirmwareVersions();
			List<PresetDTO> presets = this.getDBPresets();
			
			this.completeModelWithFirmwareVersions(models, firmwareVersions);
			this.completeModelWithPresets(models, presets);
			
			// Cable Operator Section
			cableOperators = cableOperatorService.getList();			
		}
		catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
		return new IrdSetupModelsCableOperators(models, cableOperators);
	}
	

	@Override
	public IrdSetupEncodersOperations getIrdSetupEncodersOperations(int implementsTemplate)throws Exception{
		List<EncoderDTO> encoders = new ArrayList<EncoderDTO>();
		List<ServiceDTO> services = new ArrayList<ServiceDTO>();
		List<OperationDTO> operations = new ArrayList<OperationDTO>();
		try {
			encoders = encoderService.getList(implementsTemplate == 1);
			services = encoderService.getServices();
			
			encoderService.completeEncodersWithServices(encoders, services, implementsTemplate);
			
			operations = irdDal.getOperations();
			
		}
		catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
		return new IrdSetupEncodersOperations(encoders, operations);
	}

	
}
