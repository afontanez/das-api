package com.hbola.das.service;

public interface TokenServiceI {

	public String generateToken(String username, String roleName) throws Exception;
	public boolean verify(String token) throws Exception;
}
