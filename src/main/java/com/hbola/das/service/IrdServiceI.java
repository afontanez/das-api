package com.hbola.das.service;

import java.util.List;
import java.util.Map;

import com.hbola.das.dto.IrdEditDTO;
import com.hbola.das.dto.IrdSummaryDTO;
import com.hbola.das.dto.NewRequestDecodersFilteredDTO;
import com.hbola.das.dto.OwnershipDTO;
import com.hbola.das.dto.RequestDecodersFiltered;
import com.hbola.das.dto.TranscoderDTO;

public interface IrdServiceI {

	int createIrd(IrdEditDTO ird) throws Exception;

	List <IrdSummaryDTO> newReadIrdsFromDB(NewRequestDecodersFilteredDTO filter) throws Exception;
	
	List<IrdSummaryDTO> readIrdsFromDB(RequestDecodersFiltered filter) throws Exception;
	
	IrdEditDTO readIrdById(String modelId, Integer id) throws Exception;

	void tripIrd(String modelNumber, String unitAddress) throws Exception;

	List<OwnershipDTO> getIrdSetupOwnerships() throws Exception;

	String[] setupMDUAs(String string);
	List<TranscoderDTO> setupMDUAsForMigrated(IrdEditDTO dto);

	List<TranscoderDTO> insertTranscoderBlackouts(List<TranscoderDTO> list);
	TranscoderDTO getBlackoutTranscoder(int transcoderId);

	List<TranscoderDTO> removeTranscoderBlackout(List<TranscoderDTO> list);

	List<IrdSummaryDTO> addTranscodersToIrdsWithUnitaddresses(Map<String, Object> map);

	void loadBncDecodersToDB() throws Exception;

	void loadParsedFatTiersToDB() throws Exception;

	List<TranscoderDTO> processACPsWithoutServices(Integer encoderid, List<TranscoderDTO> transcoders);

}