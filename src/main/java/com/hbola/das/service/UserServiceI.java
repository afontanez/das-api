package com.hbola.das.service;

import org.springframework.stereotype.Service;

import com.hbola.das.dto.UserLoginDTO;

@Service
public interface UserServiceI {

	public UserLoginDTO login(UserLoginDTO login) throws Exception;

	public String getRoleByUsername(String username) throws Exception;

}
