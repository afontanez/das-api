package com.hbola.das.service;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hbola.das.dal.LogDALI;
import com.hbola.das.dto.HistoryLogDTO;
import com.hbola.das.dto.TransactionLogDTO;

@Service
public class LogServiceImpl implements LogServiceI{

	private static final Logger logger = LogManager.getLogger(LogServiceImpl.class);
	

	@Autowired
	private LogDALI logDAL;
	
	@Override
	public List<HistoryLogDTO> getIrdHistoryLog(String modelId, Integer decoderId) throws Exception {
				
		List<HistoryLogDTO> historyLogList = new ArrayList<HistoryLogDTO>();
		try {
			
			if (modelId != null && modelId.equalsIgnoreCase("DSR-4410MD")) {
				historyLogList = logDAL.getMDHistoryLog(decoderId);
			}
			else {
				historyLogList = logDAL.getIrdHistoryLog(decoderId);
			}			
			
		}
		catch (Exception e) {
			logger.warn("Problem getting history log for decoderId: " + decoderId + " : " + e.getMessage());
			throw new Exception(e.getMessage());//propogate
		}
		
		return historyLogList;
	}

	@Override
	public List<TransactionLogDTO> getIrdTransactionLog(Integer decoderId) throws Exception {
		
		List<TransactionLogDTO> historyLogList = new ArrayList<TransactionLogDTO>();
		try {
			historyLogList = logDAL.getIrdTransactionLog(decoderId);
		}
		catch (Exception e) {
			logger.warn("Problem getting transactional log for decoderId: " + decoderId + " : " + e.getMessage());
			throw new Exception(e.getMessage());//propogate
		}
		return historyLogList;
	}

	@Override
	public void saveTransactionLog(String action, String status, String loggedInUser, int rollback, int decoderId,
			int decoderServiceId, Connection conn) throws Exception {
		try {
			logDAL.saveTransactionLog(action, status, loggedInUser, rollback, decoderId, decoderServiceId, conn);
		}
		catch (Exception e) {
			logger.warn("Problem getting transactional log for decoderId: " + decoderId + " : " + e.getMessage());
			throw new Exception(e.getMessage());//propogate
		}
		
	}
}
