package com.hbola.das.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.hbola.das.dto.HeadendDTO;

@Service
public interface HeadendServiceI {

	public int create(HeadendDTO headend) throws Exception;

	public int edit(HeadendDTO headend) throws Exception;

	public List<HeadendDTO> getList() throws Exception;
	
	public List<HeadendDTO> getListByCableCode(String cableCode) throws Exception;

	public HeadendDTO getOneByCode(int code, String cableCode) throws Exception;
}
