package com.hbola.das.service;

import java.util.Calendar;
import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.hbola.das.config.ConfigCache;

@Service
public class TokenServiceImpl implements TokenServiceI {
	
	private static final Logger logger = LogManager.getLogger(TokenServiceImpl.class);
	private static final String DEFAULT_SECRET = "Fnd1v1@";
	
	@Override
	public String generateToken(String username, String roleName) throws Exception {

		String token = "";
		try {
			String secret = ConfigCache.INSTANCE.getProperties().getProperty("TOKEN_SECRET");
			if (secret == null || secret.isEmpty()) secret = DEFAULT_SECRET;
			
		    Algorithm algorithm = Algorithm.HMAC256(secret);
		    
		    Calendar cal = Calendar.getInstance();
		    cal.add(Calendar.MINUTE, 30);
		    Date expiresAt = cal.getTime();
		    
		    token = JWT.create()
		        .withIssuer("auth0")
		        .withExpiresAt(expiresAt)
		        .withClaim("urn:hbola:das:username", username)
		        .withClaim("urn:hbola:das:rolename", roleName)		    
		        .sign(algorithm);
		    
		} catch (JWTCreationException e){
		    //Invalid Signing configuration / Couldn't convert Claims.
			long stamp = System.currentTimeMillis();
			logger.error("Problem creating token: " + " : " + e.getMessage() + " : " + stamp );			
			token="CouldNotCreateToken: " + stamp;
		}
		
		return token;
	}
	
	public boolean verify(String token) throws Exception {
		
		boolean isVerified = false;
		try {
			String secret = ConfigCache.INSTANCE.getProperties().getProperty("TOKEN_SECRET");
			if (secret == null || secret.isEmpty()) secret = DEFAULT_SECRET;
			
		    Algorithm algorithm = Algorithm.HMAC256(secret);
		    
		    JWTVerifier verifier = JWT.require(algorithm)
		        .withIssuer("auth0")
		        .build(); //Reusable verifier instance
		    
		    DecodedJWT jwt = verifier.verify(token);//we don't use the jwt var. verifier.verify() throws exception if invalid token
		    isVerified = true;
		} catch (Exception e){
		    //Invalid signature/claims
						
			logger.error("Could not verify token: " + e.getMessage() + " : " + token);			
			isVerified = false;
		}
		
		return isVerified;
		
	}
}
