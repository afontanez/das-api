//package com.hbola.das.helper;
//
//import java.util.HashMap;
//
//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.Logger;
//
//import com.hbola.das.dto.ACTION_NAME;
//import com.hbola.das.dto.ActionIrdDTO;
//
//public class LogHelper {
//	
//	private static final Logger logger = LogManager.getLogger(LogHelper.class);
//	
//	
//	@SuppressWarnings("serial")
//	private static HashMap<String, String> routes = new HashMap<String, String>(){{
//							put("/das-api/ird/search_post", ACTION_NAME.ACTION_SEARCH_DECODER); put("/das-api/ird/_get", ACTION_NAME.ACTION_VIEW_DECODER);
//							/*put("/das-api/ird/_post", ACTION_CREATE_DECODER);
//							put("/das-api/ird_post_", ACTION_EDIT_DECODER);*/
//							put("/das-api/user/login_post", ACTION_NAME.ACTION_LOGIN);
//							//put("/das-api/ird/trip_get", ACTION_NAME.ACTION_LOGIN);
//						}}; 
//						
//
//	static public ActionIrdDTO getActionFromRoute(String route)throws Exception{
//		ActionIrdDTO result = new ActionIrdDTO();
//		try{
//			if(route.contains("/das-api/ird/search"))
//			{
//				result.setAction(routes.get(route));
//			}
//			else if(route.contains("/das-api/ird/")){
//				String[] tmpRoute = route.split("/");
//				String lastItem = tmpRoute[tmpRoute.length -1];
//				String method = lastItem.split("_")[1];
//				String value  = lastItem.split("_")[0];
//				try{
//					result.setIrdId(Integer.valueOf(value));
//				}
//				catch(Exception e){}
//				
//				if(tmpRoute.length == 4){
//					result.setAction(routes.get("/das-api/ird/_"+method));
//				}
//				else{
//					result.setAction(routes.get(route));
//				}
//			}
//			else{
//				result.setAction(routes.get(route));
//			}
//			
//			if(result.getAction() == null || result.getAction().isEmpty() ){
//				new ActionIrdDTO();
//			}
//		} catch (Exception e){
//	    	logger.warn(e);
//	    	result.setAction(routes.get(route));
//	    	throw new Exception("Error saving TransactionalLog. Error: " + e.getMessage());
//	    }
//		return result;
//	}
//}
