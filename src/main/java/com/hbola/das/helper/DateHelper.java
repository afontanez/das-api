package com.hbola.das.helper;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateHelper {

	public static String convertDateToString(Date date){
		DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
        return formatter.format(date);		
	}
}
