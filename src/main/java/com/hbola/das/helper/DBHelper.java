package com.hbola.das.helper;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.hbola.das.dto.RequestDecodersFiltered;


public class DBHelper {

	private static final Logger logger = LogManager.getLogger(DBHelper.class);
    
    private static DataSource dataSource;    
    static {
    	try {
    		//the bnc-api uses jboss context factory and das-api uses apache context factory
    		//sometimes das-api picks a jboss context factory and this throws an error 
    		//explicitly tell das-api to use apache context factory to get a connection from the pool
    		Hashtable<String, String> env = new Hashtable<String, String>();
    		env.put(Context.INITIAL_CONTEXT_FACTORY, "org.apache.naming.java.javaURLContextFactory");
    		Context initContext = new InitialContext(env);
    		//printContext();
    		
    		dataSource = (DataSource) initContext.lookup("java:/comp/env/authcenter");    	    		
    	}
    	catch (Exception e) {
    		logger.error("Problem initilizing data source: e.getMessage: " + e.getMessage());
    		printContext();
    		e.printStackTrace();
    	}
    }

    //this is for testing why the connection pool is suddenly not available after working for days.
    private static void printContext() {
		try {
			Context initContext = new InitialContext();
			Hashtable<?, ?> ht = initContext.getEnvironment();
			Enumeration<?> keys = ht.keys();
			while (keys.hasMoreElements()) {
				Object key = keys.nextElement();
				logger.error("	context (key, value) : " + key + " " + ht.get(key));
			}
		}
		catch (Exception e2) {
			e2.printStackTrace();
		}

    }
    
    public static Connection getConnection() throws Exception {
        try {        	
            Connection connection = dataSource.getConnection();
            connection.setAutoCommit(false);
            return connection;
        } catch (Exception e) {
        	printContext();
            logger.error(e);
            throw new Exception(e.getMessage());
        }
    }
    
    public static Connection getConnection(boolean autoCommit) throws Exception {
        try {        	
            Connection connection = dataSource.getConnection();
            connection.setAutoCommit(autoCommit);
            return connection;
        } catch (Exception e) {
            logger.error(e);
            throw new Exception(e.getMessage());
        }
    }

	//https://tomcat.apache.org/tomcat-8.0-doc/jndi-datasource-examples-howto.html  -> common problems section
    public static void closeConnection(ResultSet rs, Statement stmnt, Connection conn) throws Exception {
    	// Always make sure result sets and statements are closed,
        // and the connection is returned to the pool
        if (rs != null) {
          try { rs.close(); } catch (SQLException e) { ; }
          rs = null;
        }
        if (stmnt != null) {
          try { stmnt.close(); } catch (SQLException e) { ; }
          stmnt = null;
        }
        if (conn != null) {
          try { conn.close(); } catch (SQLException e) { ; }
          conn = null;
        }
    }
    	
    
    public static void commitAndCloseConnection(ResultSet rs, Statement stmnt, Connection conn) throws Exception {
    	try {
    		conn.commit();
		} catch (Exception e) {
			logger.error("Problem commit a trasaction: e.getMessage: " + e.getMessage());
		}
		finally{
			closeConnection(rs, stmnt, conn);
		}
    }
    
    public static void rollbackAndCloseConnection(ResultSet rs, Statement stmnt, Connection conn) throws Exception {
    	try {
    		conn.rollback();
		} catch (Exception e) {
			logger.error("Problem rolling back a trasaction: e.getMessage: " + e.getMessage());
		}
		finally{
			closeConnection(rs, stmnt, conn);
		}
    }

	public static ResultSet getResultSet(CallableStatement callableStatement) throws SQLException {
		boolean results = callableStatement.execute();
		int rowsAffected = 0;
		ResultSet resultSet = null;
		
		// Protects against lack of SET NOCOUNT in stored procedure
		while (results || rowsAffected != -1) {
		    if (results) {
		    	resultSet = callableStatement.getResultSet();
		        break;
		    } else {
		        rowsAffected = callableStatement.getUpdateCount();
		    }
		    results = callableStatement.getMoreResults();
		}
		return resultSet;
	}
	
	public static RequestDecodersFiltered setupDefaultIrdFilter() {
	    	
		RequestDecodersFiltered filter = new RequestDecodersFiltered();
		try {
			Calendar cal = Calendar.getInstance();
			cal.set(2010, 0, 1);
	    		
			filter.setInitDate(cal.getTime());
			filter.setEndDate(new Date());
			filter.setIsInventory(false);
		}
		catch (Exception e) {
			logger.error("Problem setting up default filter for cache: " + e.getMessage());
		}
	    	
		return filter;
	}
	    
}
