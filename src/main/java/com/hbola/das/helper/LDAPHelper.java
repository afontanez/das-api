package com.hbola.das.helper;

import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.xml.bind.PropertyException;

import com.hbola.das.config.ConfigCache;
import com.sun.jndi.ldap.LdapCtxFactory;

@SuppressWarnings("restriction")
public class LDAPHelper {

	@SuppressWarnings({ "rawtypes", "unchecked"})
	static public DirContext checkCredential(String username, String password)
			throws PropertyException, NamingException,  Exception{
		DirContext context;
		String domainName= ConfigCache.getLDAPDomainName();
		String serverName = ConfigCache.getLDAPServerName();

		// bind by using the specified username/password
		Hashtable props = new Hashtable();
		String principalName = username + "@" + domainName;
		props.put(Context.SECURITY_PRINCIPAL, principalName);
		props.put(Context.SECURITY_CREDENTIALS, password);

		context = LdapCtxFactory.getLdapCtxInstance("ldap://" + serverName + "." + domainName + '/', props);
		
		// locate this user's record
		SearchControls controls = new SearchControls();
		controls.setSearchScope(SearchControls.SUBTREE_SCOPE);
		NamingEnumeration<SearchResult> renum = context.search(toDC(domainName),
		        "(& (userPrincipalName=" + principalName + ")(objectClass=user))", controls);
		if (!renum.hasMore()) {
    		throw new Exception("LDAP authentication failed: User not found!");
		}
		else{
		    context.close();
		}
		/*
		 * Work with groups
		            SearchResult result = renum.next();

		            List<GrantedAuthority> groups = new ArrayList<GrantedAuthority>();
		            Attribute memberOf = result.getAttributes().get("memberOf");
		            if (memberOf != null) {// null if this user belongs to no group at all
		                for (int i = 0; i < memberOf.size(); i++) {
		                    Attributes atts = context.getAttributes(memberOf.get(i).toString(), new String[] { "CN" });
		                    Attribute att = atts.get("CN");
		                    groups.add(new GrantedAuthorityImpl(att.get().toString()));
		                }
		            }

		            context.close();

		            Iterator ig = groups.iterator();
		            while (ig.hasNext()) {
		                System.out.println("   " + ig.next().toString());
		            }
		*/
		return context;
	}

	 private static String toDC(String domainName) {
	        StringBuilder buf = new StringBuilder();
	        for (String token : domainName.split("\\.")) {
	            if (token.length() == 0)
	                continue; // defensive check
	            if (buf.length() > 0)
	                buf.append(",");
	            buf.append("DC=").append(token);
	        }
	        return buf.toString();
	    }
}
