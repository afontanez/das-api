package com.hbola.das.config;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.web.client.RestTemplate;

import com.hbola.das.interceptor.IncomingApiCallInterceptor;
import com.hbola.das.interceptor.OutgoingApiCallInterceptor;

@Configuration
public class RestClientConfig {
 
	@Bean
	public OutgoingApiCallInterceptor outgoingHttpInterceptor() {
		return new OutgoingApiCallInterceptor();
	}
	
	@Bean
    public RestTemplate restTemplate() {
		RestTemplate restTemplate = new RestTemplate();
 
        List<ClientHttpRequestInterceptor> interceptors = restTemplate.getInterceptors();
        if (CollectionUtils.isEmpty(interceptors)) {
        	interceptors = new ArrayList<>();
        }
        interceptors.add(outgoingHttpInterceptor());
        restTemplate.setInterceptors(interceptors);
        return restTemplate;
    }
}
