package com.hbola.das.config;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.xml.bind.PropertyException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.hbola.das.dto.IrdEditDTO;
import com.hbola.das.dto.IrdSummaryDTO;
import com.hbola.das.dto.RequestDecodersFiltered;
import com.hbola.das.helper.DBHelper;
import com.hbola.das.service.IrdServiceI;

public enum ConfigCache {
	
	INSTANCE;//all calls must be made through instance to ensure singleton use
	
	private static final Logger logger = LogManager.getLogger(ConfigCache.class);
		
	private static final String TOMCAT_HOME_DEFAULT = "C:/Program Files/Apache Software Foundation/Tomcat 7.0";
    private static final Properties PROPS = new Properties();
    
//    private List<IrdSummaryDTO> irdCache = new ArrayList<IrdSummaryDTO>();

    public Properties getProperties() {
        return PROPS;
    }
    
    public void reload() {       
    	
    	//for testing - this will load the class triggering the static declaration in the class to get the context
    	try {
    		DBHelper.getConnection();
    	} catch (Exception e) {}
    	
    	
    	String tomcatHome = tomcatHome();
    	
    	logger.info("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
    	logger.info("ConfigCache reload. tomcatHome: " + tomcatHome);
    	logger.info("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
    	
        try {
        	
        	File file = new File(tomcatHome + "/das-api.properties");           
            InputStream fis = new FileInputStream(file);            
            PROPS.load(fis);
            
            
                       
        } catch (IOException e) {
            logger.error("Problem loading das-api.properties: " + e.getMessage());
        }
    }
         
    public static String getBNCAPIEndPoint() throws PropertyException{
    	try{
    		return INSTANCE.getProperties().getProperty("BNC_API_ENDPOINT");
    	}
    	catch(Exception e){
    		logger.error("Problem loading url BNC API: " + e.getMessage());
    		throw new PropertyException(e.getMessage());
    	}
    }
    
    public static String getLDAPDomainName() throws PropertyException{
    	try{
    		return INSTANCE.getProperties().getProperty("LDAP_DOMAIN_NAME");
    	}
    	catch(Exception e){
    		logger.error("Problem loading LDAP_DOMAIN_NAME: " + e.getMessage());
    		throw new PropertyException(e.getMessage());
    	}
    }
    public static String getLDAPServerName() throws PropertyException{
    	try{
    		return INSTANCE.getProperties().getProperty("LDAP_SERVER_NAME");
    	}
    	catch(Exception e){
    		logger.error("Problem loading LDAP_SERVER_NAME: " + e.getMessage());
    		throw new PropertyException(e.getMessage());
    	}
    }
    
    public static String getSaltPassword() throws PropertyException{
    	try{
    		return INSTANCE.getProperties().getProperty("SALT_PASSWORD");
    	}
    	catch(Exception e){
    		logger.error("Problem loading SALT_PASSWORD: " + e.getMessage());
    		throw new PropertyException(e.getMessage());
    	}
    }
    private static String tomcatHome() {
    	
    	String tomcatHome = null;
    	try {
    		RuntimeMXBean bean = ManagementFactory.getRuntimeMXBean();
    		Map<String, String> map = bean.getSystemProperties();
    		tomcatHome = map.get("catalina.home");
    	 
    		if (tomcatHome == null || tomcatHome.isEmpty()) {
    			String message = "catalina.home environment variable is null. Defaulting to " + TOMCAT_HOME_DEFAULT;
    			logger.info(message);
    			tomcatHome = TOMCAT_HOME_DEFAULT;
    		}
    	}
    	catch (Exception e) {
    		String message = "Exception thrown when finding catalina.home. Defaulting to " + TOMCAT_HOME_DEFAULT;
    		logger.error(message);
    		tomcatHome = TOMCAT_HOME_DEFAULT;
    	}
    	 
    	return tomcatHome;
    }
    
}
