package com.hbola.das.config;

import java.util.Collection;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.quartz.Scheduler;
import org.quartz.impl.StdSchedulerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.hbola.das.service.LoadBNCDecodersScheduler;

//ServletConext is maintained by the container while application context is maintained by spring. so to get the container shutdown we need to implement ServletContextListener.
@WebListener
@Component
public class DASLifeCycleListener implements ServletContextListener {

	private static final Logger logger = LogManager.getLogger(DASLifeCycleListener.class);
	
	@Override
	public void contextInitialized(ServletContextEvent sce) {
		
		boolean hasErrors = false;
		try {
			logger.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Starting Quartz Jobs ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
			
			LoadBNCDecodersScheduler loadBNCDecodersScheduler = new LoadBNCDecodersScheduler();
			loadBNCDecodersScheduler.job();				
		}
		catch (Exception e) {
			hasErrors = true;
			logger.error("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Problem Starting Quartz Jobs: " + e.getMessage() + "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
			e.printStackTrace();
		}
		
		if (!hasErrors) {//don't print this if there are errors it's confusing in the logs			
			logger.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Starting Quartz Jobs Complete ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		}
	}
	
	@Override
	public void contextDestroyed(ServletContextEvent sce) {
			
		
		boolean hasErrors = false;
		try {
			logger.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Shutting Down Any Existing Quartz Jobs ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
			
			Collection<Scheduler> schedulerCollection = new StdSchedulerFactory().getAllSchedulers();
			for (Scheduler scheduler : schedulerCollection) {
				scheduler.shutdown(true);
			}
									
			ThreadGroup threadGroup = Thread.currentThread().getThreadGroup();
			Thread[] list = new Thread[threadGroup.activeCount()];			
			threadGroup.enumerate(list);
			
			for (Thread t : list) {
				try {
					if (t != null && t.getName() != null && t.getName().contains("Timer")) {//quartz creates a thread named like "[Timer-0]" that doesn't shutdown and might be a memory leak
						t.stop();//its depricated for concurrency issues but at shutdown we don't have to worry about those.
					}
				} 
				catch (Exception e) {
					hasErrors = true;
					logger.error("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Problem Shutting Down Thread: " + t.getName() +" ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
					e.printStackTrace();
				}
				
			}
						
		} catch (Exception e) {
			hasErrors = true;
			logger.error("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Problem Shutting Down Quartz Jobs: " + e.getMessage() + " ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
			e.printStackTrace();
		}
		
		if (!hasErrors) {
			logger.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Shutting Down Any Existing Quartz Jobs Complete ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		}
	}
	
}
