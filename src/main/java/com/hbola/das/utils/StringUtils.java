package com.hbola.das.utils;

import java.util.List;

//TODO: we could move this class to Helper package as StringHelper
public class StringUtils {

	public static String truncateUA(String value) {
		
		String returnValue = value;
		try {
			
			if (value != null) {
				value = value.trim();
				returnValue = value;
		
				if (!value.isEmpty() && value.length() >= 16) {				
					returnValue = value.substring(0, value.length() - 3); 
				}
			}
		} catch (Exception e) {
			returnValue = value;
		}
		
		return returnValue;
	}

	public static String parseInventoryDescriptionForModel(String inventoryExcelDescription) {
	
		String model = null;
		try {
			int firstCommaIndex = inventoryExcelDescription.indexOf(",");
			if (firstCommaIndex > 0) {
				model = inventoryExcelDescription.substring(0, firstCommaIndex);
				model = model.replaceAll("DSR", "DSR-");
			}
		}
		catch (Exception e) {
			model = null;
		}
		
		return model;
	}	
	
	public static String listToDelimitedString(List<String> list) {
				
		String returnValue = "";
		if (list != null && !list.isEmpty()) {
			
			for (String k : list) {				
				returnValue += k + "|";
			}						
		}
		return returnValue;		
	}
	
}
