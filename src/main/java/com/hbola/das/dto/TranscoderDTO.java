package com.hbola.das.dto;

import java.io.Serializable;

public class TranscoderDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8292841905244674074L;

	private int decoderId;
	private int transcoderId;
	private ServiceDTO service;
	private String comments;
	private OperationDTO operation;
	private Boolean hdEnabled; 
	private Boolean sdEnabled; 
	private Boolean passthruEnabled;
	private int decoderServicesIdentity;
	private boolean authorized;
	private String unitAddress;
	private Boolean isUpdated;
	private String serialNumber;
	
	public TranscoderDTO(){}//needed by spring when creating the object using @RequestBody in IrdController
	
	public TranscoderDTO(int transcoderId, ServiceDTO service, String comments, OperationDTO operation, Boolean hdEnabled, Boolean sdEnabled, Boolean passthruEnabled, int decoderServicesIdentity, boolean authorized, String unitAddress) {
		super();
		this.transcoderId = transcoderId;
		this.service = service;
		this.comments = comments;
		this.operation = operation;
		this.hdEnabled = hdEnabled;
		this.sdEnabled = sdEnabled;
		this.passthruEnabled = passthruEnabled;
		this.decoderServicesIdentity = decoderServicesIdentity;
		this.authorized = authorized;
		this.unitAddress = unitAddress;
	}
	
	public TranscoderDTO(int decoderId, int transcoderId, ServiceDTO service, String comments, OperationDTO operation, Boolean hdEnabled, Boolean sdEnabled, Boolean passthruEnabled, int decoderServicesIdentity, boolean authorized, String unitAddress) {
		super();
		this.decoderId = decoderId;
		this.transcoderId = transcoderId;
		this.service = service;
		this.comments = comments;
		this.operation = operation;
		this.hdEnabled = hdEnabled;
		this.sdEnabled = sdEnabled;
		this.passthruEnabled = passthruEnabled;
		this.decoderServicesIdentity = decoderServicesIdentity;
		this.authorized = authorized;
		this.unitAddress = unitAddress;
	}
	
	public TranscoderDTO(int decoderId, int transcoderId, ServiceDTO service, String comments, OperationDTO operation, Boolean hdEnabled, Boolean sdEnabled, Boolean passthruEnabled, int decoderServicesIdentity, boolean authorized, String unitAddress, boolean isUpdated) {
		super();
		this.decoderId = decoderId;
		this.transcoderId = transcoderId;
		this.service = service;
		this.comments = comments;
		this.operation = operation;
		this.hdEnabled = hdEnabled;
		this.sdEnabled = sdEnabled;
		this.passthruEnabled = passthruEnabled;
		this.decoderServicesIdentity = decoderServicesIdentity;
		this.authorized = authorized;
		this.unitAddress = unitAddress;
		this.isUpdated = isUpdated;
	}
	
	
	
	public Boolean getIsUpdated() {
		return isUpdated;
	}

	public void setIsUpdated(Boolean isUpdated) {
		this.isUpdated = isUpdated;
	}

	public int getDecoderId() {
		return decoderId;
	}

	public void setDecoderId(int decoderId) {
		this.decoderId = decoderId;
	}

	public String getUnitAddress() {
		return unitAddress;
	}

	public void setUnitAddress(String unitAddress) {
		this.unitAddress = unitAddress;
	}

	public boolean getAuthorized() {
		return authorized;
	}

	public void setAuthorized(boolean authorized) {
		this.authorized = authorized;
	}

	public int getDecoderServicesIdentity() {
		return decoderServicesIdentity;
	}

	public void setDecoderServicesIdentity(int decoderServicesIdentity) {
		this.decoderServicesIdentity = decoderServicesIdentity;
	}

	public int getTranscoderId() {
		return transcoderId;
	}

	public void setTranscoderId(int transcoderId) {
		this.transcoderId = transcoderId;
	}

	public ServiceDTO getService() {
		return service;
	}

	public void setService(ServiceDTO service) {
		this.service = service;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public OperationDTO getOperation() {
		return operation;
	}

	public void setOperation(OperationDTO operation) {
		this.operation = operation;
	}

	public Boolean isHdEnabled() {
		return hdEnabled;
	}

	public void setHdEnabled(Boolean hdEnabled) {
		this.hdEnabled = hdEnabled;
	}

	public Boolean isSdEnabled() {
		return sdEnabled;
	}

	public void setSdEnabled(Boolean sdEnabled) {
		this.sdEnabled = sdEnabled;
	}

	public Boolean isPassthruEnabled() {
		return passthruEnabled;
	}

	public void setPassthruEnabled(Boolean passthruEnabled) {
		this.passthruEnabled = passthruEnabled;
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}
	
	
}
