package com.hbola.das.dto;

import java.io.Serializable;

public class MsoDTO implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5709821160077552619L;
	
	String code;
	String name;
	String manager;
	String countryId;
	String countryName;
	String address;
	String city;
	String areaCode;
	String areaCode2;
	String phone;
	String phone2;
	String fax;
	String primaryEmail;
	String secondaryEmail;
	String createdDate;
	String updatedDate;
	String updatedBy;
	
	public MsoDTO() {
		super();
	}
	
	public MsoDTO(int id) {
		super();
	}
	
	public MsoDTO(String code, String name, String manager, String countryId, String countryName, String address, String city,
			String areaCode, String areaCode2, String phone, String phone2, String fax, String primaryEmail, String secondaryEmail,
			String createdDate, String updatedDate, String updatedBy) {
		super();
		this.code = code;
		this.name = name;
		this.manager = manager;
		this.countryId = countryId != null? countryId.trim() : null;
		this.countryName = countryName;
		this.address = address;
		this.city = city;
		this.areaCode = areaCode;
		this.areaCode2 = areaCode2;
		this.phone = phone;
		this.phone2 = phone2;
		this.fax = fax;
		this.primaryEmail = primaryEmail;
		this.secondaryEmail = secondaryEmail;
		this.createdDate = createdDate;
		this.updatedDate = updatedDate;
		this.updatedBy = updatedBy;
	}

//	public MsoDTO(String code, String name, String manager, String countryId, String countryName, String address, String city,
//			String areaCode, String phone, String phone2, String fax, String primaryEmail, String secondaryEmail,
//			String createdDate, String updatedDate, String updatedBy) {
//		super();
//		this.code = code;
//		this.name = name;
//		this.manager = manager;
//		this.countryName = countryName;
//		this.address = address;
//		this.city = city;
//		this.areaCode = areaCode;
//		this.phone = phone;
//		this.phone2 = phone2;
//		this.fax = fax;
//		this.primaryEmail = primaryEmail;
//		this.secondaryEmail = secondaryEmail;
//		this.createdDate = createdDate;
//		this.updatedDate = updatedDate;
//		this.updatedBy = updatedBy;
//	}
	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getManager() {
		return manager;
	}

	public void setManager(String manager) {
		this.manager = manager;
	}
	
	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public String getCountryId() {
		return countryId;
	}

	public void setCountryId(String countryId) {
		this.countryId = countryId;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getAreaCode() {
		return areaCode;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}
	
	public String getAreaCode2() {
		return areaCode2;
	}

	public void setAreaCode2(String areaCode2) {
		this.areaCode2 = areaCode2;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getPhone2() {
		return phone2;
	}

	public void setPhone2(String phone2) {
		this.phone2 = phone2;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getPrimaryEmail() {
		return primaryEmail;
	}

	public void setPrimaryEmail(String primaryEmail) {
		this.primaryEmail = primaryEmail;
	}

	public String getSecondaryEmail() {
		return secondaryEmail;
	}

	public void setSecondaryEmail(String secondaryEmail) {
		this.secondaryEmail = secondaryEmail;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(String updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
}
