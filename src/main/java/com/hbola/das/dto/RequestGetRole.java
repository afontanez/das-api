package com.hbola.das.dto;

public class RequestGetRole extends ARequest{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4779663874193627345L;
	String username;
	public RequestGetRole() {
		super();
	}
	
	public RequestGetRole(String token) {
		super(token);
		// TODO Auto-generated constructor stub
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
}
