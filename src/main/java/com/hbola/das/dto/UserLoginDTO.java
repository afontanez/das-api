package com.hbola.das.dto;

import java.io.Serializable;

public class UserLoginDTO implements Serializable{

	private static final long serialVersionUID = -2113465130341074733L;
	String username;
	String password;
	String roleName;
	String token;
	
	public UserLoginDTO() {}
	
	public UserLoginDTO(String username, String password) {
		super();
		this.username = username;
		this.password = password;
	}
	
	public UserLoginDTO(String username, String password, String roleName) {
		super();
		this.username = username;
		this.password = password;
		this.roleName = roleName;
	}
	
	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
	
	
}
