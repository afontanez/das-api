package com.hbola.das.dto;

import java.io.Serializable;

public class CountryDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5885315720146980057L;

	int id;
	String name;
	public CountryDTO(int id, String name) {
		super();
		this.id = id;
		this.name = name;
	}
	public CountryDTO() {
		super();
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
