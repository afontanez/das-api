package com.hbola.das.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.hbola.das.utils.StringUtils;

public class IrdBNCDTO implements Serializable{

	private static final long serialVersionUID = 4416516498567572727L;
	
	private Boolean frontPanelEnabled;
    private Boolean controlPortEnabled;//localControl in BNC 3.5
    private Integer transportStreamOutput;
    private Integer sdiOutput;
	private String modelNumber;
	private String presetId;
	private String decimalUnitAddress;
	private String secondaryDecimalUnitAdress;
	private String originalDecimalUnitAddress;
	private String originalSecondaryUnitAddress;
	private String iRTUnitAddress;
	private String iRTSerialNumber;
	private String name;
	private List<TierDTO> tiers;
	private List<ServiceBNCDTO> services;
	
	public IrdBNCDTO() {}
	
	public IrdBNCDTO(IrdEditDTO ird) {
		
		this.frontPanelEnabled = ird.getFrontPanelEnabled(); 
	    this.controlPortEnabled = ird.getControlPortEnabled();
	    this.transportStreamOutput = ird.getTransportStreamOutput();
	    this.sdiOutput = ird.getSdiOutput();
		
		this.modelNumber = ird.getModelName();
		this.presetId = Integer.toString(ird.getBncPresetId());
		
		this.originalDecimalUnitAddress = ird.getUnitAddress();
		this.originalSecondaryUnitAddress = ird.getSecondaryUnitAddress();
		
		this.decimalUnitAddress = StringUtils.truncateUA(ird.getUnitAddress()); 
		this.secondaryDecimalUnitAdress = StringUtils.truncateUA(ird.getSecondaryUnitAddress());
		
		this.name = ird.getSerialNumber();
		
		//irt values are all ready 13 digits so no need to truncate
		this.iRTUnitAddress = ird.getIRTUnitAddress();
		this.iRTSerialNumber = ird.getIRTSerialNumber();
		
		this.tiers = new ArrayList<TierDTO>();
		this.services = new ArrayList<ServiceBNCDTO>();
		
		for(TranscoderDTO t : ird.getTranscoders())
		{
			
			String transcoderOperation = "create";
			if (t.getOperation() == null) transcoderOperation = "delete";
			else if (!t.getOperation().isAuthorized()) transcoderOperation = "delete";//we know it's not null so no need for a null check 
			
			if (t.getService() != null) {
			
				if(!tiers.contains(t.getService().getTierNumber()))
				{
					tiers.add(new TierDTO(t.getService().getTierNumber(), transcoderOperation, t.getService().getIsTemplateTierExtended(), t.getService().getTemplateId()));
				} 
				else
				{
					TierDTO tier = tiers.get(tiers.indexOf(t.getService().getTierNumber()));
					if(t.getOperation() != null && t.getOperation().isAuthorized() && tier.getOperation().equalsIgnoreCase("delete"))
					{
						tier.setOperation("create");
					}
				}
				
				this.services.add(new ServiceBNCDTO(String.valueOf(t.getTranscoderId()), 
													String.valueOf(t.getService().getVchNumber()), 
													String.valueOf(t.getService().getVchTable()), 
													transcoderOperation, 
													t.isHdEnabled(), 
													t.isSdEnabled(), 
													t.isPassthruEnabled()));
			}
			else {//service is null so make an empty service just so hd, sd, and passthru can be set to false in BNC
				this.services.add(new ServiceBNCDTO(String.valueOf(t.getTranscoderId()), 
						null, 
						null, 
						"delete", 
						false, 
						false, 
						false));
			}
		}
	}

	
	
	public Boolean getFrontPanelEnabled() {
		return frontPanelEnabled;
	}

	public void setFrontPanelEnabled(Boolean frontPanelEnabled) {
		this.frontPanelEnabled = frontPanelEnabled;
	}

	public Boolean getControlPortEnabled() {
		return controlPortEnabled;
	}

	public void setControlPortEnabled(Boolean controlPortEnabled) {
		this.controlPortEnabled = controlPortEnabled;
	}

	public Integer getTransportStreamOutput() {
		return transportStreamOutput;
	}

	public void setTransportStreamOutput(Integer transportStreamOutput) {
		this.transportStreamOutput = transportStreamOutput;
	}

	public Integer getSdiOutput() {
		return sdiOutput;
	}

	public void setSdiOutput(Integer sdiOutput) {
		this.sdiOutput = sdiOutput;
	}

	public String getOriginalDecimalUnitAddress() {
		return originalDecimalUnitAddress;
	}

	public void setOriginalDecimalUnitAddress(String originalDecimalUnitAddress) {
		this.originalDecimalUnitAddress = originalDecimalUnitAddress;
	}

	public String getOriginalSecondaryUnitAddress() {
		return originalSecondaryUnitAddress;
	}

	public void setOriginalSecondaryUnitAddress(String originalSecondaryUnitAddress) {
		this.originalSecondaryUnitAddress = originalSecondaryUnitAddress;
	}

	public String getModelNumber() {
		return modelNumber;
	}

	public void setModelNumber(String modelNumber) {
		this.modelNumber = modelNumber;
	}

	public String getPresetId() {
		return presetId;
	}

	public void setPresetId(String presetId) {
		this.presetId = presetId;
	}

	public String getDecimalUnitAddress() {
		return decimalUnitAddress;
	}

	public void setDecimalUnitAddress(String decimalUnitAddress) {
		this.decimalUnitAddress = decimalUnitAddress;
	}

	public String getSecondaryDecimalUnitAdress() {
		return secondaryDecimalUnitAdress;
	}

	public void setSecondaryDecimalUnitAdress(String secondaryDecimalUnitAdress) {
		this.secondaryDecimalUnitAdress = secondaryDecimalUnitAdress;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<TierDTO> getTiers() {
		return tiers;
	}

	public void setTiers(List<TierDTO> tiers) {
		this.tiers = tiers;
	}

	public List<ServiceBNCDTO> getServices() {
		return services;
	}

	public void setServices(List<ServiceBNCDTO> services) {
		this.services = services;
	}

	public String getIRTUnitAddress() {
		return iRTUnitAddress;
	}

	public void setIRTUnitAddress(String iRTUnitAddress) {
		this.iRTUnitAddress = iRTUnitAddress;
	}

	public String getIRTSerialNumber() {
		return iRTSerialNumber;
	}

	public void setIRTSerialNumber(String iRTSerialNumber) {
		this.iRTSerialNumber = iRTSerialNumber;
	}
	
	
	
}

