package com.hbola.das.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class IrdSummaryDTO implements Serializable{

	private static final long serialVersionUID = -5488441078484097702L;

	protected int decoderId;
	protected String modelNumber;
	protected String serialNumber;
	protected List<String> unitAddress = new ArrayList<String>();
	protected String cableOperator;
	protected String headend;
	protected String country;
	protected String encoder;
	private boolean inventory;
	private int ownerShipId;
	private String ownerShipName;
	protected List<String> services = new ArrayList<String>();
	protected List<String> operationsList = new ArrayList<String>();
	//protected Map<String, String> services = new HashMap<String, String>();
	private String operations = "";
	private String authorizations ="";
	protected List<TranscoderDTO> transcoders = new ArrayList<TranscoderDTO>();
	private String searchUA1;
	private String searchUA2;

	public IrdSummaryDTO(int decoderId, String modelNumber, String serialNumber, List<String> unitAddress,
			String cableOperator, String headend, String country, String encoder, boolean inventory, int ownerShipId,
			String ownerShipName, List<String> services/*Map<String, String> services*/, List<TranscoderDTO> transcoders) {
		super();
		this.decoderId = decoderId;
		this.modelNumber = modelNumber;
		this.serialNumber = serialNumber;
		this.unitAddress = unitAddress;
		this.cableOperator = cableOperator;
		this.headend = headend;
		this.country = country;
		this.encoder = encoder;
		this.inventory = inventory;
		this.ownerShipId = ownerShipId;
		this.ownerShipName = ownerShipName;
		this.setServices(services);
		this.transcoders = transcoders;
		
		//this unit address are used exclusively in the UI search page we had to flatten out the unit address list so we can filter as you type
		if (this.unitAddress != null) {
			this.searchUA1 = this.unitAddress.get(0) != null ? this.unitAddress.get(0) : ""; 
			this.searchUA2 = this.unitAddress.get(1) != null ? this.unitAddress.get(1) : "";
		}
		
	}
	
	public IrdSummaryDTO(IrdEditDTO ird) {		
		
		this(ird.getId(), ird.getModelName(), ird.getSerialNumber(), null,
		ird.getCableOperatorCode()+"-"+ird.getCableOperatorName(), ird.getHeandName(), ird.getCountryName(), null, ird.isInventory(), ird.getOwnershipId(),
		ird.getOwnershipName(), null, ird.getTranscoders());
		
		List<String> unitAddress = new ArrayList<String>();
		
		unitAddress.add(ird.getUnitAddress());
		unitAddress.add(ird.getSecondaryUnitAddress());
		
		this.setUnitAddress(unitAddress);
		
		try {
			setEncoder(ird.getTranscoders().get(0).getService().getEncoderName());
		}
		catch (Exception e) {
			setEncoder("Unable To Find Encoder");
		}
				
		List<String> services = new ArrayList<String>();
//		Map<String, String> services = new HashMap<String, String>();
		int i = 1;
		for (TranscoderDTO transc : transcoders) {						
			services.add(i + "-" + transc.getService().getTierName());
			//services.put(i + "-" + transc.getService().getTierName(), transc.getOperation().getDescription());
			i++;
		}
		this.setServices(services);
		
		
		
	}

	
	public int getDecoderId() {
		return decoderId;
	}


	public void setDecoderId(int decoderId) {
		this.decoderId = decoderId;
	}

	public String getSerialNumber() {
		return serialNumber;
	}


	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public String getModelNumber() {
		return modelNumber;
	}


	public void setModelNumber(String modelNumber) {
		this.modelNumber = modelNumber;
	}


	public List<String> getUnitAddress() {
		return unitAddress;
	}


	public void setUnitAddress(List<String> unitAddress) {
		this.unitAddress = unitAddress;
		
		//this unit address are used exclusively in the UI search page we had to flatten out the unit address list so we can filter as you type
		if (this.unitAddress != null) {
			this.searchUA1 = this.unitAddress.get(0) != null ? this.unitAddress.get(0) : ""; 
			this.searchUA2 = this.unitAddress.get(1) != null ? this.unitAddress.get(1) : "";
		}
	}


	public String getCableOperator() {
		return cableOperator;
	}


	public void setCableOperator(String cableOperator) {
		this.cableOperator = cableOperator;
	}


	public String getHeadend() {
		return headend;
	}


	public void setHeadend(String headend) {
		this.headend = headend;
	}


	public String getCountry() {
		return country;
	}


	public void setCountry(String country) {
		this.country = country;
	}

	public String getEncoder() {
		return encoder;
	}


	public void setEncoder(String encoder) {
		this.encoder = encoder;
	}


	public List<String> getServices() {
		if(this.services == null)
			this.services = new ArrayList<String>();
		return services;
	}
//	public Map<String, String> getServices() {
//		if (this.services == null) {
//			this.services = new HashMap<String, String>();
//		}
//		return services;
//	}

	public void setServices(List<String> services) {
		this.services = services;
	}
//	public void setServices(Map<String, String> services) {
//		this.services = services;
//		
//	}

	public List<String> getOperationsList() {
		if(this.operationsList == null)
			this.operationsList = new ArrayList<String>();
		return operationsList;
	}
	public void setOperationsList(List<String> operationsList) {
		this.operationsList = operationsList;
	}

	
	public List<TranscoderDTO> getTranscoders() {
		if(this.transcoders == null)
			this.transcoders = new ArrayList<TranscoderDTO>();
		return transcoders;
	}

	public void setTranscoders(List<TranscoderDTO> transcoders) {
		this.transcoders = transcoders;
	}

	public boolean isInventory() {
		return inventory;
	}

	public void setInventory(boolean inventory) {
		this.inventory = inventory;
	}

	public int getOwnerShipId() {
		return ownerShipId;
	}

	public void setOwnerShipId(int ownerShipId) {
		this.ownerShipId = ownerShipId;
	}

	public String getOwnerShipName() {
		return ownerShipName;
	}

	public void setOwnerShipName(String ownerShipName) {
		this.ownerShipName = ownerShipName;
	}

	public String getSearchUA1() {
		return searchUA1;
	}

	public void setSearchUA1(String searchUA1) {
		this.searchUA1 = searchUA1;
	}

	public String getSearchUA2() {
		return searchUA2;
	}

	public void setSearchUA2(String searchUA2) {
		this.searchUA2 = searchUA2;
	}

	public String getOperations() {
		return operations;
	}

	public void setOperations(String operations) {
		this.operations = operations;
	}

	public String getAuthorizations() {
		return authorizations;
	}

	public void setAuthorizations(String authorizations) {
		this.authorizations = authorizations;
	}
	
}
