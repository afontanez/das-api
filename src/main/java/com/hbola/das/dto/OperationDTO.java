package com.hbola.das.dto;

import java.io.Serializable;

public class OperationDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8844402960076372394L;
	
	private int id;
	private String description;
	private boolean authorized;
	private String startDate;
	private String endDate;
	
	public OperationDTO(){}//needed by spring when creating the object using @RequestBody in IrdController
	
	public OperationDTO(int id, String description, boolean authorized, String startDate, String endDate) {
		super();
		this.id = id;
		this.description = description;
		this.authorized = authorized;
		this.startDate = startDate;
		this.endDate = endDate;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public boolean isAuthorized() {
		return authorized;
	}

	public void setAuthorized(boolean isAuthorized) {
		this.authorized = isAuthorized;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
}
