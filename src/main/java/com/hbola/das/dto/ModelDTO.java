package com.hbola.das.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ModelDTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -971344689690054595L;
	
	private String name;
	private int numberOfSelectableServices;
	private int implementsTemplate;
	private List<FirmwareVersionDTO> firmwareVersions = new ArrayList<FirmwareVersionDTO>();
	private List<PresetDTO> presets = new ArrayList<PresetDTO>();
	
	
	public ModelDTO(String name, int numberOfSelectableServices, int implementsTemplate) {
		super();
		
		this.name = name;
		this.numberOfSelectableServices = numberOfSelectableServices;
		this.implementsTemplate = implementsTemplate;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getNumberOfSelectableServices() {
		return numberOfSelectableServices;
	}

	public void setNumberOfSelectableServices(int numberOfSelectableServices) {
		this.numberOfSelectableServices = numberOfSelectableServices;
	}

	public int getImplementsTemplate() {
		return implementsTemplate;
	}

	public void setImplementsTemplate(int implementsTemplate) {
		this.implementsTemplate = implementsTemplate;
	}

	public List<FirmwareVersionDTO> getFirmwareVersions() {
		return firmwareVersions;
	}

	public void setFirmwareVersions(List<FirmwareVersionDTO> firmwareVersions) {
		this.firmwareVersions = firmwareVersions;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public List<PresetDTO> getPresets() {
		return presets;
	}

	public void setPresets(List<PresetDTO> presets) {
		this.presets = presets;
	}
}
