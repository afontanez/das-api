package com.hbola.das.dto;

import java.io.Serializable;

public class TierDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7955273610874611556L;
	
	String number;
	String operation;
	Integer isTemplateTierExtended;
	Integer templateId;
	
	public TierDTO() {}
	
	public TierDTO(String number, String operation, Integer isTemplateTierExtended, Integer templateId) {
		super();
		this.number = number;
		this.operation = operation;
		this.isTemplateTierExtended = isTemplateTierExtended;
		this.templateId = templateId;
	}
	
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public String getOperation() {
		return operation;
	}
	public void setOperation(String operation) {
		this.operation = operation;
	}	
}
