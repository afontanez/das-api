package com.hbola.das.dto;

import java.io.Serializable;

public enum MESSAGE_CODE implements Serializable{

	SUCCESS(0), 
	INTERNAL_ERROR(100),
	BNC_COMUNICATION_ERROR(101),
	BNC_PROPERTY_ERROR(102),
	
	LOGIN_ERROR(201),
	USER_DB_ERROR(202),//User not found in DB
	USER_ROLE_ERROR(203),//Role not associate to User in DB
	LDAP_AUTHENTICATE_LOGIN_ERROR(204);
	
	private int value;
	MESSAGE_CODE(int value){
		this.value = value;
	}
	
	public int getValue() { return this.value; }	
	
	public String toString()
	{
		return String.valueOf(getValue());
	}
}
