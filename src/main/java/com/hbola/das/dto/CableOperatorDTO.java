package com.hbola.das.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class CableOperatorDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6447404288783195890L;

	private String code;
	private String name;
	private String msoCode;
	private String companyName;
	private String countryId;
	private String countryName;
	private String manager;
	private String address;
	private String city;
	private String areaCode;
	private String areaCode2;
	private String phone;
	private String phone2;
	private String fax;
	private String postalCode;
	private String email;
	private String secondaryEmail;
	private int headendSeed;
	private String auxiliaryRefCode;
	private String createdDate;
	private String updatedDate;
	private String updatedBy;
	private boolean inventoryHolder;
	private boolean active;
	private int appVersion;
	private boolean newCable;
	private List<HeadendDTO> headEnds = new ArrayList<HeadendDTO>();

	public CableOperatorDTO(String code, String name) {
		this.code = code;
		this.name = name;
	}

	public CableOperatorDTO(String code, String name, String msoCode, String companyName, String countryId,
			String countryName, String manager, String address, String city, String areaCode, String areaCode2,
			String phone, String phone2, String fax, String postalCode, String email, String secondaryEmail,
			int headendSeed, String auxiliaryRefCode, String createdDate, String updatedDate, String updatedBy,
			boolean inventoryHolder, boolean active, int appVersion, List<HeadendDTO> headEnds) {
		super();
		this.code = code;
		this.name = name;
		this.msoCode = msoCode;
		this.companyName = companyName;
		this.countryId = countryId != null ? countryId.trim() : null;
		this.countryName = countryName;
		this.manager = manager;
		this.address = address;
		this.city = city;
		this.areaCode = areaCode;
		this.areaCode2 = areaCode2;
		this.phone = phone;
		this.phone2 = phone2;
		this.fax = fax;
		this.postalCode = postalCode;
		this.email = email;
		this.secondaryEmail = secondaryEmail;
		this.headendSeed = headendSeed;
		this.auxiliaryRefCode = auxiliaryRefCode;
		this.createdDate = createdDate;
		this.updatedDate = updatedDate;
		this.updatedBy = updatedBy;
		this.inventoryHolder = inventoryHolder;
		this.active = active;
		this.appVersion = appVersion;
		this.headEnds = headEnds;
	}

	public CableOperatorDTO() {
		// TODO Auto-generated constructor stub
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<HeadendDTO> getHeadEnds() {
		return headEnds;
	}

	public void setHeadEnds(List<HeadendDTO> headEnds) {
		this.headEnds = headEnds;
	}

	public String getMsoCode() {
		return msoCode;
	}

	public void setMsoCode(String msoCode) {
		this.msoCode = msoCode;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCountryId() {
		return countryId;
	}

	public void setCountryId(String countryId) {
		this.countryId = countryId;
	}

	public String getManager() {
		return manager;
	}

	public void setManager(String manager) {
		this.manager = manager;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getAreaCode() {
		return areaCode;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}

	public String getAreaCode2() {
		return areaCode2;
	}

	public void setAreaCode2(String areaCode2) {
		this.areaCode2 = areaCode2;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getPhone2() {
		return phone2;
	}

	public void setPhone2(String phone2) {
		this.phone2 = phone2;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSecondaryEmail() {
		return secondaryEmail;
	}

	public void setSecondaryEmail(String secondaryEmail) {
		this.secondaryEmail = secondaryEmail;
	}

	public int getHeadendSeed() {
		return headendSeed;
	}

	public void setHeadendSeed(int headendSeed) {
		this.headendSeed = headendSeed;
	}

	public String getAuxiliaryRefCode() {
		return auxiliaryRefCode;
	}

	public void setAuxiliaryRefCode(String auxiliaryRefCode) {
		this.auxiliaryRefCode = auxiliaryRefCode;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(String updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public boolean isInventoryHolder() {
		return inventoryHolder;
	}

	public void setInventoryHolder(boolean inventoryHolder) {
		this.inventoryHolder = inventoryHolder;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public int getAppVersion() {
		return appVersion;
	}

	public void setAppVersion(int appVersion) {
		this.appVersion = appVersion;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public boolean isNewCable() {
		return newCable;
	}

	public void setNewCable(boolean newCable) {
		this.newCable = newCable;
	}

}
