package com.hbola.das.dto;

import java.util.List;

public class FatTierDTO {
	
	Integer encoderId;
	Integer fatTier;
	List<Integer> tiers;
	Integer isTierExtended;
	
	public FatTierDTO(){}
	public FatTierDTO(Integer encoderId, Integer fatTier, Integer isTierExtended) {
		this.encoderId = encoderId;
		this.fatTier = fatTier;
		this.isTierExtended = isTierExtended;
	}
	
	public Integer getEncoderId() {
		return encoderId;
	}
	public void setEncoderId(Integer encoderId) {
		this.encoderId = encoderId;
	}
	public Integer getFatTier() {
		return fatTier;
	}
	public void setFatTier(Integer fatTier) {
		this.fatTier = fatTier;
	}
	public List<Integer> getTiers() {
		return tiers;
	}
	public void setTiers(List<Integer> tiers) {
		this.tiers = tiers;
	}
	public Integer getIsTierExtended() {
		return isTierExtended;
	}
	public void setIsTierExtended(Integer isTierExtended) {
		this.isTierExtended = isTierExtended;
	}

	
}
