package com.hbola.das.dto;

import java.io.Serializable;

public abstract class ARequest implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 8772411539642042779L;
	String token ;
	public ARequest() {
		super();
	}

	public ARequest(String token) {
		super();
		this.token = token;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
}
