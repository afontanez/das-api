package com.hbola.das.dto;

import java.util.Date;

public class RequestDecodersFiltered extends ARequest {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2534226118564592087L;
	Date initDate;
	Date endDate;
	Boolean isInventory;
	
	public RequestDecodersFiltered() {
		super();
	}

	public RequestDecodersFiltered(Date initDate, Date endDate) {
		super();
		this.initDate = initDate;
		this.endDate = endDate;
	}

	public Date getInitDate() {
		return initDate;
	}

	public void setInitDate(Date initDate) {
		this.initDate = initDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Boolean getIsInventory() {
		return isInventory;
	}

	public void setIsInventory(Boolean isInventory) {
		this.isInventory = isInventory;
	}
	
	
}
