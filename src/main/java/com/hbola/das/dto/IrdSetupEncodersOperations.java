package com.hbola.das.dto;

import java.util.ArrayList;
import java.util.List;

public class IrdSetupEncodersOperations extends IrdSetup{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3730729110394693856L;
	
	List<EncoderDTO> encoders;
	List<OperationDTO> operations;
	
	public IrdSetupEncodersOperations() {
		super();
		this.operations = new ArrayList<OperationDTO>();
		this.encoders = new ArrayList<EncoderDTO>();
	}

	public IrdSetupEncodersOperations(List<EncoderDTO> encoders,
			List<OperationDTO> operations) {
		super();
		this.encoders = encoders;
		this.operations = operations;
	}

	public List<OperationDTO> getOperations() {
		return operations;
	}

	public void setOperations(List<OperationDTO> operations) {
		this.operations = operations;
	}

	public List<EncoderDTO> getEncoders() {
		return encoders;
	}

	public void setEncoders(List<EncoderDTO> encoders) {
		this.encoders = encoders;
	}	
}
