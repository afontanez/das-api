package com.hbola.das.dto;

import java.io.Serializable;


public class ServiceDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5783940122796347037L;
	
	private Integer encoderId;
	private String encoderName;
	private String tierNumber;
	private String tierName;
	private Integer vchNumber; 
	private Integer vchTable;
	private Integer isTemplateTierExtended;
	private Integer templateId;
	
	public ServiceDTO(){} //spring required a default constructor when attempting the post

	public ServiceDTO(Integer encoderId, String tierNumber, String tierName, String vchNumber, Integer vchTable) {
		this.encoderId = encoderId;
		this.tierNumber = tierNumber;
		this.tierName = tierName;
		this.setVchNumber(vchNumber);
		this.vchTable = vchTable;
	}
	
	public ServiceDTO(Integer encoderId, String encoderName, String tierNumber, String tierName, String vchNumber, Integer vchTable) {
		this.encoderId = encoderId;
		this.encoderName = encoderName;
		this.tierNumber = tierNumber;
		this.tierName = tierName;
		this.setVchNumber(vchNumber);
		this.vchTable = vchTable;
	}
	
	public ServiceDTO(Integer encoderId, String tierNumber, String tierName, String vchNumber, Integer vchTable, Integer isTemplateTierExtended, Integer templateId) {
		this.encoderId = encoderId;
		this.tierNumber = tierNumber;
		this.tierName = tierName;
		this.setVchNumber(vchNumber);
		this.vchTable = vchTable;
		this.isTemplateTierExtended = isTemplateTierExtended;
		this.templateId = templateId;
	}
	
	public ServiceDTO(String tierNumber) {
		this.tierNumber = tierNumber;
	}
		
	
	public Integer getTemplateId() {
		return templateId;
	}

	public void setTemplateId(Integer templateId) {
		this.templateId = templateId;
	}

	public Integer getIsTemplateTierExtended() {
		return isTemplateTierExtended;
	}

	public void setIsTemplateTierExtended(Integer isTemplateTierExtended) {
		this.isTemplateTierExtended = isTemplateTierExtended;
	}

	public Integer getEncoderId() {
		return encoderId;
	}

	public void setEncoderId(Integer encoderId) {
		this.encoderId = encoderId;
	}

	public String getEncoderName() {
		return encoderName;
	}

	public void setEncoderName(String encoderName) {
		this.encoderName = encoderName;
	}

	public String getTierNumber() {
		return tierNumber;
	}

	public void setTierNumber(String tierNumber) {
		this.tierNumber = tierNumber;
	}

	public String getTierName() {
		return tierName;
	}

	public void setTierName(String tierName) {
		this.tierName = tierName;
	}

	public Integer getVchNumber() {
		return vchNumber;
	}

	public void setVchNumber(String vchNumber) {
		
		if (vchNumber != null) {
			try {
				this.vchNumber = Integer.parseInt(vchNumber);
			}
			catch (Exception e) {
				this.vchNumber = 0;
			}
		}
		else {
			this.vchNumber = 0;
		}
		
	}

	public Integer getVchTable() {
		return vchTable;
	}

	public void setVchTable(Integer vchTable) {
		this.vchTable = vchTable;
	}

}
