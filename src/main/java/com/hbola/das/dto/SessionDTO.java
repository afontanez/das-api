package com.hbola.das.dto;

import java.io.Serializable;
import java.util.Date;

public class SessionDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4903978707865540280L;
	String userId;
	String token;
	String username;
	String roleName;
	String email;
	boolean approved;
	Date createdDate;
	Date expirationDate;
	
	public SessionDTO() {
		super();
	}
	
	public SessionDTO(String userId, String token, String username,  String roleName, String email, boolean approved, Date createdDate,
			Date expirationDate) {
		super();
		this.userId = userId;
		this.token = token;
		this.username = username;
		this.roleName = roleName;
		this.email = email;
		this.approved = approved;
		this.createdDate = createdDate;
		this.expirationDate = expirationDate;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public boolean isApproved() {
		return approved;
	}

	public void setApproved(boolean approved) {
		this.approved = approved;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}
}
