package com.hbola.das.dto;

public class ActionIrdDTO {

	String action;
	int irdId;
	
	public ActionIrdDTO(String action, int irdId) {
		super();
		this.action = action;
		this.irdId = irdId;
	}
	public ActionIrdDTO() {
		super();
		this.action = "";
		this.irdId = 0;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public int getIrdId() {
		return irdId;
	}

	public void setIrdId(int irdId) {
		this.irdId = irdId;
	}
	
}
