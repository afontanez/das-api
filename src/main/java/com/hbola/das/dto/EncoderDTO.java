package com.hbola.das.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class EncoderDTO implements Serializable {

	private static final long serialVersionUID = 4169883031222848525L;

	int id;
	
	int implementsTemplate;
	String name;
	List<ServiceDTO> services;

	public EncoderDTO(int id, int implementsTemplate, String name) {
		super();
		this.id = id;
		this.implementsTemplate = implementsTemplate;
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getImplementsTemplate() {
		return implementsTemplate;
	}

	public void setImplementsTemplate(int implementsTemplate) {
		this.implementsTemplate = implementsTemplate;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<ServiceDTO> getServices() {
		if (services == null)
			services = new ArrayList<ServiceDTO>();
		return services;
	}

	public void setServices(List<ServiceDTO> services) {
		this.services = services;
	}
}
