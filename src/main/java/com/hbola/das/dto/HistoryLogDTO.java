package com.hbola.das.dto;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

public class HistoryLogDTO {

	private static final SimpleDateFormat SDF = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
	private String unitAddress;
	private String createdOnFormatted;
	private Timestamp createdOn;
	private String operation;
	private String createdBy;
	private String comments;
	private String service;
	private boolean hd;
	private boolean sd;
	private boolean passthru;	
	
	public HistoryLogDTO(String unitAddress, Timestamp createdOn, String operation, String createdBy,
			String comments, String service, boolean hd, boolean sd, boolean passthru) {
		super();
		
		this.unitAddress = unitAddress;
		this.createdOn = createdOn;
		this.operation = operation;
		this.createdBy = createdBy;
		this.comments = comments;

		try {
			Date date = new Date(createdOn.getTime());			
			this.createdOnFormatted = SDF.format(date);
		} 
		catch (Exception e) {
			this.createdOnFormatted = null;
		}
		this.service = service;
		this.hd = hd;
		this.sd = sd;
		this.passthru = passthru;
	}



	public String getCreatedOnFormatted() {
		return createdOnFormatted;
	}

	public void setCreatedOnFormatted(String createdOnFormatted) {
		this.createdOnFormatted = createdOnFormatted;
	}

	public Timestamp getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getService() {
		return service;
	}

	public void setService(String service) {
		this.service = service;
	}

	public boolean isHd() {
		return hd;
	}

	public void setHd(boolean hd) {
		this.hd = hd;
	}

	public boolean isSd() {
		return sd;
	}

	public void setSd(boolean sd) {
		this.sd = sd;
	}

	public boolean isPassthru() {
		return passthru;
	}

	public void setPassthru(boolean passthru) {
		this.passthru = passthru;
	}



	public String getUnitAddress() {
		return unitAddress;
	}



	public void setUnitAddress(String unitAddress) {
		this.unitAddress = unitAddress;
	}
	
	
}
