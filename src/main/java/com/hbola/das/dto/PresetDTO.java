package com.hbola.das.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class PresetDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2852902642857238835L;
	
	private int id;
	private int bncPresetId;
	private String name;
	private int encoderId;
	private String modelName;
	private List<TranscoderDTO> transcodersPreloaded;

	public PresetDTO() {
		super();
	}

	public PresetDTO(int id, int bncPresetId, String name, int encoderId, String modelName,
			List<TranscoderDTO> transcodersPreloaded) {
		super();
		this.id = id;
		this.bncPresetId = bncPresetId;
		this.name = name;
		this.encoderId = encoderId;
		this.modelName = modelName;
		this.transcodersPreloaded = transcodersPreloaded;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getEncoderId() {
		return encoderId;
	}

	public void setEncoderId(int encoderId) {
		this.encoderId = encoderId;
	}

	public String getModelName() {
		return modelName;
	}

	public void setModelName(String modelName) {
		this.modelName = modelName;
	}

	public int getBncPresetId() {
		return bncPresetId;
	}

	public void setBncPresetId(int bncPresetId) {
		this.bncPresetId = bncPresetId;
	}

	public List<TranscoderDTO> getTranscodersPreloaded() {
		if(this.transcodersPreloaded == null)
			this.transcodersPreloaded = new ArrayList<TranscoderDTO>();
		return transcodersPreloaded;
	}

	public void setTranscodersPreloaded(List<TranscoderDTO> transcodersPreloaded) {
		this.transcodersPreloaded = transcodersPreloaded;
	}

}
