package com.hbola.das.dto;

import java.io.Serializable;

public class ServiceBNCDTO implements Serializable{

	private static final long serialVersionUID = 4720336765163582237L;
	
	private String transcoderID;
	private String vcNumber;
	private String vcTable;
	private String operation;
	private Boolean hdEnable;
	private Boolean sdEnable;
	private Boolean passthruEnable;
	//private String unitAddress;//for the 4410MD
	
	public ServiceBNCDTO(String transcoderID, String vcNumber, String vcTable, String operation, Boolean hdEnable, Boolean sdEnable, Boolean passthruEnable) {//, String unitAddress) {
		this.transcoderID = transcoderID;
		this.vcNumber = vcNumber;
		this.vcTable = vcTable;
		this.operation = operation;
		this.hdEnable = hdEnable;
		this.sdEnable = sdEnable;
		this.passthruEnable = passthruEnable;
	//	this.unitAddress = unitAddress;
	}

	public String getTranscoderID() {
		return transcoderID;
	}

	public void setTranscoderID(String transcoderID) {
		this.transcoderID = transcoderID;
	}

	public String getVcNumber() {
		return vcNumber;
	}

	public void setVcNumber(String vcNumber) {
		this.vcNumber = vcNumber;
	}

	public String getVcTable() {
		return vcTable;
	}

	public void setVcTable(String vcTable) {
		this.vcTable = vcTable;
	}

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	public Boolean isHdEnable() {
		return hdEnable;
	}

	public void setHdEnable(Boolean hdEnable) {
		this.hdEnable = hdEnable;
	}

	public Boolean isSdEnable() {
		return sdEnable;
	}

	public void setSdEnable(Boolean sdEnable) {
		this.sdEnable = sdEnable;
	}

	public Boolean isPassthruEnable() {
		return passthruEnable;
	}

	public void setPassthruEnable(Boolean passthruEnable) {
		this.passthruEnable = passthruEnable;
	}	
}
