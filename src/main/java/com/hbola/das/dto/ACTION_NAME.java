package com.hbola.das.dto;

public class ACTION_NAME{

	public static String ACTION_SEARCH_DECODER = "Search Decoder";
	public static String ACTION_VIEW_DECODER = "View Decoder";
	public static String ACTION_CREATE_DECODER = "Create Decoder";
	public static String ACTION_CREATE_DECODER_INVENTORY = "Create Decoder - Inventory";
	public static String ACTION_EDIT_DECODER = "Edit Decoder";
	public static String ACTION_LOGIN = "Login";
	public static String ACTION_TRIP = "Instant Trip";

	public static String ACTION_CREATE_MSO = "Create MSO";
	public static String ACTION_EDIT_MSO = "Edit MSO";
	public static String ACTION_VIEW_MSO = "View MSO";
	
	public static String ACTION_CREATE_CABLE_OPERATOR = "Create Cable Operator";
	public static String ACTION_EDIT_CABLE_OPERATOR = "Edit Cable Operator";
	public static String ACTION_VIEW_CABLE_OPERATOR = "View Cable Operator";
	

	public static String ACTION_CREATE_HEADEND = "Create Headend";
	public static String ACTION_EDIT_HEADEND = "Edit Headend";
	public static String ACTION_VIEW_HEADEND = "View Headend";
	
}
