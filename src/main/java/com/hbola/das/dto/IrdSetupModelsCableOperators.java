package com.hbola.das.dto;

import java.util.List;

public class IrdSetupModelsCableOperators extends IrdSetup{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7915018315783397888L;
	
	List<ModelDTO> models;
	List<CableOperatorDTO> cableOperators;
	
	public IrdSetupModelsCableOperators(List<ModelDTO> models, List<CableOperatorDTO> cableOperators) {
		super();
		this.models = models;
		this.cableOperators = cableOperators;
	}

	public List<ModelDTO> getModels() {
		return models;
	}

	public void setModels(List<ModelDTO> models) {
		this.models = models;
	}

	public List<CableOperatorDTO> getCableOperators() {
		return cableOperators;
	}

	public void setCableOperators(List<CableOperatorDTO> cableOperators) {
		this.cableOperators = cableOperators;
	}
}
