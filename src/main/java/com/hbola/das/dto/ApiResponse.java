package com.hbola.das.dto;

public class ApiResponse {

	public static final String OK = "OK";
	public static final String ERROR = "ERROR";
	
	private String status;
	private Object data;
	
	public ApiResponse(){}
	
	public ApiResponse(String status, Object data) {
		this.status = status;
		this.data = data;		
	}


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}


	public Object getData() {
		return data;
	}


	public void setData(Object data) {
		this.data = data;
	}
}
