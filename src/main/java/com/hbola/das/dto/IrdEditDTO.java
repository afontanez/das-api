package com.hbola.das.dto;

import java.io.Serializable;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class IrdEditDTO implements Serializable{

	private static final long serialVersionUID = 8850519892692689641L;
	
	private static final SimpleDateFormat SDF = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
	
	private int id;
	private String modelName;
	private String firmwareVersion;
	private String firmwareVersionId;
	private String serialNumber;
	private String unitAddress;
	private String secondaryUnitAddress;
	private String cableOperatorCode;
	private String cableOperatorName;
	private String countryName;
	private String headendCode;
	private String heandName;
	private int presetId;
	private int bncPresetId;
	private String presetName;
	private String loggedInUser;
	private String createdDateFormatted;
	private String updatedDateFormatted;
	private String updatedBy;
	private boolean inventory = false;
	private int ownershipId;
	private String ownershipName;
	private Boolean frontPanelEnabled = false;
	private Boolean controlPortEnabled = false;//local control in old bnc 3.5
	private Integer transportStreamOutput;
    private Integer sdiOutput;
    private List<TranscoderDTO> transcoders = new ArrayList<TranscoderDTO>();	
    private int encoderId = 0;
    private String comments;
    private String iRTUnitAddress;
	private String iRTSerialNumber;
	    

	public IrdEditDTO(){} //needed by spring when creating the object using @RequestBody in IrdController
    public IrdEditDTO(String user, String model, String serial, String unitAddress, String headEndCode, String cableOperatorCode, int encoderId, boolean isInventory, String comments) {
    	
    	this.loggedInUser = user;
    	this.modelName = model;
    	this.serialNumber = serial;
    	this.unitAddress = unitAddress;  
    	this.headendCode = headEndCode;
    	this.cableOperatorCode = cableOperatorCode;
    	this.encoderId = encoderId;
    	this.inventory = isInventory;
    	this.comments = comments;
    }
    

    
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public Boolean getFrontPanelEnabled() {
		return frontPanelEnabled;
	}

	public void setFrontPanelEnabled(Boolean frontPanelEnabled) {
		this.frontPanelEnabled = frontPanelEnabled;
	}

	public Boolean getControlPortEnabled() {
		return controlPortEnabled;
	}

	public void setControlPortEnabled(Boolean controlPortEnabled) {
		this.controlPortEnabled = controlPortEnabled;
	}

	public Integer getTransportStreamOutput() {
		return transportStreamOutput;
	}

	public void setTransportStreamOutput(Integer transportStreamOutput) {
		this.transportStreamOutput = transportStreamOutput;
	}

	public Integer getSdiOutput() {
		return sdiOutput;
	}

	public void setSdiOutput(Integer sdiOutput) {
		this.sdiOutput = sdiOutput;
	}

	public String getCreatedDateFormatted() {
		return createdDateFormatted;
	}

	public void setCreatedDateFormatted(Timestamp createdDate) {
		
		try {
			Date date = new Date(createdDate.getTime());			
			this.createdDateFormatted = SDF.format(date);
		} 
		catch (Exception e) {
			this.createdDateFormatted = null;
		}
		
	}
	
	public String getUpdatedDateFormatted() {
		return updatedDateFormatted;
	}

	public void setUpdatedDateFormatted(Timestamp updatedDate) {
		
		try {
			Date date = new Date(updatedDate.getTime());			
			this.updatedDateFormatted = SDF.format(date);
		} 
		catch (Exception e) {
			this.updatedDateFormatted = null;
		}
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getPresetName() {
		return presetName;
	}

	public void setPresetName(String presetName) {
		this.presetName = presetName;
	}

	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getModelName() {
		return modelName;
	}


	public void setModelName(String modelName) {
		this.modelName = modelName;
	}


	public String getFirmwareVersion() {
		return firmwareVersion;
	}


	public void setFirmwareVersion(String firmwareVersion) {
		this.firmwareVersion = firmwareVersion;
	}


	public String getFirmwareVersionId() {
		return firmwareVersionId;
	}


	public void setFirmwareVersionId(String firmwareVersionId) {
		this.firmwareVersionId = firmwareVersionId;
	}


	public String getSerialNumber() {
		return serialNumber;
	}


	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}


	public String getUnitAddress() {
		return unitAddress;
	}


	public void setUnitAddress(String unitAddress) {
		this.unitAddress = unitAddress;
	}


	public String getSecondaryUnitAddress() {
		return secondaryUnitAddress;
	}


	public void setSecondaryUnitAddress(String secondaryUnitAddress) {
		this.secondaryUnitAddress = secondaryUnitAddress;
	}


	public String getCableOperatorCode() {
		return cableOperatorCode;
	}


	public void setCableOperatorCode(String cableOperatorCode) {
		this.cableOperatorCode = cableOperatorCode;
	}


	public String getCableOperatorName() {
		return cableOperatorName;
	}


	public void setCableOperatorName(String cableOperatorName) {
		this.cableOperatorName = cableOperatorName;
	}


	public String getHeadendCode() {
		return headendCode;
	}


	public void setHeadendCode(String headendCode) {
		this.headendCode = headendCode;
	}


	public String getHeandName() {
		return heandName;
	}


	public void setHeandName(String heandName) {
		this.heandName = heandName;
	}


	public int getPresetId() {
		return presetId;
	}


	public void setPresetId(int presetId) {
		this.presetId = presetId;
	}
	
	public String getLoggedInUser() {
		return loggedInUser;
	}

	public void setLoggedInUser(String loggedInUser) {
		this.loggedInUser = loggedInUser;
	}

	public List<TranscoderDTO> getTranscoders() {
		return transcoders;
	}


	public void setTranscoders(List<TranscoderDTO> transcoders) {
		this.transcoders = transcoders;
	}		
	
	public int getBncPresetId() {
		return bncPresetId;
	}

	public void setBncPresetId(int bncPresetId) {
		this.bncPresetId = bncPresetId;
	}
	
	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public boolean isInventory() {
		return inventory;
	}

	public void setInventory(boolean inventory) {
		this.inventory = inventory;
	}

	public int getOwnershipId() {
		return ownershipId;
	}

	public void setOwnershipId(int ownershipId) {
		this.ownershipId = ownershipId;
	}

	public String getOwnershipName() {
		return ownershipName;
	}

	public void setOwnershipName(String ownershipName) {
		this.ownershipName = ownershipName;
	}

	public int getEncoderId() {
		return encoderId;
	}
	public void setEncoderId(int encoderId) {
		this.encoderId = encoderId;
	}
	

	
	public String getIRTUnitAddress() {
		return iRTUnitAddress;
	}
	public void setIRTUnitAddress(String irtUnitAddress) {
		this.iRTUnitAddress = irtUnitAddress;
	}
	public String getIRTSerialNumber() {
		return iRTSerialNumber;
	}
	public void setIRTSerialNumber(String irtSerialNumber) {
		this.iRTSerialNumber = irtSerialNumber;
	}
	@Override
	public String toString(){
		
		return "Attributes:"+ " cableOperatorCode- "+this.cableOperatorCode
				+ " cableOperatorName- "+this.cableOperatorName
				+ " createdDate- "+this.createdDateFormatted
				+ " firmwareVersion- "+this.firmwareVersion
				+ " firmwareVersionId- "+this.firmwareVersionId
				+ " headendCode- "+this.headendCode
				+ " id- "+this.id
				+ " loggedInUser- "+this.loggedInUser
				+ " modelName- "+this.modelName
				+ " presetId- "+this.presetId
				+ " bncPresetId- "+this.bncPresetId
				+ " presetName- "+this.presetName
				+ " unitAddress- "+this.unitAddress
				+ " secondaryUnitAddress- "+this.secondaryUnitAddress
				+ " serialNumber- "+this.serialNumber
				+ " inventory- "+this.inventory
				+ " ownershipId- "+this.ownershipId
				+ " ownershipName- "+this.ownershipName;		
	}
}
