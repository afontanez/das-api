package com.hbola.das.dto;

import java.io.Serializable;

public class OwnershipDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4823918457146963679L;
	
	int id;
	String name;
	public OwnershipDTO() {
		super();
	}
	public OwnershipDTO(int id, String name) {
		super();
		this.id = id;
		this.name = name;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
