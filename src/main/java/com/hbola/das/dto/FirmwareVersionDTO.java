package com.hbola.das.dto;

import java.io.Serializable;

public class FirmwareVersionDTO  implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1322017992086036468L;
	
	private String version;
	private String modelName;
	
	public FirmwareVersionDTO(String version, String modelName) {
		super();
		this.version = version;
		this.setModelName(modelName);
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getModelName() {
		return modelName;
	}

	public void setModelName(String modelName) {
		this.modelName = modelName;
	}
	
}
