package com.hbola.das.dto;

import java.io.Serializable;
import java.util.Date;

public class UserDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7821890608548546132L;

	String id;
	String username;
	String password;
	String passwordSalt;
	String email;
	String roleName;
	boolean isApproved;
	boolean uniqueEmail;
	int passwordFormat;
	Date createdDate;
	
	public UserDTO() {
		super();
	}
	
	public UserDTO(String username) {
		super();
		this.id = "";
		this.username = username;
		this.password = "";
		this.passwordSalt = "";
		this.email = "";
		this.isApproved = true;
		this.uniqueEmail = true;
		this.passwordFormat = 1;
		this.createdDate = new Date();
	}
	public UserDTO(String username, String roleName) {
		this.username = username;
		this.roleName = roleName;
	}
	
	public UserDTO(String id, String username, boolean isApproved) {
		super();
		this.id = id;
		this.username = username;
		this.password = "";
		this.passwordSalt = "";
		this.email = "";
		this.isApproved = isApproved;
		this.uniqueEmail = true;
		this.passwordFormat = 1;
		this.createdDate = new Date();
	}
	public UserDTO(String id, String username, String email, boolean isApproved, Date createdDate) {
		super();
		this.id = id;
		this.username = username;
		this.password = "";
		this.passwordSalt = "";
		this.email = email;
		this.isApproved = isApproved;
		this.uniqueEmail = true;
		this.passwordFormat = 1;
		this.createdDate = createdDate;
	}
	
	public UserDTO(String id, String username, String password, String passwordSalt, String email, boolean isApproved,
			boolean uniqueEmail, int passwordFormat, Date createdDate) {
		super();
		this.id = id;
		this.username = username;
		this.password = password;
		this.passwordSalt = passwordSalt;
		this.email = email;
		this.isApproved = isApproved;
		this.uniqueEmail = uniqueEmail;
		this.passwordFormat = passwordFormat;
		this.createdDate = createdDate;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPasswordSalt() {
		return passwordSalt;
	}

	public void setPasswordSalt(String passwordSalt) {
		this.passwordSalt = passwordSalt;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public boolean isApproved() {
		return isApproved;
	}

	public void setApproved(boolean isApproved) {
		this.isApproved = isApproved;
	}

	public boolean isUniqueEmail() {
		return uniqueEmail;
	}

	public void setUniqueEmail(boolean uniqueEmail) {
		this.uniqueEmail = uniqueEmail;
	}

	public int getPasswordFormat() {
		return passwordFormat;
	}

	public void setPasswordFormat(int passwordFormat) {
		this.passwordFormat = passwordFormat;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
}
