package com.hbola.das.dto;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

public class TransactionLogDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1540950951499297311L;

	private static final SimpleDateFormat SDF = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
	private String createdOnFormatted;
	int id;
	int rollback;
	int decoderId;
	String action;
	String createdBy;
	Timestamp createdOn;
	
	public TransactionLogDTO(int id, String action, int rollback, int decoderId, String createdBy, Timestamp createdOn) {
		super();
		this.id = id;
		this.action = action;
		this.rollback = rollback;
		this.decoderId = decoderId;
		this.createdBy = createdBy;
		this.createdOn = createdOn;
		try {
			Date date = new Date(createdOn.getTime());			
			this.createdOnFormatted = SDF.format(date);
		} 
		catch (Exception e) {
			this.createdOnFormatted = null;
		}
	}
	
	public TransactionLogDTO() {
		super();
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public int getRollback() {
		return rollback;
	}

	public void setRollback(int rollback) {
		this.rollback = rollback;
	}

	public int getDecoderId() {
		return decoderId;
	}

	public void setDecoderId(int decoderId) {
		this.decoderId = decoderId;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}
	
	public String getCreatedOnFormatted() {
		return createdOnFormatted;
	}

	public void setCreatedOnFormatted(String createdOnFormatted) {
		this.createdOnFormatted = createdOnFormatted;
	}
}
