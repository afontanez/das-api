package com.hbola.das.dto;

import java.io.Serializable;

public class HeadendDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4052672692836336831L;

	private String cableCode;
	private Integer code;
	private String location;	
	private String manager;
	private String address;
	private String areaCode;
	private String areaCode2;
	private String phone;
	private String phone2;
	private String fax;
	private String pas1Dish;
	private String pas2Dish;
	private String pas3Dish;
	private String services;
	private String createdDate;
	private String updatedDate;
	private String updatedBy;
	private String primaryEmail;
	private String secondaryEmail;
	private boolean active;
	private int appVersion;	
	
	public HeadendDTO(){
		
	}
			
	public HeadendDTO(Integer code, String location, String cableCode) {
		super();
		this.code = code;
		this.location = location;
		this.cableCode = cableCode;
	}

	
	public HeadendDTO(String cableCode, Integer code, String location, String manager, String address, String areaCode, String areaCode2,
			String phone, String phone2, String fax, String pas1Dish, String pas2Dish, String pas3Dish, String services,
			String createdDate, String updatedDate, String updatedBy, String primaryEmail, String secondaryEmail,
			boolean active, int appVersion) {
		super();
		this.cableCode = cableCode;
		this.code = code;
		this.location = location;
		this.manager = manager;
		this.address = address;
		this.areaCode = areaCode;
		this.areaCode2 = areaCode2;
		this.phone = phone;
		this.phone2 = phone2;
		this.fax = fax;
		this.pas1Dish = pas1Dish;
		this.pas2Dish = pas2Dish;
		this.pas3Dish = pas3Dish;
		this.services = services;
		this.createdDate = createdDate;
		this.updatedDate = updatedDate;
		this.updatedBy = updatedBy;
		this.primaryEmail = primaryEmail;
		this.secondaryEmail = secondaryEmail;
		this.active = active;
		this.appVersion = appVersion;
	}


	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}
	
	public String getCableCode() {
		return cableCode;
	}

	public void setCableCode(String cableCode) {
		this.cableCode = cableCode;
	}


	public String getManager() {
		return manager;
	}


	public void setManager(String manager) {
		this.manager = manager;
	}


	public String getAddress() {
		return address;
	}


	public void setAddress(String address) {
		this.address = address;
	}


	public String getAreaCode() {
		return areaCode;
	}


	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}
	
	public String getAreaCode2() {
		return areaCode2;
	}

	public void setAreaCode2(String areaCode2) {
		this.areaCode2 = areaCode2;
	}

	public String getPhone() {
		return phone;
	}


	public void setPhone(String phone) {
		this.phone = phone;
	}


	public String getPhone2() {
		return phone2;
	}


	public void setPhone2(String phone2) {
		this.phone2 = phone2;
	}


	public String getFax() {
		return fax;
	}


	public void setFax(String fax) {
		this.fax = fax;
	}


	public String getPas1Dish() {
		return pas1Dish;
	}


	public void setPas1Dish(String pas1Dish) {
		this.pas1Dish = pas1Dish;
	}


	public String getPas2Dish() {
		return pas2Dish;
	}


	public void setPas2Dish(String pas2Dish) {
		this.pas2Dish = pas2Dish;
	}


	public String getPas3Dish() {
		return pas3Dish;
	}


	public void setPas3Dish(String pas3Dish) {
		this.pas3Dish = pas3Dish;
	}


	public String getServices() {
		return services;
	}


	public void setServices(String services) {
		this.services = services;
	}


	public String getCreatedDate() {
		return createdDate;
	}


	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}


	public String getUpdatedDate() {
		return updatedDate;
	}


	public void setUpdatedDate(String updatedDate) {
		this.updatedDate = updatedDate;
	}


	public String getUpdatedBy() {
		return updatedBy;
	}


	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}


	public String getPrimaryEmail() {
		return primaryEmail;
	}


	public void setPrimaryEmail(String primaryEmail) {
		this.primaryEmail = primaryEmail;
	}


	public String getSecondaryEmail() {
		return secondaryEmail;
	}


	public void setSecondaryEmail(String secondaryEmail) {
		this.secondaryEmail = secondaryEmail;
	}


	public boolean isActive() {
		return active;
	}


	public void setActive(boolean active) {
		this.active = active;
	}


	public int getAppVersion() {
		return appVersion;
	}


	public void setAppVersion(int appVersion) {
		this.appVersion = appVersion;
	}
}
