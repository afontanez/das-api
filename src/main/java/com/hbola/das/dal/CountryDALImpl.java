package com.hbola.das.dal;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import com.hbola.das.dto.CountryDTO;
import com.hbola.das.helper.DBHelper;

@Component
public class CountryDALImpl implements CountryDALI {

	private static final Logger logger = LogManager.getLogger(CountryDALImpl.class);

	@Override
	public List<CountryDTO> getList() throws Exception {
		CallableStatement stmnt = null;
        Connection conn = DBHelper.getConnection(true);
        ResultSet rs = null;
        List<CountryDTO> countries = new ArrayList<CountryDTO>();
        
        try {	        	
			stmnt = conn.prepareCall("{call dbo.HBO_s_GetAllCountries()}");
			
			rs = DBHelper.getResultSet(stmnt);

	        while (rs.next()) {	        	
	        	countries.add(new CountryDTO(rs.getInt("CountryID"), rs.getString("CountryName")));
	        }
			
		    DBHelper.closeConnection(rs, stmnt, conn);
		    
        } catch (Exception e) {            
        	logger.warn(e.getMessage());
            DBHelper.closeConnection(rs, stmnt, conn);
            throw new Exception("Problem getting list of Country: " + e.getMessage());
        }
        
        return countries;
	}
	
}
