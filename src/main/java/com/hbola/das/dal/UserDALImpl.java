package com.hbola.das.dal;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import com.hbola.das.dto.UserDTO;
import com.hbola.das.helper.DBHelper;

@Component
public class UserDALImpl implements UserDALI {
	
	private static final Logger logger = LogManager.getLogger(UserDALI.class);

	@Override
	public UserDTO getUserByUsername(String username) throws Exception {
		CallableStatement stmnt = null;
        ResultSet rs = null;
        Connection  conn = DBHelper.getConnection(true);
                
        try {	        	
			stmnt = conn.prepareCall("{call dbo.aspnet_Membership_FindUsersByName(?,?,?,?)}",
	                ResultSet.TYPE_SCROLL_INSENSITIVE,
	                ResultSet.CONCUR_READ_ONLY);
			
			stmnt.setString("ApplicationName","DAS");
			stmnt.setString("UserNameToMatch",username);
			stmnt.setInt("PageIndex", 0);
			stmnt.setInt("PageSize", 1);
			
			
			rs = DBHelper.getResultSet(stmnt);
			if(rs != null && rs.next()){
				UserDTO user = new UserDTO(rs.getString("UserId"), rs.getString("UserName"), 
										rs.getString("Email"), rs.getBoolean("IsApproved"), 
										rs.getDate("CreateDate"));
				DBHelper.closeConnection(rs, stmnt, conn);
				return user;
			}
			else {
				String message = "No Response from get user stored procedure";
				logger.warn(message);
				throw new Exception(message);
			}			
        } catch (Exception e) {            
        	logger.warn(e.getMessage());
            DBHelper.closeConnection(rs, stmnt, conn);            
            //propogate exception
            throw new Exception("Problem getting user: " + e.getMessage());
        }
	}

	@Override
	public String getRoleByUsername(String username) throws Exception {
		CallableStatement stmnt = null;
        ResultSet rs = null;
        Connection  conn = DBHelper.getConnection(true);
                
        try {	        	
			stmnt = conn.prepareCall("{call dbo.aspnet_UsersInRoles_GetRolesForUser(?,?)}",
	                ResultSet.TYPE_SCROLL_INSENSITIVE,
	                ResultSet.CONCUR_READ_ONLY);
			
			stmnt.setString("ApplicationName","DAS");
			stmnt.setString("UserName",username);
			
			
			rs = DBHelper.getResultSet(stmnt);
			if(rs != null && rs.next()){
				String roleName = rs.getString("RoleName");
				DBHelper.closeConnection(rs, stmnt, conn);
				return roleName;
			}
			else {
				String message = "No Response from get role stored procedure";
				logger.warn(message);
				throw new Exception(message);
			}			
        } catch (Exception e) {            
        	logger.warn(e.getMessage());
            DBHelper.closeConnection(rs, stmnt, conn);            
            //propogate exception
            throw new Exception("Problem getting role: " + e.getMessage());
        }
	}

}
