package com.hbola.das.dal;

import java.util.List;

import com.hbola.das.dto.CableOperatorDTO;

public interface CableOperatorDALI {

	/* (non-Javadoc)
	 * @see com.hbola.das.dal.ModelDALI#getModels()
	 */
	List<CableOperatorDTO> getList() throws Exception;

	int create(CableOperatorDTO co) throws Exception;
	int edit(CableOperatorDTO co) throws Exception;
	CableOperatorDTO getOneByCode(String code) throws Exception;

}