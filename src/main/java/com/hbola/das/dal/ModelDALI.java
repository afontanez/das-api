package com.hbola.das.dal;

import java.util.List;

import com.hbola.das.dto.FirmwareVersionDTO;
import com.hbola.das.dto.ModelDTO;
import com.hbola.das.dto.PresetDTO;

public interface ModelDALI {

	List<ModelDTO> getModels() throws Exception;

	List<FirmwareVersionDTO> getFirmwareVersions() throws Exception;

	List<PresetDTO> getPresets() throws Exception;

}