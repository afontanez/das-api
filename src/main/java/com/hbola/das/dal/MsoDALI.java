package com.hbola.das.dal;

import java.util.List;

import com.hbola.das.dto.MsoDTO;

public interface MsoDALI {

	int create(MsoDTO mso) throws Exception;

	int edit(MsoDTO mso) throws Exception;	
	
	List<MsoDTO> getList() throws Exception;

	MsoDTO getOneByCode(String code) throws Exception;

}
