package com.hbola.das.dal;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import com.hbola.das.dto.CableOperatorDTO;
import com.hbola.das.dto.HeadendDTO;
import com.hbola.das.helper.DBHelper;

@Component
public class CableOperatorDALImpl implements CableOperatorDALI{
	
	private static final Logger logger = LogManager.getLogger(ModelDALImpl.class);
	
	
	/* (non-Javadoc)
	 * @see com.hbola.das.dal.CableOperatorDALI#getCableOperators()
	 */
	@Override
	public List<CableOperatorDTO> getList() throws Exception {
		CallableStatement stmnt = null;
        Connection conn = DBHelper.getConnection(true);
        ResultSet rs = null;
        List<CableOperatorDTO> cableOperators = new ArrayList<CableOperatorDTO>();
        
        try {	        	
			//stmnt = conn.prepareCall("{call das.GetAllCableCodesOrderByMSO()}");
        	stmnt = conn.prepareCall("{call das.GetCable()}");
			rs = DBHelper.getResultSet(stmnt);

	        while (rs.next()) {
	        	cableOperators.add(new CableOperatorDTO(rs.getString("CableCode"), rs.getString("CableName"), rs.getString("MSOCode"), 
	        			rs.getString("CompanyName"), /*rs.getString("CountryID"),*/"",  rs.getString("CountryName"), rs.getString("Manager"), 
	        			rs.getString("Address"), rs.getString("City"), rs.getString("AreaCode"), rs.getString("AreaCode"), rs.getString("Phone"),
	        			rs.getString("Phone2"), rs.getString("Fax"), rs.getString("PostalCode"), rs.getString("Email"), 
	        			rs.getString("SecondaryEmail"), 0, rs.getString("AuxiliaryRefCode"), rs.getString("CreatedDate"), 
	        			rs.getString("UpdatedDate"), rs.getString("UpdatedBy"), rs.getBoolean("IsInventoryHolder"),
	        			rs.getBoolean("IsActive"), rs.getInt("App_Version"), new ArrayList<HeadendDTO>()));
	        }
			
		    DBHelper.closeConnection(rs, stmnt, conn);
		    
        } catch (Exception e) {            
        	logger.warn(e.getMessage());
            DBHelper.closeConnection(rs, stmnt, conn);
        }
        
        return cableOperators;
	}


	@Override
	public int create(CableOperatorDTO co) throws Exception {
		CallableStatement stmnt = null;
        Connection conn = DBHelper.getConnection();
        ResultSet rs = null;
        int response = 0;
        
        try {	    
        	
			stmnt = conn.prepareCall("{call das.Save_Cable(?, ?, ?, ?, ?, ?,?, ?, ?, ?, ?, ?,?, ?, ?, ?, ?, ?, ?, ?)}",
	                ResultSet.TYPE_SCROLL_INSENSITIVE,
	                ResultSet.CONCUR_READ_ONLY);
			
			stmnt.setString("MSOCode", co.getMsoCode());
			stmnt.setString("CableCode", co.getCode());
			stmnt.setString("CableName", co.getName());
			stmnt.setString("CountryID", co.getCountryId());
			stmnt.setString("CompanyName", co.getCompanyName());
			stmnt.setString("Manager", co.getManager());
			stmnt.setString("Address", co.getAddress());
			stmnt.setString("City", co.getCity());
			stmnt.setString("AreaCode", co.getAreaCode());
			stmnt.setString("AreaCode2", co.getAreaCode2());
			stmnt.setString("Phone", co.getPhone());
			stmnt.setString("Phone2", co.getPhone2());
			stmnt.setString("Fax", co.getFax());
			stmnt.setString("PostalCode", co.getPostalCode());
			stmnt.setString("Email", co.getEmail());
			stmnt.setString("SecondaryEmail", co.getSecondaryEmail());
			stmnt.setString("AuxiliaryRefCode", co.getAuxiliaryRefCode());
			stmnt.setString("UpdatedBy", co.getUpdatedBy());
			stmnt.setBoolean("IsInventoryHolder", co.isInventoryHolder());
			stmnt.setBoolean("IsUpdated", co.isNewCable());
			
			
        	rs = DBHelper.getResultSet(stmnt);
			if (rs.next()) {        	
				response = rs.getInt(1);
			}
			else {
				String message = "No Response from extend time to token stored procedure";
				logger.warn(message);
				throw new Exception(message);
			}
			
			if (response == -1){
				String message = "CableCode already exists.";
				throw new Exception(message);
			}

			DBHelper.commitAndCloseConnection(rs, stmnt, conn);
			
        } catch (Exception e) {            
        	logger.warn(e.getMessage());
            DBHelper.rollbackAndCloseConnection(rs, stmnt, conn);
            
            //propogate exception
            throw new Exception("Problem creating Cable Operator: " + e.getMessage());
        }
        return response;
	}


	@Override
	public int edit(CableOperatorDTO co) throws Exception {
		CallableStatement stmnt = null;
        Connection conn = DBHelper.getConnection();
        ResultSet rs = null;
        int response = 0;
        
        try {	    
        	
			stmnt = conn.prepareCall("{call dbo.HBO_u_Cable(?, ?, ?, ?, ?, ?,"
															+ "?, ?, ?, ?, ?, ?,"
															+ "?, ?, ?, ?, ?, ?,"
															+ "?)}",
	                ResultSet.TYPE_SCROLL_INSENSITIVE,
	                ResultSet.CONCUR_READ_ONLY);
			
			stmnt.setString("MSOCode", co.getMsoCode());
			stmnt.setString("CableCode", co.getCode());
			stmnt.setString("CableName", co.getName());
			stmnt.setString("CompanyName", co.getCompanyName());
			stmnt.setString("CountryName", co.getCountryName());
			stmnt.setString("Manager", co.getManager());
			stmnt.setString("Address", co.getAddress());
			stmnt.setString("City", co.getCity());
			stmnt.setString("AreaCode", co.getAreaCode());
			stmnt.setString("Phone", co.getPhone());
			stmnt.setString("Phone2", co.getPhone2());
			stmnt.setString("Fax", co.getFax());
			stmnt.setString("PostalCode", co.getPostalCode());
			stmnt.setString("Email", co.getEmail());
			stmnt.setString("SecondaryEmail", co.getSecondaryEmail());
			stmnt.setString("AuxiliaryRefCode", co.getAuxiliaryRefCode());
			stmnt.setString("UpdatedBy", co.getUpdatedBy());
			stmnt.setString("CreatedDate", co.getCreatedDate());
			stmnt.setString("UpdatedDate", co.getUpdatedDate());
			
        	rs = DBHelper.getResultSet(stmnt);
			
			if (rs.next()) {        	
				response = rs.getInt(1);
			}
			else {
				String message = "No Response from extend time to token stored procedure";
				logger.warn(message);
				throw new Exception(message);
			}
			DBHelper.commitAndCloseConnection(rs, stmnt, conn);
			
        } catch (Exception e) {            
        	logger.warn(e.getMessage());
            DBHelper.rollbackAndCloseConnection(rs, stmnt, conn);
            
            //propogate exception
            throw new Exception("Problem creating Cable Operator: " + e.getMessage());
        }
        return response;
	}


	@Override
	public CableOperatorDTO getOneByCode(String code) throws Exception {
		CallableStatement stmnt = null;
        Connection conn = DBHelper.getConnection(true);
        ResultSet rs = null;
        CableOperatorDTO co = new CableOperatorDTO();
        
        try {
			stmnt = conn.prepareCall("{call das.GetCableDetails(?)}");
			stmnt.setString("CableCode", code);
			
			rs = DBHelper.getResultSet(stmnt);

	        if (rs.next()) {
	        	co = new CableOperatorDTO(rs.getString("CableCode"), rs.getString("CableName"), rs.getString("MSOCode"), rs.getString("CompanyName"), rs.getString("CountryID"),
	        			rs.getString("CountryName"), rs.getString("Manager"),rs.getString("Address"), rs.getString("City"),rs.getString("AreaCode"), rs.getString("Areacode2"),
	        			rs.getString("Phone"), rs.getString("Phone2"),rs.getString("Fax"), rs.getString("PostalCode"), rs.getString("Email"),
	        			rs.getString("SecondaryEmail"), 0, rs.getString("AuxiliaryRefCode"), rs.getString("CreatedDate"),rs.getString("UpdatedDate"),
	        			rs.getString("UpdatedBy"), rs.getBoolean("IsInventoryHolder"),true, 1, new ArrayList<HeadendDTO>());
	        }
		    DBHelper.closeConnection(rs, stmnt, conn);
		    
        } catch (Exception e) {            
        	logger.warn(e.getMessage());
            DBHelper.closeConnection(rs, stmnt, conn);
            throw new Exception("Problem getting Cable Operator: " + e.getMessage());
        }
        
        return co;
	}
}
