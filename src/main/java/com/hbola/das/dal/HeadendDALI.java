package com.hbola.das.dal;

import java.util.List;

import com.hbola.das.dto.HeadendDTO;

public interface HeadendDALI {
	
	int create(HeadendDTO headend) throws Exception;

	int edit(HeadendDTO headend) throws Exception;	
	
	List<HeadendDTO> getList() throws Exception;

	HeadendDTO getOneByCode(int code, String cableCode) throws Exception;

	List<HeadendDTO> getListByCableCode(String cableCode) throws Exception;

}