package com.hbola.das.dal;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import com.hbola.das.dto.HistoryLogDTO;
import com.hbola.das.dto.TransactionLogDTO;
import com.hbola.das.helper.DBHelper;

@Component
public class LogDALImpl implements LogDALI{

	private static final Logger logger = LogManager.getLogger(LogDALImpl.class);
	@Override
	public List<HistoryLogDTO> getIrdHistoryLog(int decoderId) throws Exception {
		
		List<HistoryLogDTO> list = new ArrayList<HistoryLogDTO>();	
		
		Connection conn = null;
		CallableStatement stmnt = null;
		ResultSet rs = null;
		try {
	
			conn = DBHelper.getConnection();
			stmnt = conn.prepareCall("{call das.GetDecoderServices_Changes(?)}");
			stmnt.setInt("decoderid", decoderId);
			rs = DBHelper.getResultSet(stmnt);
		
			while (rs != null && rs.next()) {
				HistoryLogDTO dto = new HistoryLogDTO("", rs.getTimestamp("CreatedOn"), rs.getString("Description"), 
												rs.getString("CreatedBy"), rs.getString("Comments"), rs.getString("TierName"),
												rs.getBoolean("HD"), rs.getBoolean("SD"), rs.getBoolean("Passthru"));
				list.add(dto);
			}
	    
			DBHelper.commitAndCloseConnection(rs, stmnt, conn);
	    
		} catch (Exception e) {            
			logger.warn(e.getMessage());
			DBHelper.closeConnection(rs, stmnt, conn);
		}

		return list;
	}
	@Override
	public List<TransactionLogDTO> getIrdTransactionLog(int decoderId) throws Exception {
		
		List<TransactionLogDTO> list = new ArrayList<TransactionLogDTO>();	
		
		Connection conn = null;
		CallableStatement stmnt = null;
		ResultSet rs = null;
		try {
	
			conn = DBHelper.getConnection(true);
			
			stmnt = conn.prepareCall("{call das.GetTransactionalLogByDecoderID(?)}");
			stmnt.setInt("decoderid", decoderId);
			rs = DBHelper.getResultSet(stmnt);
		
			while (rs != null && rs.next()) {	
				TransactionLogDTO dto = new TransactionLogDTO(rs.getInt("TransactionalLogID"),rs.getString("Action"),
											rs.getInt("Rollback"), rs.getInt("DecoderID"),rs.getString("Createdby"), 
											rs.getTimestamp("CreatedOn"));
				list.add(dto);
			}
	    
			DBHelper.closeConnection(rs, stmnt, conn);
	    
		} catch (Exception e) {            
			logger.warn(e.getMessage());
			DBHelper.closeConnection(rs, stmnt, conn);
		}

		return list;
	}
	@Override
	public void saveTransactionLog(String action, String status, String loggedInUser, int rollback, int decoderId,
			int decoderServiceId, Connection conn) throws Exception {
		if(action == null || action.isEmpty())
			return;
		if(loggedInUser == null || loggedInUser.isEmpty())
			return;
		
		CallableStatement stmnt = null;
        ResultSet rs = null;
        Connection ownConn = conn;
        //int transactionId = 0;
        if(conn == null)
        	ownConn = DBHelper.getConnection(true);
        try
        {
			stmnt = ownConn.prepareCall("{call das.SaveTransactionalLog(?, ?, ?, ?, ?, ?, ?)}");
			
			if(status == null)
				status = "";
			
			stmnt.setString("Action", action);
			stmnt.setString("Status", status);
			stmnt.setString("User", loggedInUser);
			stmnt.setInt("rollback", rollback);
			stmnt.setInt("DecoderId", decoderId);
			stmnt.setInt("DecoderServicesId", decoderServiceId);
			stmnt.registerOutParameter("TransactionalLogID", java.sql.Types.INTEGER);  
			
			stmnt.execute();
			//transactionId = stmnt.getInt("TransactionalLogID");
        } catch (Exception e){
        	logger.warn(e);
        	throw new Exception("Error saving TransactionalLog. Error: " + e.getMessage());
        }
        finally{
        	if(conn == null)
        		DBHelper.closeConnection(rs, stmnt, ownConn);
        }
	}
	@Override
	public List<HistoryLogDTO> getMDHistoryLog(Integer decoderId) throws Exception {

		List<HistoryLogDTO> list = new ArrayList<HistoryLogDTO>();	
		
		Connection conn = null;
		CallableStatement stmnt = null;
		ResultSet rs = null;
		try {
	
			conn = DBHelper.getConnection();
			stmnt = conn.prepareCall("{call das.GetMDDecoderServices_Changes(?)}");
			stmnt.setInt("decoderid", decoderId);
			rs = DBHelper.getResultSet(stmnt);
		
			while (rs != null && rs.next()) {
				HistoryLogDTO dto = new HistoryLogDTO(rs.getString("UnitAddress"), rs.getTimestamp("CreatedOn"), rs.getString("Description"), 
												rs.getString("CreatedBy"), rs.getString("Comments"), rs.getString("TierName"),
												rs.getBoolean("HD"), rs.getBoolean("SD"), rs.getBoolean("Passthru"));
				list.add(dto);
			}
	    
			DBHelper.commitAndCloseConnection(rs, stmnt, conn);
	    
		} catch (Exception e) {            
			logger.warn(e.getMessage());
			DBHelper.closeConnection(rs, stmnt, conn);
		}

		return list;
		
	}

}
