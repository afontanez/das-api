package com.hbola.das.dal;

import java.util.List;

import com.hbola.das.dto.EncoderDTO;
import com.hbola.das.dto.ServiceDTO;

public interface EncoderDALI {

	List<EncoderDTO> getEncoders(Boolean implementsTemplate) throws Exception;

	List<ServiceDTO> getServices() throws Exception;

}