package com.hbola.das.dal;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.MultiMapUtils;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.multimap.ArrayListValuedHashMap;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.hbola.das.dto.FatTierDTO;
import com.hbola.das.dto.IrdEditDTO;
import com.hbola.das.dto.IrdSummaryDTO;
import com.hbola.das.dto.OperationDTO;
import com.hbola.das.dto.OwnershipDTO;
import com.hbola.das.dto.RequestDecodersFiltered;
import com.hbola.das.dto.ServiceDTO;
import com.hbola.das.dto.TranscoderDTO;
import com.hbola.das.helper.DBHelper;
import com.hbola.das.helper.DateHelper;
import com.hbola.das.service.BNCServiceI;
import com.hbola.das.service.IrdServiceI;

@Component
public class IrdDALImpl implements IrdDALI {

	private static final Logger logger = LogManager.getLogger(IrdDALImpl.class);
    	    
	@Autowired
	private BNCServiceI bncService;
	
//	@Autowired
//	private IrdServiceI irdService;
	
	@Override
	public void checkCableCodeHeadend(IrdEditDTO ird) throws Exception {
		
		Connection conn = DBHelper.getConnection();
		ResultSet rs = null;
		PreparedStatement stmnt = null;
		
		boolean doesExist = false;
		try {
			
			String sql = "select * from TB_HeadEnd where CableCode = ? and HeadEndCode = ?";
			stmnt = conn.prepareStatement(sql);
			
			stmnt.setString(1, ird.getCableOperatorCode());			
			stmnt.setString(2, ird.getHeadendCode());
				
			rs = stmnt.executeQuery();
			if (rs.next()) {
				doesExist = true;	
			}
		}
		finally {
			DBHelper.commitAndCloseConnection(rs, stmnt, conn);
		}
		
		if (!doesExist) {
			throw new Exception("Invalid Cable Code " + ird.getCableOperatorCode() + " HeadEnd " + ird.getHeadendCode() + " Combination");
		}
		
	}
	
	
	/* (non-Javadoc)
	 * @see com.hbola.das.dal.IrdDal2I#saveIrd(com.hbola.das.dto.IrdEditDTO)
	 */
	@Override
	public int saveIrd(IrdEditDTO ird) throws Exception {

	        CallableStatement stmnt = null;
	        Connection conn = DBHelper.getConnection();
	        ResultSet rs = null;
	        int irdId = 0;
	        
	        try {	        	
				stmnt = conn.prepareCall("{call das.SaveDecoder(?, ?, ?, ?, ?, ?, ?,"
															+ "?, ?, ?, ?, ?, ?,"
															+ "?, ?, ?, ?, ?, ?, "
															+ "?, ?, ?, ?, ?, ?, ?)}");
				
				stmnt.setString("IRTAddress", ird.getIRTUnitAddress());
				stmnt.setString("UnitAddress", ird.getUnitAddress());
				stmnt.setString("DecoSerial",ird.getSerialNumber());
				stmnt.setInt("OperationID", 10);//new decoder
				stmnt.setString("TestEndDate", "");				
				stmnt.setString("TierName", "");
				stmnt.setString("CableCode", ird.getCableOperatorCode());
				stmnt.setString("HeadEndCode", ird.getHeadendCode());
				stmnt.setString("Comments", ird.getComments());
				stmnt.setString("ModelID", ird.getModelName());
				stmnt.setString("Version", ird.getFirmwareVersion());
				stmnt.setString("UpdatedBy", ird.getLoggedInUser());
				
				
				if (ird.getTransportStreamOutput()==null) {
					stmnt.setNull("TransportStreamOutput", java.sql.Types.INTEGER);
				}
				else {
					stmnt.setInt("TransportStreamOutput", ird.getTransportStreamOutput());	
				}				
				
				if (ird.getFrontPanelEnabled()==null) {
					stmnt.setNull("FrontPanelEnabled", java.sql.Types.INTEGER);
				}
				else {
					stmnt.setInt("FrontPanelEnabled", ird.getFrontPanelEnabled()?1:0);
				}
				
				stmnt.setNull("EMMStream", java.sql.Types.INTEGER);
				stmnt.setNull("EMMProvider", java.sql.Types.INTEGER);
				stmnt.setInt("Active", 1);
				stmnt.setInt("DecoderId", ird.getId());
				stmnt.setString("CreatedBy", ird.getLoggedInUser());
				stmnt.setInt("LocalControl", (ird.getControlPortEnabled()==null ? 0 : (ird.getControlPortEnabled()?1:0)));
				stmnt.setInt("IsInventory", (ird.isInventory())?1:0);
				stmnt.setInt("OwnershipId", ird.getOwnershipId());
				stmnt.setString("SecondaryUnitAddress", ird.getSecondaryUnitAddress());
				stmnt.setInt("PresetID", ird.getPresetId());
				stmnt.setInt("EncoderID", ird.getEncoderId());
				
				if (ird.getSdiOutput() == null) {
					stmnt.setNull("SDIOutput", java.sql.Types.INTEGER);
				}
				else {
					stmnt.setInt("SDIOutput", ird.getSdiOutput());	
				}
				
			    rs = stmnt.executeQuery();
				
				if (rs.next()) {        	
					irdId = rs.getInt(1);
				}
				else {
					String message = "No Response from create decoder stored procedure";
					logger.warn(message + "_"+ird.toString());
					throw new Exception(message);
				}
				
				if(irdId <= 0)
				{
					String message = "Unit Address is already in use.";
					logger.warn(message + "_"+ird.toString());
					throw new Exception(message);
				}
				
				if (!ird.isInventory()) {
					
					saveIrdServices(ird.getLoggedInUser(), ird.getTranscoders(), irdId, conn);
					
//					List<TranscoderDTO> transcoders = ird.getTranscoders();
//					List<TranscoderDTO> newTranscoders = irdService.removeTranscoderBlackout(transcoders);
//					ird.setTranscoders(newTranscoders);
					
					bncService.createIRDOnBNC(ird);
				}
				
			    DBHelper.commitAndCloseConnection(rs, stmnt, conn);
			    return irdId;
	        } catch (Exception e) {            
	        	logger.warn(e.getMessage());
	            DBHelper.rollbackAndCloseConnection(rs, stmnt, conn);
	            //propogate exception
	            throw new Exception("Problem saving decoder: UA: " + ird.getUnitAddress() + "\nSecondary UA: " + ird.getSecondaryUnitAddress() + "\nmessaage: " + e.getMessage());
	        }
	}   
		
	private void saveIrdServices(String loggedInUser, List<TranscoderDTO> transcoders, int irdId, Connection conn) throws Exception
	{
		CallableStatement stmnt = null;
        ResultSet rs = null;
        
        try
        {
			for (TranscoderDTO transc : transcoders) {
				
				if (transc.getIsUpdated() != null && transc.getIsUpdated()) {
				
					stmnt = conn.prepareCall("{call das.SaveDecoderServices(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)}");
					
					stmnt.setInt("DecoderId", irdId);
					stmnt.setInt("DecoderServicesOrder", transc.getTranscoderId());
					stmnt.setString("TierNumber", transc.getService() == null ? "-1": transc.getService().getTierNumber());
					stmnt.setString("UnitAddress", transc.getUnitAddress());//for 4410MD blank for every other type of decoder				
					stmnt.setInt("OperationId", transc.getOperation() == null ? -1: transc.getOperation().getId());
					stmnt.setInt("IsAuthorized", transc.getAuthorized() ? 1 : 0);
					stmnt.setString("Comments", transc.getComments());
					stmnt.setString("ModifiedBy", loggedInUser);				
					stmnt.setInt("DecoderServicesId", transc.getDecoderServicesIdentity());//update will have a value save will be null
					
									
					if (transc.isHdEnabled() == null) {					
						stmnt.setNull("HD", java.sql.Types.INTEGER);
					}
					else {
						stmnt.setInt("HD", transc.isHdEnabled() ? 1 : 0);	
					}				
	
					if (transc.isSdEnabled() == null) {					
						stmnt.setNull("SD", java.sql.Types.INTEGER);
					}
					else {
						stmnt.setInt("SD", transc.isSdEnabled() ? 1 : 0);	
					}
	
					if (transc.isPassthruEnabled() == null) {
						stmnt.setNull("Passthru", java.sql.Types.INTEGER);
					}
					else {
						stmnt.setInt("Passthru", transc.isPassthruEnabled() ? 1 : 0);	
					}
					
					
					String startDate = (transc.getOperation() == null || transc.getOperation().getStartDate() == null ||
										transc.getOperation().getStartDate().isEmpty()) ?null : transc.getOperation().getStartDate().trim();
					startDate = (startDate == null || startDate.indexOf("1900")==0 || startDate.isEmpty())?null:startDate.substring(0, 10);
					
					String endDate = (transc.getOperation() == null || transc.getOperation().getEndDate() == null ||
										transc.getOperation().getEndDate().isEmpty()) ?null : transc.getOperation().getEndDate().trim();
					endDate = (endDate == null || endDate.indexOf("1900")==0 ||endDate.isEmpty())?null:endDate.substring(0, 10);
					
					stmnt.setString("StartDate",  startDate);	
					stmnt.setString("EndDate", endDate);	
					if(transc.getOperation() != null){
						transc.getOperation().setStartDate(startDate);
						transc.getOperation().setEndDate(endDate);
					}
					
					rs = stmnt.executeQuery();
					if (!rs.next()) {
						throw new Exception("Creating services is failing.");
					}
				}//if transc.isUpdated
			}
        } catch (Exception e){
        	logger.warn(e);
        	throw new Exception("Error saving transcoder. Error: " + e.getMessage());
        }
	}
	
	@Override
	public IrdEditDTO getIrd(int irdId) throws Exception {
		CallableStatement stmnt = null;
        Connection conn = DBHelper.getConnection(true);
        ResultSet rs = null;
        IrdEditDTO ird = new IrdEditDTO();
        
        try {	        	
			stmnt = conn.prepareCall("{call das.GetDecoderByDecoderId(?)}");
			stmnt.setInt("DecoderId", irdId);
			rs = DBHelper.getResultSet(stmnt);
			int encoderId = 0;
			String encoderName = "";
			if(rs != null && rs.next()){
				ird.setId(rs.getInt("DecoderId")); 
				ird.setModelName(rs.getString("ModelID"));
				ird.setFirmwareVersionId(rs.getString("FirmwareVersionId"));
				ird.setFirmwareVersion(rs.getString("FirmwareVersion"));
				ird.setCableOperatorCode(rs.getString("CableCode"));
				ird.setCableOperatorName(rs.getString("CableName"));
				ird.setCountryName(rs.getString("CountryName"));
				ird.setHeadendCode(rs.getString("HeadEndCode"));
				ird.setHeandName(rs.getString("Location"));
				ird.setPresetId(rs.getInt("PresetID"));
				ird.setPresetName(rs.getString("PresetName"));
				ird.setSecondaryUnitAddress(rs.getString("SecondaryUnitAddress"));
				ird.setUnitAddress(rs.getString("UnitAddress"));
				ird.setSerialNumber(rs.getString("DecoSerial"));
				ird.setTranscoders( new ArrayList<TranscoderDTO>());
				encoderId = rs.getInt("EncoderID");
				encoderName = rs.getString("EncoderName");
				ird.setCreatedDateFormatted(rs.getTimestamp("CreatedOn"));
				ird.setUpdatedDateFormatted(rs.getTimestamp("LastModifiedOn"));
				ird.setUpdatedBy(rs.getString("LastModifiedBy"));
				ird.setControlPortEnabled((rs.getInt("LocalControl")==1));
				ird.setInventory(rs.getBoolean("IsInventory"));
				ird.setOwnershipId(rs.getInt("OwnershipId"));
				ird.setOwnershipName(rs.getString("OwnershipName"));
				ird.setFrontPanelEnabled(rs.getBoolean("FrontPanelEnabled"));
				ird.setTransportStreamOutput(rs.getInt("TransportStreamOutput"));
				ird.setComments(rs.getString("Comments"));
				ird.setSdiOutput(rs.getInt("SDIOutput"));			
			}
			
			//Decoder Comments 
			boolean isResultSet = stmnt.getMoreResults();
			if (isResultSet) {  
				rs = stmnt.getResultSet();
				if(rs != null && rs.next()){
					//why is this results set here if we are just skipping it? 
				}   
			}
			
			//set up transcoder  
			isResultSet = stmnt.getMoreResults();
			if (isResultSet) {
				rs = stmnt.getResultSet();
				if(rs != null){
					while (rs.next()){
						TranscoderDTO transc = new TranscoderDTO();
						transc.setUnitAddress(rs.getString("UnitAddress"));
						transc.setComments(rs.getString("Comments"));
						transc.setHdEnabled(rs.getBoolean("HD"));
						transc.setOperation(new OperationDTO(rs.getInt("OperationId"), rs.getString("OperationName"), rs.getBoolean("IsAuthorized"), 
								rs.getString("StartDate"), rs.getString("EndDate")));
						transc.setPassthruEnabled(rs.getBoolean("Passthru"));
						transc.setSdEnabled(rs.getBoolean("SD"));
						transc.setService(new ServiceDTO(encoderId,
															encoderName,
															rs.getString("TierNumber"),
															rs.getString("TierName"),
															rs.getString("VchNumber"),
															rs.getInt("VchTable")));
						transc.setDecoderServicesIdentity(rs.getInt("DecoderServicesId"));
						transc.setTranscoderId(rs.getInt("DecoderServicesOrder"));
						transc.setAuthorized(rs.getBoolean("IsAuthorized"));
						ird.getTranscoders().add(transc);
					}
				}
			}
			
			// Services Comments 
			isResultSet = stmnt.getMoreResults();
			if (isResultSet) {  
				rs = stmnt.getResultSet();
				if(rs != null && rs.next()){
					// Set Comments to Services
				}   
			}
			
		    DBHelper.closeConnection(rs, stmnt, conn);
		    
        } catch (Exception e) {            
        	logger.warn(e.getMessage());
            DBHelper.closeConnection(rs, stmnt, conn);
        }
        
        return ird;
	}
	
	@Override
	public List<OperationDTO> getOperations() throws Exception {
		CallableStatement stmnt = null;
        Connection conn = DBHelper.getConnection(true);
        ResultSet rs = null;
        List<OperationDTO> operations = new ArrayList<OperationDTO>();
        
        try {	        	
			stmnt = conn.prepareCall("{call das.GetAllOperations()}");
			rs = DBHelper.getResultSet(stmnt);

	        while (rs.next()) {
	        	operations.add(new OperationDTO(rs.getInt("OperationID"),
        										rs.getString("Operation"), 
		        								rs.getInt("IsAuthorized") == 0 ? false : true,
		        								null, 
		        								null));
	        }
			
		    DBHelper.closeConnection(rs, stmnt, conn);
		    
        } catch (Exception e) {            
        	logger.warn(e.getMessage());
            DBHelper.closeConnection(rs, stmnt, conn);
        }
        
        return operations;
	}

	@Override
	public Map<String, Object> getIrds(RequestDecodersFiltered filter) throws Exception {
		
		CallableStatement stmnt = null;
        Connection conn = DBHelper.getConnection();
        ResultSet rs = null;
        
        List<IrdSummaryDTO> irds = new ArrayList<IrdSummaryDTO>();
        List<TranscoderDTO> transcoders = new ArrayList<TranscoderDTO>();
        
        //https://www.baeldung.com/java-map-duplicate-keys
        MultiValuedMap<String, TranscoderDTO> tdtoMap = new ArrayListValuedHashMap<>();
        
        try {	        	
			stmnt = conn.prepareCall("{call das.GetAllDecodersWithServices(?,?,?)}");
			
			if(filter.getInitDate() != null)
				stmnt.setString("StartDate", DateHelper.convertDateToString(filter.getInitDate()));
			else
				stmnt.setString("StartDate",null);
			
			if(filter.getEndDate() != null)
				stmnt.setString("EndDate", DateHelper.convertDateToString(filter.getEndDate()));
			else
				stmnt.setString("EndDate",null);
			
			stmnt.setBoolean("IsInventory", filter.getIsInventory() != null? filter.getIsInventory():false);
							
			
			rs = DBHelper.getResultSet(stmnt);

	        while (rs.next()) {
	        	List<String> unitAddresses = new ArrayList<String>();
	        	unitAddresses.add(rs.getString("UnitAddress"));
	        	unitAddresses.add(rs.getString("SecondaryUnitAddress"));
	        	
	        	irds.add(new IrdSummaryDTO(rs.getInt("DecoderId"),
						rs.getString("ModelID"), 
						rs.getString("DecoSerial"),
						unitAddresses, 
						rs.getString("CableCode")+"-"+rs.getString("CableName"), 
						rs.getString("Location"), 
						rs.getString("CountryName"), 
						rs.getString("EncoderName"), 
						rs.getBoolean("IsInventory"), 
						0, 
						rs.getString("OwnershipName"), 
						/*new HashMap<String, String>()*/new ArrayList<String>(),
						new ArrayList<TranscoderDTO>()));
	        }
	        	        
	        boolean isResultSet = stmnt.getMoreResults();
			if (isResultSet) {
				rs = stmnt.getResultSet();
				if(rs != null){
					while (rs.next()){
						
						TranscoderDTO tdto = new TranscoderDTO(rs.getInt("DecoderId"), rs.getInt("DecoderServicesOrder"), 
								new ServiceDTO(rs.getInt("DecoderId"), rs.getString("TierNumber"), rs.getString("TierName"), "", 0), 
								rs.getString("Comments"), 
								new OperationDTO(rs.getInt("OperationId"), rs.getString("OperationName"), rs.getBoolean("IsAuthorized"), "", ""),
								 rs.getBoolean("HD"), rs.getBoolean("SD"), rs.getBoolean("Passthru"), rs.getInt("DecoderServicesId"), 
								 rs.getBoolean("IsAuthorized"), rs.getString("UnitAddress")); 
						
						transcoders.add(tdto);
						tdtoMap.put(Integer.toString(rs.getInt("DecoderId")), tdto);
					}
				}
			}
			
		    DBHelper.commitAndCloseConnection(rs, stmnt, conn);
		    
        } catch (Exception e) {            
        	logger.warn(e.getMessage());
            DBHelper.rollbackAndCloseConnection(rs, stmnt, conn);
        }
        
        //return irds;
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("irds", irds);
        map.put("transcodersMap", tdtoMap);
        return map;
	}
		
	@Override
	public List<OwnershipDTO> getOwnerships() throws Exception {
		CallableStatement stmnt = null;
        Connection conn = DBHelper.getConnection(true);
        ResultSet rs = null;
        List<OwnershipDTO> ownerships = new ArrayList<OwnershipDTO>();
        
        try {	        	
			stmnt = conn.prepareCall("{call das.GetDecoder_Ownership()}");
			rs = DBHelper.getResultSet(stmnt);

	        while (rs.next()) {
	        	ownerships.add(new OwnershipDTO(rs.getInt("OwnershipId"),
        										rs.getString("OwnershipName")));
	        }
			
		    DBHelper.closeConnection(rs, stmnt, conn);
		    
        } catch (Exception e) {            
        	logger.warn(e.getMessage());
            DBHelper.closeConnection(rs, stmnt, conn);
        }
        
        return ownerships;
	}
	
	@Override
	public int getBNCPreset(int id) throws Exception {
		
		int bncPresetId = 0;
		PreparedStatement stmnt = null;
		ResultSet rs = null;
		Connection conn = DBHelper.getConnection(true);
		try {
			
			String query = "select bnc_presetID from das.presets where presetid = ?";
			stmnt = conn.prepareStatement(query);
			stmnt.setInt(1, id);
			rs = stmnt.executeQuery();		  
			
			if (rs.next()) {        	
				bncPresetId = rs.getInt(1);
			}			
			
		    DBHelper.closeConnection(rs, stmnt, conn);
		}
		catch (Exception e) {
        	logger.warn(e.getMessage());
            DBHelper.closeConnection(rs, stmnt, conn);			
		}
		
		return bncPresetId;
		
	}
	
	@Override
	public String getTierName(String tierNumber) {
		
		String tierName = "";
		PreparedStatement stmnt = null;
		ResultSet rs = null;
		Connection conn = null;
	
		try {		
			conn = DBHelper.getConnection(true);
			
			String query = "select tierName from tb_channel where tiernumber = ?";
			stmnt = conn.prepareStatement(query);
			stmnt.setString(1, tierNumber);
			rs = stmnt.executeQuery();		  
				
			if (rs.next()) {        	
				tierName = rs.getString(1);
			}			
				
		    DBHelper.closeConnection(rs, stmnt, conn);		
		}
		catch (Exception e) {
        	logger.warn(e.getMessage());
        	try {
        		DBHelper.closeConnection(rs, stmnt, conn);
        	}
        	catch (Exception e2) {}
		}
		
		return tierName;
		
	}


	@Override
	public void clearBncDecoders() throws Exception {
	
		CallableStatement stmnt = null;
        Connection conn = DBHelper.getConnection(true);
        ResultSet rs = null;       
        
        try {	        	
			stmnt = conn.prepareCall("{call dbo.deleteBncDecoders()}");
			stmnt.execute();
		    DBHelper.closeConnection(rs, stmnt, conn);
        } catch (Exception e) {            
        	logger.warn(e.getMessage());
            DBHelper.closeConnection(rs, stmnt, conn);
        }
	}
	
	@Override
	public void saveBncDecoders(List<Map<String, Object>> decoders) throws Exception {

		ResultSet rs = null;
		Connection conn = DBHelper.getConnection();
		final PreparedStatement stmnt = conn.prepareStatement("{call dbo.SaveBncDecoders(?, ?, ?, ?, ?, ?, ?)}");
		        
        try
        {	      
        	conn.setAutoCommit(true);
        	for (Map<String, Object> d : decoders) {
        		try {
	        		stmnt.setString(1, (String)d.get("unitAddress") );
		        	stmnt.setInt(2, (Integer)d.get("tierNumber"));
		        	stmnt.setString(3, (String)d.get("cableOperator"));
		        	stmnt.setString(4, (String)d.get("modelID"));
		        	stmnt.setString(5, (String)d.get("status"));
		        	stmnt.setString(6, (String)d.get("headend"));
		        	
		        	Long createdOnLong = (Long)d.get("createdOn");
		        	Timestamp createdOn = new Timestamp(createdOnLong);
		        	
		        	
		        	stmnt.setTimestamp(7, createdOn);//new java.sql.Timestamp(d.get("createdOn").getTime()));
		        	stmnt.addBatch();
        		}
        		catch (Exception e) {        			
        		}        	
        	} 	
								
        	stmnt.executeBatch();
        	DBHelper.closeConnection(rs, stmnt, conn);			
        }
		catch (Exception e) {
        	logger.warn(e.getMessage());
        	try {
        		DBHelper.closeConnection(rs, stmnt, conn);
        	}
        	catch (Exception e1) {
        		throw new Exception("Error saving BNC decoders for audit report. Error: " + e1.getMessage());
        	}        		
		}        
	}


	@Override
	public List<FatTierDTO> getAllFatTiers() {
		
		List<FatTierDTO> list = new ArrayList<FatTierDTO>();
		
		PreparedStatement stmnt = null;
		ResultSet rs = null;
		Connection conn = null;
	
		try {		
			conn = DBHelper.getConnection(true);
			
			//String query = "select encoderid, tiernumber from tb_channel where IsTemplateTierExtended = 1 order by templateid asc";
			String query = "select encoderid, tiernumber, IsTemplateTierExtended from tb_channel where TemplateID > 0 order by templateid asc";
			stmnt = conn.prepareStatement(query);
			rs = stmnt.executeQuery();		  
				
			while (rs.next()) { 
				FatTierDTO fat = new FatTierDTO(rs.getInt(1), rs.getInt(2), rs.getInt(3));
				list.add(fat);
			}			
				
		    DBHelper.closeConnection(rs, stmnt, conn);		
		}
		catch (Exception e) {
        	logger.warn(e.getMessage());
        	try {
        		DBHelper.closeConnection(rs, stmnt, conn);
        	}
        	catch (Exception e2) {}
		}
		
		return list;
	}
	
	@Override
	public void deleteFatTiers() {
		
		PreparedStatement stmnt = null;
		ResultSet rs = null;
		Connection conn = null;
	
		try {		
			conn = DBHelper.getConnection(true);
			
			String query = "delete from dbo.ChannellParsedTiers";
			stmnt = conn.prepareStatement(query);
			stmnt.executeUpdate();		  
								
		    DBHelper.closeConnection(rs, stmnt, conn);		
		}
		catch (Exception e) {
        	logger.warn(e.getMessage());
        	try {
        		DBHelper.closeConnection(rs, stmnt, conn);
        	}
        	catch (Exception e2) {}
		}
		
	}

	@Override
	public void saveParsedFatTiers(List<FatTierDTO> completeFatTiers) throws Exception {
		
		ResultSet rs = null;
		Connection conn = DBHelper.getConnection(true);
		final PreparedStatement stmnt = conn.prepareStatement("insert into dbo.ChannellParsedTiers values (?, ?, ?)");
		
		try {		
			conn.setAutoCommit(true);
			
			for (FatTierDTO fat : completeFatTiers) {
				
				for (Integer i : fat.getTiers()) {
					stmnt.setInt(1, fat.getEncoderId());
					stmnt.setInt(2, fat.getFatTier());
					stmnt.setInt(3, i);
					stmnt.addBatch();
				}
			}
			
			stmnt.executeBatch();
        		
		    DBHelper.closeConnection(rs, stmnt, conn);		
		}
		catch (Exception e) {
        	logger.warn(e.getMessage());
        	try {
        		DBHelper.closeConnection(rs, stmnt, conn);
        	}
        	catch (Exception e2) {}
		}
		
	}


	@Override
	public IrdEditDTO getMD(Integer id) throws Exception {
		
		CallableStatement stmnt = null;
		Connection conn = DBHelper.getConnection(true);
		ResultSet rs = null;
		IrdEditDTO ird = new IrdEditDTO();
	        
		try {	        	
			stmnt = conn.prepareCall("{call das.GetMDByDecoderID(?)}");
			stmnt.setInt("DecoderId", id);
			rs = DBHelper.getResultSet(stmnt);
			
			int encoderId = 0;
			String encoderName = "";
			
			if(rs != null && rs.next()){
			
				ird.setId(rs.getInt("DecoderId")); 
				ird.setInventory(rs.getBoolean("IsInventory"));				
				
				if (ird.isInventory()) {
					ird.setUnitAddress(rs.getString("UnitAddress"));
				}
				else {
					ird.setUnitAddress(rs.getString("IRTAddress"));
				}
				
				ird.setComments(rs.getString("Comments"));
				ird.setSerialNumber(rs.getString("DecoSerial"));
				ird.setModelName(rs.getString("ModelID"));
				ird.setFirmwareVersion(rs.getString("FirmwareVersionId"));
				ird.setFirmwareVersionId(rs.getString("FirmwareVersionId"));
				ird.setCableOperatorCode(rs.getString("CableCode"));
				ird.setCableOperatorName(rs.getString("CableName"));
				ird.setCountryName(rs.getString("CountryName"));
				ird.setHeadendCode(rs.getString("HeadEndCode"));
				ird.setHeandName(rs.getString("Location"));
				ird.setTranscoders( new ArrayList<TranscoderDTO>());
				
				encoderId = rs.getInt("EncoderID");
				encoderName = rs.getString("EncoderName");
				
				ird.setEncoderId(encoderId);				
				
				ird.setCreatedDateFormatted(rs.getTimestamp("CreatedOn"));
				ird.setUpdatedDateFormatted(rs.getTimestamp("LastModifiedOn"));
				ird.setUpdatedBy(rs.getString("LastModifiedBy"));
			}
		
			//set up transcoder  
			boolean isResultSet = stmnt.getMoreResults();
			if (isResultSet) {
					
				rs = stmnt.getResultSet();
				if(rs != null){
					
					while (rs.next()){
						
						TranscoderDTO transc = new TranscoderDTO();
						transc.setUnitAddress(rs.getString("UnitAddress"));
						transc.setSerialNumber(rs.getString("DecoSerial"));
						transc.setComments(rs.getString("Comments"));
							
						transc.setOperation(new OperationDTO(rs.getInt("OperationId"), 
								rs.getString("OperationName"), rs.getBoolean("IsAuthorized"), 
								rs.getString("StartDate"), rs.getString("EndDate")));
						
						
						transc.setService(new ServiceDTO(encoderId,
														encoderName,
														rs.getString("TierNumber"),
														rs.getString("TierName"),
														rs.getString("VchNumber"),
														rs.getInt("VchTable")));
						transc.setDecoderServicesIdentity(rs.getInt("DecoderServicesId"));
						transc.setTranscoderId(1);//each ACP can only have one transcoder meaning the order is always 1st
						transc.setDecoderId(rs.getInt("DecoderID"));
						transc.setAuthorized(rs.getBoolean("IsAuthorized"));
						ird.getTranscoders().add(transc);
					}
				}
			}
				
			DBHelper.closeConnection(rs, stmnt, conn);
			    
		} catch (Exception e) {            
			logger.warn(e.getMessage());
			DBHelper.closeConnection(rs, stmnt, conn);
		}
	        
		return ird;
	}

	@Override
	public int saveMD(List<IrdEditDTO> irds) throws Exception {

		CallableStatement stmnt = null;
		Connection conn = DBHelper.getConnection();
		ResultSet rs = null;
		int irdId = 0;
		
		String irdUnitAddressForException = null;
		
		try {
			
			for (IrdEditDTO ird : irds) {
	           
				irdUnitAddressForException = ird.getUnitAddress();
				
				stmnt = conn.prepareCall("{call das.SaveDecoder(?, ?, ?, ?, ?, ?, ?,"
																+ "?, ?, ?, ?, ?, ?,"
																+ "?, ?, ?, ?, ?, ?, "
																+ "?, ?, ?, ?, ?, ?, ?)}");
				
				stmnt.setString("IRTAddress", ird.getIRTUnitAddress());				
				stmnt.setString("UnitAddress", ird.getUnitAddress());
				stmnt.setString("DecoSerial",ird.getIRTSerialNumber());//ird.getSerialNumber());
				stmnt.setInt("OperationID", 10);//new decoder
				stmnt.setString("TestEndDate", "");				
				stmnt.setString("TierName", "");
				stmnt.setString("CableCode", ird.getCableOperatorCode());
				stmnt.setString("HeadEndCode", ird.getHeadendCode());
				stmnt.setString("Comments", ird.getComments());
				stmnt.setString("ModelID", ird.getModelName());
				stmnt.setString("Version", ird.getFirmwareVersion());
				stmnt.setString("UpdatedBy", ird.getLoggedInUser());
					
					
				if (ird.getTransportStreamOutput()==null) {
					stmnt.setNull("TransportStreamOutput", java.sql.Types.INTEGER);
				}
				else {
					stmnt.setInt("TransportStreamOutput", ird.getTransportStreamOutput());	
				}				
					
				if (ird.getFrontPanelEnabled()==null) {
					stmnt.setNull("FrontPanelEnabled", java.sql.Types.INTEGER);
				}
				else {
					stmnt.setInt("FrontPanelEnabled", ird.getFrontPanelEnabled()?1:0);
				}
					
				stmnt.setNull("EMMStream", java.sql.Types.INTEGER);
				stmnt.setNull("EMMProvider", java.sql.Types.INTEGER);
				stmnt.setInt("Active", 1);
				stmnt.setInt("DecoderId", ird.getId());
				stmnt.setString("CreatedBy", ird.getLoggedInUser());
				stmnt.setInt("LocalControl", (ird.getControlPortEnabled()==null ? 0 : (ird.getControlPortEnabled()?1:0)));
				stmnt.setInt("IsInventory", (ird.isInventory())?1:0);
				stmnt.setInt("OwnershipId", ird.getOwnershipId());
				stmnt.setString("SecondaryUnitAddress", ird.getSecondaryUnitAddress());
				stmnt.setInt("PresetID", ird.getPresetId());
				stmnt.setInt("EncoderID", ird.getEncoderId());
					
				if (ird.getSdiOutput() == null) {
					stmnt.setNull("SDIOutput", java.sql.Types.INTEGER);
				}
				else {
					stmnt.setInt("SDIOutput", ird.getSdiOutput());	
				}
					
				rs = stmnt.executeQuery();
					
				if (rs.next()) {        	
					irdId = rs.getInt(1);
				}
				else {
					String message = "No Response from create decoder stored procedure";
					logger.warn(message + "_"+ird.toString());
					throw new Exception(message);
				}
					
				if(irdId <= 0)
				{
					String message = "Unit Address is already in use.";
					logger.warn(message + "_"+ird.toString());
					throw new Exception(message);
				}
					
				if (!ird.isInventory()) {
					saveIrdServices(ird.getLoggedInUser(), ird.getTranscoders(), irdId, conn);					
				}
					
				
			}//for
			
			//make sure no db exceptions before going to the bnc
//			for (IrdEditDTO ird : irds) {
//				if (!ird.isInventory()) {
//					bncService.createIRDOnBNC(ird);
//				}
//			}
			if (!irds.get(0).isInventory()) {//if acp0 / anchor is not inventory - no need for null checks here if we got this far.
				bncService.createIRDOnBNCBatch(irds);			
			}
			
			//if no db exceptions or bnc exceptions then commit
			DBHelper.commitAndCloseConnection(rs, stmnt, conn);
		
		} catch (Exception e) {            
			logger.warn(e.getMessage());
			DBHelper.rollbackAndCloseConnection(rs, stmnt, conn);
			//propogate exception
			throw new Exception("Problem saving decoder: UA: " + irdUnitAddressForException + "\nmessaage: " + e.getMessage());
		}
		return irdId;
	}


	@Override
	public List<String> getACPsForIRT(String unitAddress) {
		
		List<String> uas = new ArrayList<String>();
		PreparedStatement stmnt = null;
		ResultSet rs = null;
		Connection conn = null;
	
		try {		
			conn = DBHelper.getConnection(true);
			
			String query = "select unitaddress from das.Decoder where irtaddress = ? and unitaddress <> irtaddress";
			stmnt = conn.prepareStatement(query);
			stmnt.setString(1, unitAddress);
			rs = stmnt.executeQuery();		  
				
			while (rs.next()) { 
				uas.add(rs.getString(1));				
			}			
				
		    DBHelper.closeConnection(rs, stmnt, conn);		
		}
		catch (Exception e) {
        	logger.warn(e.getMessage());
        	try {
        		DBHelper.closeConnection(rs, stmnt, conn);
        	}
        	catch (Exception e2) {}
		}
		
		return uas;
		
	}   
		
}
