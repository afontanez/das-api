package com.hbola.das.dal;

import java.util.List;

import com.hbola.das.dto.CountryDTO;

public interface CountryDALI {

	List<CountryDTO> getList() throws Exception;

}
