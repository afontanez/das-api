package com.hbola.das.dal;

import java.util.List;
import java.util.Map;

import com.hbola.das.dto.FatTierDTO;
import com.hbola.das.dto.IrdEditDTO;
import com.hbola.das.dto.OperationDTO;
import com.hbola.das.dto.OwnershipDTO;
import com.hbola.das.dto.RequestDecodersFiltered;

public interface IrdDALI {

	void checkCableCodeHeadend(IrdEditDTO ird) throws Exception;
	
	Map<String, Object> getIrds(RequestDecodersFiltered filter) throws Exception;
	
	int saveIrd(IrdEditDTO ird) throws Exception;
	
	IrdEditDTO getIrd(int irdId) throws Exception;

	List<OperationDTO> getOperations() throws Exception;

	List<OwnershipDTO> getOwnerships() throws Exception;
	
	int getBNCPreset(int id) throws Exception;
	
	String getTierName(String tierNumber);

	void saveBncDecoders(List<Map<String, Object>> decoders) throws Exception;

	void clearBncDecoders() throws Exception;

	List<FatTierDTO> getAllFatTiers();

	void saveParsedFatTiers(List<FatTierDTO> completeFatTiers) throws Exception;

	void deleteFatTiers();

	IrdEditDTO getMD(Integer id) throws Exception;

	int saveMD(List<IrdEditDTO> irds) throws Exception;

	List<String> getACPsForIRT(String unitAddress);
}