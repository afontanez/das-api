package com.hbola.das.dal;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import com.hbola.das.dto.HeadendDTO;
import com.hbola.das.helper.DBHelper;

@Component
public class HeadendDALImpl implements HeadendDALI {

	private static final Logger logger = LogManager.getLogger(HeadendDALImpl.class);
	
	@Override
	public int create(HeadendDTO headend) throws Exception {
		CallableStatement stmnt = null;
        Connection conn = DBHelper.getConnection();
        ResultSet rs = null;
        int response = 0;
        
        try {	    
        	
			stmnt = conn.prepareCall("{call das.Save_HeadEnd(?, ?, ?, ?, ?, ?,"
															+ "?, ?, ?, ?, ?, ?,"
															+ "?, ?, ?, ?)}",
	                ResultSet.TYPE_SCROLL_INSENSITIVE,
	                ResultSet.CONCUR_READ_ONLY);
			
			
			if (headend.getCode()==null) {
				stmnt.setNull("HeadEndCode", java.sql.Types.INTEGER);
			}
			else {
				stmnt.setInt("HeadEndCode", headend.getCode());	
			}	
			
			stmnt.setString("CableCode", headend.getCableCode());
			stmnt.setString("Location", headend.getLocation());
			stmnt.setString("Manager", headend.getManager());
			stmnt.setString("Address", headend.getAddress());
			stmnt.setString("AreaCode", headend.getAreaCode());
			stmnt.setString("AreaCode2", headend.getAreaCode2());
			stmnt.setString("Phone", headend.getPhone());
			stmnt.setString("Phone2", headend.getPhone2());
			stmnt.setString("Fax", headend.getFax());
			stmnt.setString("PrimaryEmail", headend.getPrimaryEmail());
			stmnt.setString("SecondaryEmail", headend.getSecondaryEmail());
			stmnt.setString("Pas1Dish", headend.getPas1Dish());
			stmnt.setString("Pas3Dish", headend.getPas3Dish());
			stmnt.setString("Services", headend.getServices());
			stmnt.setString("UpdatedBy", headend.getUpdatedBy());
			
        	rs = DBHelper.getResultSet(stmnt);
			
			if (rs.next()) {        	
				response = rs.getInt(1);
			}
			else {
				String message = "No Response from save headend stored procedure";
				logger.warn(message);
				throw new Exception(message);
			}
			
			DBHelper.commitAndCloseConnection(rs, stmnt, conn);
			
        } catch (Exception e) {            
        	logger.warn(e.getMessage());
            DBHelper.rollbackAndCloseConnection(rs, stmnt, conn);
            
            //propogate exception
            throw new Exception("Problem creating Headend: " + e.getMessage());
        }
        return response;
	}

	@Override
	public int edit(HeadendDTO headend) throws Exception {
		CallableStatement stmnt = null;
        Connection conn = DBHelper.getConnection();
        ResultSet rs = null;
        int response = 0;
        
        try {	    
        	
			stmnt = conn.prepareCall("{call dbo.HBO_u_HeadEnd(?, ?, ?, ?, ?, ?,"
															+ "?, ?, ?, ?, ?, ?,"
															+ "?, ?, ?, ?, ?)}",
	                ResultSet.TYPE_SCROLL_INSENSITIVE,
	                ResultSet.CONCUR_READ_ONLY);
			
			stmnt.setString("CableCode", headend.getCableCode());
			stmnt.setInt("HeadEndCode", headend.getCode());
			stmnt.setString("Location", headend.getLocation());
			stmnt.setString("Manager", headend.getManager());
			stmnt.setString("Address", headend.getAddress());
			stmnt.setString("AreaCode", headend.getAreaCode());
			stmnt.setString("Phone", headend.getPhone());
			stmnt.setString("Phone2", headend.getPhone2());
			stmnt.setString("Fax", headend.getFax());
			stmnt.setString("PrimaryEmail", headend.getPrimaryEmail());
			stmnt.setString("SecondaryEmail", headend.getSecondaryEmail());
			stmnt.setString("Pas1Dish", headend.getPas1Dish());
			stmnt.setString("Pas3Dish", headend.getPas3Dish());
			stmnt.setString("Services", headend.getServices());
			stmnt.setString("UpdatedDate", headend.getUpdatedDate());
			stmnt.setString("CreatedDate", headend.getCreatedDate());
			stmnt.setString("UpdatedBy", headend.getUpdatedBy());
			
        	rs = DBHelper.getResultSet(stmnt);
			/*
			if (rs.next()) {        	
				response = rs.getInt(1);
			}
			else {
				String message = "No Response from extend time to token stored procedure";
				logger.warn(message);
				throw new Exception(message);
			}

			if(response > 0)
				*/
			DBHelper.commitAndCloseConnection(rs, stmnt, conn);
			
        } catch (Exception e) {            
        	logger.warn(e.getMessage());
            DBHelper.rollbackAndCloseConnection(rs, stmnt, conn);
            
            //propogate exception
            throw new Exception("Problem creating Headend: " + e.getMessage());
        }
        return response;
	}

	@Override
	public List<HeadendDTO> getList() throws Exception {
		CallableStatement stmnt = null;
        Connection conn = DBHelper.getConnection(true);
        ResultSet rs = null;
        List<HeadendDTO> headends = new ArrayList<HeadendDTO>();
        
        try {	        	
			stmnt = conn.prepareCall("{call das.GetAllHeadEndLocations()}");
			rs = DBHelper.getResultSet(stmnt);

	        while (rs.next()) {
	        	headends.add(new HeadendDTO(rs.getString("CableCode"), rs.getInt("HeadEndCode"), 
	        			rs.getString("Location"), rs.getString("Manager"), rs.getString("Address"), 
	        			rs.getString("AreaCode"), rs.getString("AreaCode"), rs.getString("Phone"), rs.getString("Phone2"),
	        			rs.getString("Fax"), rs.getString("Pas1Dish"), "", rs.getString("Pas3Dish"),
	        			rs.getString("Services"), rs.getString("CreatedDate"), rs.getString("UpdatedDate"), 
	        			rs.getString("UpdatedBy"), rs.getString("PrimaryEmail"), rs.getString("SecondaryEmail"),
	        			rs.getBoolean("IsActive"), rs.getInt("App_Version")));
	        }
			
		    DBHelper.closeConnection(rs, stmnt, conn);
		    
        } catch (Exception e) {            
        	logger.warn(e.getMessage());
            DBHelper.closeConnection(rs, stmnt, conn);
        }
        
        return headends;
	}

	@Override
	public HeadendDTO getOneByCode(int code, String cableCode) throws Exception {
		CallableStatement stmnt = null;
        Connection conn = DBHelper.getConnection(true);
        ResultSet rs = null;
        HeadendDTO headend = new HeadendDTO();
        
        try {
			stmnt = conn.prepareCall("{call das.GetHeadEndDetail(?, ? )}");
			stmnt.setString("CableCode", cableCode);
			stmnt.setInt("HeadEndCode", code);
			
			rs = DBHelper.getResultSet(stmnt);

	        if (rs.next()) {
	        	
	        	headend = new HeadendDTO(rs.getString("CableCode"), rs.getInt("HeadEndCode"), rs.getString("Location"), rs.getString("Manager"),
	        			rs.getString("Address"), rs.getString("AreaCode"), rs.getString("AreaCode2"), rs.getString("Phone"), rs.getString("Phone2"),rs.getString("Fax"),
	        			rs.getString("Pas1Dish"), "",rs.getString("Pas3Dish"), rs.getString("Services"), rs.getString("CreatedDate"),
	        			rs.getString("UpdatedDate"), rs.getString("UpdatedBy"), rs.getString("PrimaryEmail"),rs.getString("SecondaryEmail"),
	        			true, 0);
	        }
		    DBHelper.closeConnection(rs, stmnt, conn);
		    
        } catch (Exception e) {            
        	logger.warn(e.getMessage());
            DBHelper.closeConnection(rs, stmnt, conn);
            throw new Exception("Problem getting Headend: " + e.getMessage());
        }
        
        return headend;
	}

	@Override
	public List<HeadendDTO> getListByCableCode(String cableCode) throws Exception {
		CallableStatement stmnt = null;
        Connection conn = DBHelper.getConnection(true);
        ResultSet rs = null;
        List<HeadendDTO> headends = new ArrayList<HeadendDTO>();
        
        try {	        	
			stmnt = conn.prepareCall("{call das.GetAllHeadEndByCableCode(?)}");
			stmnt.setString("CableCode", cableCode);
			rs = DBHelper.getResultSet(stmnt);

	        while (rs.next()) {
	        	headends.add(new HeadendDTO(rs.getInt("HeadEndCode"), rs.getString("Location"), rs.getString("CableCode")));
	        }
			
		    DBHelper.closeConnection(rs, stmnt, conn);
		    
        } catch (Exception e) {            
        	logger.warn(e.getMessage());
            DBHelper.closeConnection(rs, stmnt, conn);
        }
        
        return headends;
	}

}
