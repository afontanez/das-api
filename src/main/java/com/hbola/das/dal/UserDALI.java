package com.hbola.das.dal;

import com.hbola.das.dto.UserDTO;

public interface UserDALI {

	public UserDTO getUserByUsername(String username) throws Exception;
	public String getRoleByUsername(String username) throws Exception;
}
