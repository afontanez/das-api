package com.hbola.das.dal;

import java.sql.Connection;
import java.util.List;

import com.hbola.das.dto.HistoryLogDTO;
import com.hbola.das.dto.TransactionLogDTO;

public interface LogDALI {

	List<HistoryLogDTO> getIrdHistoryLog(int decoderId) throws Exception;
	List<TransactionLogDTO> getIrdTransactionLog(int decoderId) throws Exception;
	void saveTransactionLog(String action, String status, String loggedInUser, int rollback, int decoderId,
	int decoderServiceId, Connection conn) throws Exception;
	List<HistoryLogDTO> getMDHistoryLog(Integer decoderId) throws Exception;
}
