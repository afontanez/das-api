package com.hbola.das.dal;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import com.hbola.das.dto.MsoDTO;
import com.hbola.das.helper.DBHelper;

@Component
public class MsoDALImpl implements MsoDALI {

	private static final Logger logger = LogManager.getLogger(MsoDALImpl.class);

	@Override
	public int create(MsoDTO mso) throws Exception {
		CallableStatement stmnt = null;
        Connection conn = DBHelper.getConnection();
        ResultSet rs = null;
        int response = 0;
        
        try {	    
        	
			stmnt = conn.prepareCall("{call das.Save_MSO(?, ?, ?,?, ?, ?,?, ?, ?,?, ?, ?,?, ?, ?, ?)}",
	                ResultSet.TYPE_SCROLL_INSENSITIVE,
	                ResultSet.CONCUR_READ_ONLY);
			
			stmnt.setString("MSOCode", mso.getCode());
			stmnt.setString("MSOName", mso.getName());
			stmnt.setString("Manager", (mso.getManager() != null)?mso.getManager():"");
			stmnt.setString("CountryId", mso.getCountryId());
			stmnt.setString("Address", mso.getAddress());
			stmnt.setString("City", mso.getCity());
			stmnt.setString("AreaCode", mso.getAreaCode());
			stmnt.setString("AreaCode2", mso.getAreaCode2());
			stmnt.setString("Phone", mso.getPhone());
			stmnt.setString("Phone2", mso.getPhone2());
			stmnt.setString("Fax", mso.getFax());
			stmnt.setString("PrimaryEmail", mso.getPrimaryEmail());
			stmnt.setString("SecondaryEmail", mso.getSecondaryEmail());
			stmnt.setString("UpdatedBy", mso.getUpdatedBy());
			stmnt.setString("CreatedDate", mso.getCreatedDate());
			stmnt.setString("UpdatedDate", mso.getUpdatedDate());
			
        	rs = DBHelper.getResultSet(stmnt);
			
			if (rs.next()) {        	
				response = rs.getInt(1);
			}
			else {
				String message = "Problem saving MSO: could not find MSO to update ";
				logger.warn(message);
				throw new Exception(message);
			}
				
			DBHelper.commitAndCloseConnection(rs, stmnt, conn);
			
        } catch (Exception e) {            
        	logger.warn(e.getMessage());
            DBHelper.rollbackAndCloseConnection(rs, stmnt, conn);
            
            //propogate exception
            throw new Exception("Problem creating MSO: " + e.getMessage());
        }
        return response;
	}
	
	@Override
	public int edit(MsoDTO mso) throws Exception {
		CallableStatement stmnt = null;
        Connection conn = DBHelper.getConnection();
        ResultSet rs = null;
        int response = 0;
        
        try {	   
        	
			stmnt = conn.prepareCall("{call dbo.HBO_u_MSO(?, ?, ?,?, ?, ?,?, ?, ?,?, ?, ?,?,?,?)}",
	                ResultSet.TYPE_SCROLL_INSENSITIVE,
	                ResultSet.CONCUR_READ_ONLY);
						
			stmnt.setString("MSOCode", mso.getCode());
			stmnt.setString("MSOName", mso.getName());
			stmnt.setString("Manager", mso.getManager());
			stmnt.setString("CountryId", mso.getCountryId());
			stmnt.setString("Address", mso.getAddress());
			stmnt.setString("City", mso.getCity());
			stmnt.setString("AreaCode", mso.getAreaCode());
			stmnt.setString("Phone", mso.getPhone());
			stmnt.setString("Phone2", mso.getPhone2());
			stmnt.setString("Fax", mso.getFax());
			stmnt.setString("PrimaryEmail", mso.getPrimaryEmail());
			stmnt.setString("SecondaryEmail", mso.getSecondaryEmail());
			stmnt.setString("UpdatedBy", mso.getUpdatedBy());
			stmnt.setString("CreatedDate", mso.getCreatedDate());
			stmnt.setString("UpdatedDate", mso.getUpdatedDate());
			
        	rs = DBHelper.getResultSet(stmnt);
        	
			if (rs.next()) {        	
				response = rs.getInt(1);
			}
			else {
				String message = "No Response from extend time to token stored procedure";
				logger.warn(message);
				throw new Exception(message);
			}

			DBHelper.commitAndCloseConnection(rs, stmnt, conn);
			
        } catch (Exception e) {            
        	logger.warn(e.getMessage());
            DBHelper.rollbackAndCloseConnection(rs, stmnt, conn);
            
            //propogate exception
            throw new Exception("Problem editing MSO: " + e.getMessage());
        }
        return response;
	}

	@Override
	public MsoDTO getOneByCode(String code) throws Exception {
		CallableStatement stmnt = null;
        Connection conn = DBHelper.getConnection(true);
        ResultSet rs = null;
        MsoDTO mso = new MsoDTO();
        
        try {	        	
			stmnt = conn.prepareCall("{call das.GetMSO(?)}");
			stmnt.setString("MSOCode", code);
			
			rs = DBHelper.getResultSet(stmnt);

	        if (rs.next()) {				
	        	mso = new MsoDTO(rs.getString("MSOCode"), rs.getString("MSOName"), rs.getString("Manager"),rs.getString("CountryId"), rs.getString("CountryName"),
	        			rs.getString("Address"),rs.getString("City"),rs.getString("AreaCode"), rs.getString("AreaCode2"), rs.getString("Phone"),
	        			rs.getString("Phone2"),rs.getString("Fax"),rs.getString("PrimaryEmail"),rs.getString("SecondaryEmail"),
	        			rs.getString("CreatedDate"),rs.getString("UpdatedDate"),rs.getString("UpdatedBy"));
	        }
			
		    DBHelper.closeConnection(rs, stmnt, conn);
		    
        } catch (Exception e) {            
        	logger.warn(e.getMessage());
            DBHelper.closeConnection(rs, stmnt, conn);
            throw new Exception("Problem getting MSO: " + e.getMessage());
        }
        
        return mso;
	}

	@Override
	public List<MsoDTO> getList() throws Exception {
		CallableStatement stmnt = null;
        Connection conn = DBHelper.getConnection(true);
        ResultSet rs = null;
        List<MsoDTO> msos = new ArrayList<MsoDTO>();
        
        try {	        	
			stmnt = conn.prepareCall("{call dbo.HBO_s_MSO()}");
			
			rs = DBHelper.getResultSet(stmnt);

	        while (rs.next()) {	        	
	        	msos.add(new MsoDTO(rs.getString("MSOCode"), rs.getString("MSOName"), rs.getString("Manager"),null, rs.getString("CountryName"),
	        			rs.getString("Address"),rs.getString("City"),rs.getString("AreaCode"), rs.getString("AreaCode"), rs.getString("Phone"),
	        			rs.getString("Phone2"),rs.getString("Fax"),rs.getString("PrimaryEmail"),rs.getString("SecondaryEmail"),
	        			rs.getString("CreatedDate"),rs.getString("UpdatedDate"),rs.getString("UpdatedBy")));
	        }
			
		    DBHelper.closeConnection(rs, stmnt, conn);
		    
        } catch (Exception e) {            
        	logger.warn(e.getMessage());
            DBHelper.closeConnection(rs, stmnt, conn);
            throw new Exception("Problem getting list of MSO: " + e.getMessage());
        }
        
        return msos;
	}
	
}
