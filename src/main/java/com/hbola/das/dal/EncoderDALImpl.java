package com.hbola.das.dal;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import com.hbola.das.dto.EncoderDTO;
import com.hbola.das.dto.ServiceDTO;
import com.hbola.das.helper.DBHelper;

@Component
public class EncoderDALImpl implements EncoderDALI {
	
	private static final Logger logger = LogManager.getLogger(EncoderDALImpl.class);
	
	/* (non-Javadoc)
	 * @see com.hbola.das.dal.EncoderDALI#getEncoders(java.lang.Boolean)
	 */
	@Override
	public List<EncoderDTO> getEncoders(Boolean implementsTemplate) throws Exception {
		CallableStatement stmnt = null;
        Connection conn = DBHelper.getConnection();
        ResultSet rs = null;
        List<EncoderDTO> encoders = new ArrayList<EncoderDTO>();
        
        try {	    
        	if(implementsTemplate)
        		stmnt = conn.prepareCall("{call das.GetAllEncoderIdsWithTemplate()}");
        	else
        		stmnt = conn.prepareCall("{call das.GetAllEncoderIdsWithOutTemplate()}");
			rs = DBHelper.getResultSet(stmnt);

	        while (rs.next()) {
	        	encoders.add(new EncoderDTO(rs.getInt("EncoderID"), rs.getInt("ImplementsTemplate"), rs.getString("EncoderName")));
	        }
			
		    DBHelper.commitAndCloseConnection(rs, stmnt, conn);
		    
        } catch (Exception e) {            
        	logger.warn(e.getMessage());
            DBHelper.rollbackAndCloseConnection(rs, stmnt, conn);
        }
        
        return encoders;
	}
	
	/* (non-Javadoc)
	 * @see com.hbola.das.dal.EncoderDALI#getServices()
	 */
	@Override
	public List<ServiceDTO> getServices() throws Exception {
		CallableStatement stmnt = null;
        Connection conn = DBHelper.getConnection();
        ResultSet rs = null;
        List<ServiceDTO> services = new ArrayList<ServiceDTO>();
        
        try {	    
        	stmnt = conn.prepareCall("{call das.GetAllChannelsOrderByTemplateIDAndEncoderId()}");
			rs = DBHelper.getResultSet(stmnt);

	        while (rs.next()) {
	        	services.add(new ServiceDTO(rs.getInt("EncoderID"), rs.getString("TierNumber"), rs.getString("TierName"), rs.getString("VchNumber"), rs.getInt("VchTable"), rs.getInt("IsTemplateTierExtended"), rs.getInt("TemplateID")));
	        }
			
		    DBHelper.commitAndCloseConnection(rs, stmnt, conn);
		    
        } catch (Exception e) {            
        	logger.warn(e.getMessage());
            DBHelper.rollbackAndCloseConnection(rs, stmnt, conn);
        }
        
        return services;
	}
}
