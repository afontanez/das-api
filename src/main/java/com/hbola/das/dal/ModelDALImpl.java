package com.hbola.das.dal;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import com.hbola.das.dto.FirmwareVersionDTO;
import com.hbola.das.dto.ModelDTO;
import com.hbola.das.dto.PresetDTO;
import com.hbola.das.helper.DBHelper;

@Component
public class ModelDALImpl implements ModelDALI{
	
	private static final Logger logger = LogManager.getLogger(ModelDALImpl.class);
	
	
	/* (non-Javadoc)
	 * @see com.hbola.das.dal.ModelDALI#getModels()
	 */
	@Override
	public List<ModelDTO> getModels() throws Exception {
		CallableStatement stmnt = null;
        Connection conn = DBHelper.getConnection();
        ResultSet rs = null;
        List<ModelDTO> models = new ArrayList<ModelDTO>();
        
        try {	        	
			stmnt = conn.prepareCall("{call das.GetAllDecoderModels()}");
			rs = DBHelper.getResultSet(stmnt);

	        while (rs.next()) {
	        	models.add(new ModelDTO(rs.getString("ModelID"), rs.getInt("SelectableServices"), rs.getInt("ImplementsTemplate")));
	        }
			
		    DBHelper.commitAndCloseConnection(rs, stmnt, conn);
		    
        } catch (Exception e) {            
        	logger.warn(e.getMessage());
            DBHelper.rollbackAndCloseConnection(rs, stmnt, conn);
        }
        
        return models;
	}
	
	/* (non-Javadoc)
	 * @see com.hbola.das.dal.ModelDALI#getFirmwareVersions()
	 */
	@Override
	public List<FirmwareVersionDTO> getFirmwareVersions() throws Exception {
		CallableStatement stmnt = null;
        Connection conn = DBHelper.getConnection();
        ResultSet rs = null;
        List<FirmwareVersionDTO> firmwareVersions = new ArrayList<FirmwareVersionDTO>();
        
        try {	        	
			stmnt = conn.prepareCall("{call das.GetAllDecoderModelIDVersions}");
			rs = DBHelper.getResultSet(stmnt);

	        while (rs.next()) {
	        	firmwareVersions.add(new FirmwareVersionDTO(rs.getString("Version"), rs.getString("ModelID")));
	        }
			
		    DBHelper.commitAndCloseConnection(rs, stmnt, conn);
		    
        } catch (Exception e) {            
        	logger.warn(e.getMessage());
            DBHelper.rollbackAndCloseConnection(rs, stmnt, conn);
        }
        
        return firmwareVersions;
	}
	
	/* (non-Javadoc)
	 * @see com.hbola.das.dal.ModelDALI#getPresets()
	 */
	@Override
	public List<PresetDTO> getPresets() throws Exception {
		CallableStatement stmnt = null;
        Connection conn = DBHelper.getConnection();
        ResultSet rs = null;
        List<PresetDTO> presets = new ArrayList<PresetDTO>();
        
        try {	        	
			stmnt = conn.prepareCall("{call das.GetAllPresets}");
			rs = DBHelper.getResultSet(stmnt);

	        while (rs.next()) {	        	                                                            	
	        	presets.add(new PresetDTO(rs.getInt("PresetId"), rs.getInt("BNC_PresetID"), rs.getString("PresetName"), rs.getInt("EncoderID"), rs.getString("ModelId"), null));
	        }
			
		    DBHelper.commitAndCloseConnection(rs, stmnt, conn);
		    
        } catch (Exception e) {            
        	logger.warn(e.getMessage());
            DBHelper.rollbackAndCloseConnection(rs, stmnt, conn);
        }
        
        return presets;
	}
	
}
