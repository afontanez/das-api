package com.hbola.das.controller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.hbola.das.dto.ApiResponse;
import com.hbola.das.dto.HistoryLogDTO;
import com.hbola.das.dto.TransactionLogDTO;
import com.hbola.das.service.LogServiceI;

@RestController
@EnableWebMvc
@RequestMapping("/log")
public class LogController {

	private static final Logger logger = LogManager.getLogger(LogController.class);
	
	@Autowired
	private LogServiceI logService;

	/**
	 * Read one by Id (cRud)
	 * @return
	 */
	@GetMapping(value = "/history/{modelId}/{id}", produces = "application/json")
	public ResponseEntity<ApiResponse> getHistoryLog(@PathVariable String modelId, @PathVariable Integer id) {
		
		ApiResponse response = new ApiResponse(ApiResponse.ERROR, ApiResponse.ERROR); 
		try {
			List<HistoryLogDTO> dto = logService.getIrdHistoryLog(modelId, id);
			response = new ApiResponse(ApiResponse.OK, dto);
		}
		catch (Exception e) {				
			logger.warn(e.getMessage());
			response = new ApiResponse(ApiResponse.ERROR, e.getMessage());
		}

		return ResponseEntity.status(HttpStatus.OK).body(response);
	}
	
	@GetMapping(value = "/transactions/{id}", produces = "application/json")
	public ResponseEntity<ApiResponse> getTransactionsLog(@PathVariable Integer id){
		ApiResponse response = new ApiResponse(ApiResponse.ERROR, ApiResponse.ERROR); 
		try {
			List<TransactionLogDTO> dto = logService.getIrdTransactionLog(id);
			response = new ApiResponse(ApiResponse.OK, dto);
		}
		catch (Exception e) {				
			logger.warn(e.getMessage());
			response = new ApiResponse(ApiResponse.ERROR, e.getMessage());
		}

		return ResponseEntity.status(HttpStatus.OK).body(response);
	}
	
}
