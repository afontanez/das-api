package com.hbola.das.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.hbola.das.dto.ACTION_NAME;
import com.hbola.das.dto.ApiResponse;
import com.hbola.das.dto.HeadendDTO;
import com.hbola.das.service.HeadendServiceI;
import com.hbola.das.service.LogServiceI;

@RestController
@EnableWebMvc
@RequestMapping("/headend")
public class HeadendController {

	private static final Logger logger = LogManager.getLogger(HeadendController.class);
	
	@Autowired
	private HeadendServiceI headendService;
	
	@Autowired 
	private LogServiceI logService;

	/**
	 * Create (Crud)
	 * @param headend
	 * @return
	 */
	@PostMapping(value = "/save", produces = "application/json", consumes = "application/json")
	public ResponseEntity<ApiResponse> create(HttpServletRequest request, @RequestBody HeadendDTO headend){
		
		ApiResponse response = new ApiResponse(ApiResponse.OK, ApiResponse.OK); 
		int rollback = 0;
		String action = ACTION_NAME.ACTION_EDIT_HEADEND;
		
		String username = request.getHeader("username") != null?request.getHeader("username").toString():"username was null";
		try {			
			headend.setUpdatedBy(username);
			if(headend.getCode() != null && headend.getCode() <= 0){
				headend.setCode(null);
				action = ACTION_NAME.ACTION_CREATE_HEADEND;
			}			
			response.setData(headendService.create(headend));
		}
		catch (Exception e) {
			logger.warn(e.getMessage());
			rollback = 1;
			response = new ApiResponse(ApiResponse.ERROR, e.getMessage());
		}
		finally{		
			try {				
				logService.saveTransactionLog(action, "", username, rollback, 0, 0, null);
			} catch (Exception e) {
				logger.warn(e.getMessage());
			}
		}

		return ResponseEntity.status(HttpStatus.OK).body(response);
	}
	
	/**
	 * Update (crUd)
	 * @param headend
	 * @return
	 */
	@PostMapping(value = "/edit", produces = "application/json", consumes = "application/json")
	public ResponseEntity<ApiResponse> edit(HttpServletRequest request, @RequestBody HeadendDTO headend){
		
		ApiResponse response = new ApiResponse(ApiResponse.OK, ApiResponse.OK);
		int rollback = 0;
		
		try {
			response.setData(headendService.edit(headend));
		}
		catch (Exception e) {
			logger.warn(e.getMessage());
			rollback = 1;
			response = new ApiResponse(ApiResponse.ERROR, e.getMessage());
		}
		finally{		
			try {
				String username = request.getHeader("username") != null?request.getHeader("username").toString():"username was null";
				logService.saveTransactionLog(ACTION_NAME.ACTION_EDIT_HEADEND, "", username, rollback, 0, 0, null);
			} catch (Exception e) {
				logger.warn(e.getMessage());
			}
		}

		return ResponseEntity.status(HttpStatus.OK).body(response);
	}
	
	/**
	 * GetList (cRud)
	 * @return
	 */
	@GetMapping(value = "/", produces = "application/json")
	public ResponseEntity<ApiResponse> list(){
		ApiResponse response = new ApiResponse(ApiResponse.OK, ApiResponse.OK); 
		try{
			response.setData(headendService.getList());
		}catch(Exception e){
			logger.warn(e.getMessage());
			response = new ApiResponse(ApiResponse.ERROR, e.getMessage());
		}
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}
	
	/**
	 * GetList (cRud)
	 * @return
	 */
	@GetMapping(value = "/{code}/{cableCode}", produces = "application/json")
	public ResponseEntity<ApiResponse> get(HttpServletRequest request, @PathVariable int code, @PathVariable String cableCode){
		ApiResponse response = new ApiResponse(ApiResponse.OK, ApiResponse.OK); 
		try{
			response.setData(headendService.getOneByCode(code, cableCode));
		}catch(Exception e){
			logger.warn(e.getMessage());
			response = new ApiResponse(ApiResponse.ERROR, e.getMessage());
		}
		finally{		
			try {
				String username = request.getHeader("username") != null?request.getHeader("username").toString():"username was null";
				logService.saveTransactionLog(ACTION_NAME.ACTION_VIEW_HEADEND, "", username, 0, 0, 0, null);
			} catch (Exception e) {
				logger.warn(e.getMessage());
			}
		}
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}
	
}
