package com.hbola.das.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.hbola.das.dto.ACTION_NAME;
import com.hbola.das.dto.ApiResponse;
import com.hbola.das.dto.IrdEditDTO;
import com.hbola.das.dto.NewRequestDecodersFilteredDTO;
import com.hbola.das.dto.RequestDecodersFiltered;
import com.hbola.das.dto.TranscoderDTO;
import com.hbola.das.service.IrdServiceI;
import com.hbola.das.service.LogServiceI;
import com.hbola.das.service.SetupIrdServiceI;

@RestController
@EnableWebMvc
@RequestMapping("/ird")
public class IrdController {
	
	private static final Logger logger = LogManager.getLogger(IrdController.class);
	
	@Autowired
	private IrdServiceI irdService;

	@Autowired
	private SetupIrdServiceI setupIrdService;

	@Autowired 
	private LogServiceI logService;
	
	@GetMapping(value = "/setup/modelsdb", produces = "application/json")
	public ResponseEntity<ApiResponse> setupModelsCableOperators(){
		
		ApiResponse response = new ApiResponse(ApiResponse.OK, ApiResponse.OK); 
		try{
			response.setData(setupIrdService.getIrdSetupModelsCableOperators());
		}catch(Exception e){
			logger.warn(e.getMessage());
			response = new ApiResponse(ApiResponse.ERROR, e.getMessage());
		}
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}

	
	@GetMapping(value = "/setup/decodersdb", produces = "application/json")
	public ResponseEntity<ApiResponse> setupDecodersOperations(@RequestParam("implementsTemplate") int implementsTemplate){
		ApiResponse response = new ApiResponse(ApiResponse.OK, ApiResponse.OK); 
		try{
			response.setData(setupIrdService.getIrdSetupEncodersOperations(implementsTemplate));
		}catch(Exception e){
			logger.warn(e.getMessage());
			response = new ApiResponse(ApiResponse.ERROR, e.getMessage());
		}
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}
	
	@GetMapping(value = "/setup/ownerships", produces = "application/json")
	public ResponseEntity<ApiResponse> setupOwnerships(){
		
		ApiResponse response = new ApiResponse(ApiResponse.OK, ApiResponse.OK); 
		try{
			response.setData(irdService.getIrdSetupOwnerships());
		}catch(Exception e){
			logger.warn(e.getMessage());
			response = new ApiResponse(ApiResponse.ERROR, e.getMessage());
		}
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}

	/**
	 * Does pagination and also does filtering
	 */
	@PostMapping(value = "/newsearch", produces = "application/json", consumes = "application/json")
	public ResponseEntity<ApiResponse> newListByFilter(@RequestBody NewRequestDecodersFilteredDTO filter){
		ApiResponse response = new ApiResponse(ApiResponse.OK, ApiResponse.OK); 
		try{
			response.setData(irdService.newReadIrdsFromDB(filter));
		}catch(Exception e){
			logger.warn(e.getMessage());
			response = new ApiResponse(ApiResponse.ERROR, e.getMessage());
		}
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}

	
	/**
	 * Search (cRud)
	 * @param Ird
	 * @return
	 * This is the Api which return decoders with services
	 */
	@PostMapping(value = "/search", produces = "application/json", consumes = "application/json")
	public ResponseEntity<ApiResponse> listByFilter(@RequestBody RequestDecodersFiltered filter){
		ApiResponse response = new ApiResponse(ApiResponse.OK, ApiResponse.OK); 
		try{
			response.setData(irdService.readIrdsFromDB(filter));
		}catch(Exception e){
			logger.warn(e.getMessage());
			response = new ApiResponse(ApiResponse.ERROR, e.getMessage());
		}
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}
	
	/**
	 * Create (Crud)
	 * @param ird
	 * @return
	 */
	@PostMapping(value = "/", produces = "application/json", consumes = "application/json")
	public ResponseEntity<ApiResponse> create(HttpServletRequest request, @RequestBody IrdEditDTO ird){
		
		List<TranscoderDTO> tdto = ird.getTranscoders();
		tdto.forEach(transcoderDTO -> logger.info("transcoder value: "+transcoderDTO.toString()+"\r\n"));
	
		
		ApiResponse response = new ApiResponse(ApiResponse.OK, ApiResponse.OK); 
		int decoderIdFirst = ird.getId();
		int decoderIdSecond = 0;
		int rollback = 0;
		String action = ACTION_NAME.ACTION_CREATE_DECODER;
		if (ird.isInventory()) {
			action = ACTION_NAME.ACTION_CREATE_DECODER_INVENTORY;
		}
		
		try {
			decoderIdSecond = irdService.createIrd(ird);
		}
		catch (Exception e) {
			logger.warn(e.getMessage());
			rollback = 1;
			response = new ApiResponse(ApiResponse.ERROR, e.getMessage());
		}
		finally{
			if(decoderIdFirst > 0)
			{
				action = ACTION_NAME.ACTION_EDIT_DECODER;
				decoderIdSecond = decoderIdFirst;
			}
			
			try {
				String username = request.getHeader("username") != null?request.getHeader("username").toString():"username was null";
				logService.saveTransactionLog(action, "", username, rollback, decoderIdSecond, 0, null);
			} catch (Exception e) {
				logger.info("problem saving transaction: e.getMessage: " + e.getMessage());
			}
		}

		return ResponseEntity.status(HttpStatus.OK).body(response);
	}
	
	/**
	 * Read all without Id (cRud)
	 * @return
	 */
	@GetMapping(value = "/", produces = "application/json")
	public ResponseEntity<ApiResponse> list(){		
		ApiResponse response = new ApiResponse(ApiResponse.OK, ApiResponse.OK); 
		try{
//			response.setData(irdService.readIrdsFromCache(null));
			response.setData(irdService.readIrdsFromDB(null));
		}catch(Exception e){
			logger.warn(e.getMessage());
			response = new ApiResponse(ApiResponse.ERROR, e.getMessage());
		}
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}
	
	/**
	 * Read one by Id (cRud)
	 * @return
	 */
	@GetMapping(value = "/{modelId}/{id}", produces = "application/json")
	public ResponseEntity<ApiResponse> read(@PathVariable String modelId, @PathVariable Integer id) {

		ApiResponse response = new ApiResponse(ApiResponse.ERROR, ApiResponse.ERROR); 
		try {
			IrdEditDTO dto = irdService.readIrdById(modelId, id);
			response = new ApiResponse(ApiResponse.OK, dto);
		}
		catch (Exception e) {				
			logger.warn(e.getMessage());
			response = new ApiResponse(ApiResponse.ERROR, e.getMessage());
		}

		return ResponseEntity.status(HttpStatus.OK).body(response);
		
	}
	
	@GetMapping(value = "/getMDUAs/{anchorUnitAddress}", produces = "application/json")
	public ResponseEntity<ApiResponse> getMDUAs(HttpServletRequest request, @PathVariable String anchorUnitAddress) {
		
		ApiResponse response = new ApiResponse(ApiResponse.ERROR, ApiResponse.ERROR); 
		try {
			String[] mdUAs = irdService.setupMDUAs(anchorUnitAddress);
			response = new ApiResponse(ApiResponse.OK, mdUAs);			
		}
		catch (Exception e) {				
			logger.warn(e.getMessage());
			response = new ApiResponse(ApiResponse.ERROR, e.getMessage());
		}
		
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}
	
	@GetMapping(value = "/trip/{id}/{modelNumber}/{unitAddress}", produces = "application/json")
	public ResponseEntity<ApiResponse> trip(HttpServletRequest request, @PathVariable int id, @PathVariable String modelNumber, @PathVariable String unitAddress) {

		ApiResponse response = new ApiResponse(ApiResponse.ERROR, ApiResponse.ERROR); 
		try {
			irdService.tripIrd(modelNumber, unitAddress);
			response = new ApiResponse(ApiResponse.OK, ApiResponse.OK);
						
			try {
				String username = request.getHeader("username") != null?request.getHeader("username").toString():"username was null";
				logService.saveTransactionLog(ACTION_NAME.ACTION_TRIP, "", username, 0, id, 0, null);
			} catch (Exception e) {
				logger.info("problem saving transaction: e.getMessage: " + e.getMessage());
			}
		}
		catch (Exception e) {				
			logger.warn(e.getMessage());
			response = new ApiResponse(ApiResponse.ERROR, e.getMessage());
		}

		return ResponseEntity.status(HttpStatus.OK).body(response);
		
	}
	
	@GetMapping(value = "/loadBncDecodersToDB", produces = "application/json")
	public ResponseEntity<ApiResponse> loadBncDecodersToDB() {

		ApiResponse response = new ApiResponse(ApiResponse.ERROR, ApiResponse.ERROR); 
		try {
			irdService.loadBncDecodersToDB();
			response = new ApiResponse(ApiResponse.OK, ApiResponse.OK);						
		}
		catch (Exception e) {				
			logger.warn(e.getMessage());
			response = new ApiResponse(ApiResponse.ERROR, e.getMessage());
		}

		return ResponseEntity.status(HttpStatus.OK).body(response);
		
	}
	
	@GetMapping(value = "/loadParsedFatTiersToDB", produces = "application/json")
	public ResponseEntity<ApiResponse> loadParsedFattiersToDB() {

		ApiResponse response = new ApiResponse(ApiResponse.ERROR, ApiResponse.ERROR); 
		try {
			irdService.loadParsedFatTiersToDB();
			response = new ApiResponse(ApiResponse.OK, ApiResponse.OK);						
		}
		catch (Exception e) {				
			logger.warn(e.getMessage());
			response = new ApiResponse(ApiResponse.ERROR, e.getMessage());
		}

		return ResponseEntity.status(HttpStatus.OK).body(response);
		
	}
	
}
