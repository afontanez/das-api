package com.hbola.das.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.hbola.das.dto.ApiResponse;
import com.hbola.das.service.InventoryServiceI;


@RestController
@EnableWebMvc
@RequestMapping("/inventory")
public class InventoryController {

	private static final Logger logger = LogManager.getLogger(InventoryController.class);
	
	@Autowired 
	private InventoryServiceI inventoryService;
	
	@Bean(name = "multipartResolver")
	public CommonsMultipartResolver multipartResolver() {
	    CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
	    multipartResolver.setMaxUploadSize(100000);
	    return multipartResolver;
	}
	
	@PostMapping(value = "/upload", produces = "application/json")
	public ResponseEntity<ApiResponse> upload(@RequestParam("user") String user, @RequestParam("file") MultipartFile file){
		
		ApiResponse response = new ApiResponse(ApiResponse.OK, ApiResponse.OK); 
		try{
			
			int numberOfDecdoersAddedToInventory = inventoryService.process(file, user);
			
			response.setData(numberOfDecdoersAddedToInventory);
			
		}catch(Exception e){
			logger.warn(e.getMessage());
			response = new ApiResponse(ApiResponse.ERROR, e.getMessage());
		}
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}
	
}
