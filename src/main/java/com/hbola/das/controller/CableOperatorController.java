package com.hbola.das.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.hbola.das.dto.ACTION_NAME;
import com.hbola.das.dto.ApiResponse;
import com.hbola.das.dto.CableOperatorDTO;
import com.hbola.das.service.CableOperatorServiceI;
import com.hbola.das.service.LogServiceI;

@RestController
@EnableWebMvc
@RequestMapping("/cable_operator")
public class CableOperatorController {

	//hi
	private static final Logger logger = LogManager.getLogger(CableOperatorController.class);
	
	@Autowired
	private CableOperatorServiceI coService;
	
	@Autowired 
	private LogServiceI logService;

	/**
	 * Create (Crud)
	 * @param co
	 * @return
	 */
	@PostMapping(value = "/save", produces = "application/json", consumes = "application/json")
	public ResponseEntity<ApiResponse> save(HttpServletRequest request, @RequestBody CableOperatorDTO co){
		
		ApiResponse response = new ApiResponse(ApiResponse.OK, ApiResponse.OK); 
		int rollback = 0;
		String action = ACTION_NAME.ACTION_EDIT_CABLE_OPERATOR;
		
		String username = "username was null";
		try {
			
			username = request.getHeader("username") != null?request.getHeader("username").toString():"username was null";
			
			co.setUpdatedBy(username);
			if(co.getCode() == null || co.getCode().equals("")){
				co.setCode("0");
				action = ACTION_NAME.ACTION_CREATE_CABLE_OPERATOR;
			}
			response.setData(coService.create(co));
		}
		catch (Exception e) {
			logger.warn(e.getMessage());
			rollback = 1;
			response = new ApiResponse(ApiResponse.ERROR, e.getMessage());
		}
		finally{		
			try {
				logService.saveTransactionLog(action, "", username, rollback, 0, 0, null);
			} catch (Exception e) {
				logger.warn(e.getMessage());
			}
		}

		return ResponseEntity.status(HttpStatus.OK).body(response);
	}
	
	/**
	 * GetList (cRud)
	 * @return
	 */
	@GetMapping(value = "/", produces = "application/json")
	public ResponseEntity<ApiResponse> list(){
		ApiResponse response = new ApiResponse(ApiResponse.OK, ApiResponse.OK); 
		try{
			response.setData(coService.getList());
		}catch(Exception e){
			logger.warn(e.getMessage());
			response = new ApiResponse(ApiResponse.ERROR, e.getMessage());
		}
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}
	
	/**
	 * GetList (cRud)
	 * @return
	 */
	@GetMapping(value = "/{code}", produces = "application/json")
	public ResponseEntity<ApiResponse> get(HttpServletRequest request, @PathVariable String code){
		ApiResponse response = new ApiResponse(ApiResponse.OK, ApiResponse.OK); 
		try{
			response.setData(coService.getOneByCode(code));
		}catch(Exception e){
			logger.warn(e.getMessage());
			response = new ApiResponse(ApiResponse.ERROR, e.getMessage());
		}
		finally{		
			try {
				String username = request.getHeader("username") != null?request.getHeader("username").toString():"username was null";
				logService.saveTransactionLog(ACTION_NAME.ACTION_VIEW_CABLE_OPERATOR, "", username, 0, 0, 0, null);
			} catch (Exception e) {
				logger.warn(e.getMessage());
			}
		}
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}
	
}
