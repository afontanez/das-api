package com.hbola.das.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.hbola.das.dto.ApiResponse;
import com.hbola.das.service.CountryServiceI;

@RestController
@EnableWebMvc
@RequestMapping("/country")
public class CountryController {

	private static final Logger logger = LogManager.getLogger(CountryController.class);
	
	@Autowired
	private CountryServiceI countryService;
	
	/**
	 * GetList (cRud)
	 * @return
	 */
	@GetMapping(value = "/", produces = "application/json")
	public ResponseEntity<ApiResponse> list(){
		ApiResponse response = new ApiResponse(ApiResponse.OK, ApiResponse.OK); 
		try{
			response.setData(countryService.getList());
		}catch(Exception e){
			logger.warn(e.getMessage());
			response = new ApiResponse(ApiResponse.ERROR, e.getMessage());
		}
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}
	
}
