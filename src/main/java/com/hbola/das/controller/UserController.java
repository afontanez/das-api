package com.hbola.das.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.hbola.das.dto.ACTION_NAME;
import com.hbola.das.dto.ApiResponse;
import com.hbola.das.dto.RequestGetRole;
import com.hbola.das.dto.UserLoginDTO;
import com.hbola.das.service.LogServiceI;
import com.hbola.das.service.UserServiceI;

@RestController
@EnableWebMvc
@RequestMapping("/user")
public class UserController {

	private static final Logger logger = LogManager.getLogger(UserController.class);
	
	@Autowired
	private UserServiceI userService;
	

	@Autowired 
	private LogServiceI logService;
		
	@PostMapping(value = "/login", produces = "application/json", consumes = "application/json")		
	public ResponseEntity<ApiResponse> login(HttpServletRequest request, @RequestBody UserLoginDTO login){
		
		ApiResponse response = new ApiResponse(ApiResponse.OK, ApiResponse.OK); 
		try{
			response.setData(userService.login(login));
			logService.saveTransactionLog(ACTION_NAME.ACTION_LOGIN, "", login.getUsername(), 0, 0, 0, null);
			
		}catch(Exception e){
			logger.warn(e.getMessage());
			response = new ApiResponse(ApiResponse.ERROR, e.getMessage());
		}
		return ResponseEntity.status(HttpStatus.OK).body(response);		
	}
	
	/**
	 * This method handles logout requests.
	 * Toggle the handlers if you are RememberMe functionality is useless in your app.
	 */
	/*
	@GetMapping(value="/logout", produces = "application/json")
	public ResponseEntity<ResponseUserLogin> logout (HttpServletRequest request, HttpServletResponse response){
		return ResponseEntity.status(HttpStatus.OK).body(new ResponseUserLogin(MESSAGE_CODE.SUCCESS));
	}
	*/
	
	@PostMapping(value = "/getRole", produces = "application/json", consumes = "application/json")		
	public ResponseEntity<ApiResponse> getRoleByUsername(@RequestBody RequestGetRole roleRequest){
				
		ApiResponse response = new ApiResponse(ApiResponse.OK, ApiResponse.OK); 
		try{
			response.setData(userService.getRoleByUsername(roleRequest.getUsername()));
			
		}catch(Exception e){
			logger.warn(e.getMessage());
			response = new ApiResponse(ApiResponse.ERROR, e.getMessage());
		}
		return ResponseEntity.status(HttpStatus.OK).body(response);
		
	}
}
