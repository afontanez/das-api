package com.hbola.das.utils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class StringUtilsTest {

	@Test
	public void truncateUA() {
		
		String ua = StringUtils.truncateUA(null);
		assertEquals(null, ua);
		
		ua = StringUtils.truncateUA("");
		assertEquals("", ua);
		
		ua = StringUtils.truncateUA("-1");
		assertEquals("-1", ua);
		
		ua = StringUtils.truncateUA("abcde");
		assertEquals("abcde", ua);
		
		ua = StringUtils.truncateUA("0000316219638");
		assertEquals("0000316219638", ua);
		
		ua = StringUtils.truncateUA("0000316219638 ");
		assertEquals("0000316219638", ua);

		ua = StringUtils.truncateUA("0000316219638123");
		assertEquals("0000316219638", ua);

		ua = StringUtils.truncateUA("0000316219638123 ");
		assertEquals("0000316219638", ua);		
	}
	
	@Test
	public void parseInventoryDescriptionForModel() {
		
		String model = StringUtils.parseInventoryDescriptionForModel(null);
		assertNull(model);
		
		model = StringUtils.parseInventoryDescriptionForModel("");
		assertNull(model);
		
		model = StringUtils.parseInventoryDescriptionForModel("@$junk5$as");
		assertNull(model);
		
		model = StringUtils.parseInventoryDescriptionForModel("DSR4410MD,RCVR,IRD,F/G");
		assertNotNull(model);
		assertEquals("DSR-4410MD", model);		
	}
	
	@Test
	public void listToDelimitedString() {
		
		String value = StringUtils.listToDelimitedString(null);
		assertNotNull(value);
		assertTrue(value.isEmpty());
		
		List<String> list = new ArrayList<String>();
		value = StringUtils.listToDelimitedString(list);
		assertNotNull(value);
		assertTrue(value.isEmpty());

		list.clear();
		list.add("hi ");
		list.add(" how ");
		list.add(" are you? ");
		value = StringUtils.listToDelimitedString(list);
		assertNotNull(value);
		assertTrue(!value.isEmpty());
		assertEquals("hi | how | are you? |", value);
	}
	
}
