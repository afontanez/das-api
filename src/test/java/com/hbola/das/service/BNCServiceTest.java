//package com.hbola.das.service;
//
//import static org.junit.Assert.assertEquals;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.test.context.ContextConfiguration;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//import org.springframework.test.context.web.AnnotationConfigWebContextLoader;
//import org.springframework.test.context.web.WebAppConfiguration;
//
//import com.hbola.das.dto.IrdBNCDTO;
//import com.hbola.das.dto.ServiceBNCDTO;
//import com.hbola.das.dto.TierDTO;
//import com.hbola.das.service.BNCServiceI;
//import com.test.hbola.das.service.TestSerivceConfiguration;
//
//@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration(classes = {TestSerivceConfiguration.class})
//
////@RunWith(SpringJUnit4ClassRunner.class)   
////@WebAppConfiguration
////@ContextConfiguration(loader = AnnotationConfigWebContextLoader.class, classes={TestSerivceConfiguration.class})
//public class BNCServiceTest {
//
//	@Autowired
//	BNCServiceI bncService;
//
//	private IrdBNCDTO setupDTO() {
//		
//		IrdBNCDTO dto = new IrdBNCDTO();
//		dto.setAffiliateName("HBOLA");
//		dto.setLocationName("HBOLA");
//		dto.setModelNumber("DSR7412");
//		dto.setPresetId("5");
//		dto.setDecimalUnitAddress("0001142161408");
//		dto.setSecondaryDecimalUnitAdress("0001142161409");
//		dto.setName("test 7412 0001142161408");
//		
//		List<TierDTO> tiers = new ArrayList<TierDTO>();
//		tiers.add(new TierDTO("241", null));
//		tiers.add(new TierDTO("242", "delete"));
//		tiers.add(new TierDTO("243", null));
//		dto.setTiers(tiers);
//		
//		List<ServiceBNCDTO> services = new ArrayList<ServiceBNCDTO>();
//		services.add(new ServiceBNCDTO("1", "2410", "106", "delete", false, true, true));
//		services.add(new ServiceBNCDTO("2", "2420", "106", null, true, false, true));
//		services.add(new ServiceBNCDTO("3", "2430", "106", null, true, true, false));
//		services.add(new ServiceBNCDTO("4", "2440", "106", null, true, true, true));
//		services.add(new ServiceBNCDTO("5", "2450", "106", null, true, true, true));
//		services.add(new ServiceBNCDTO("6", "2460", "106", null, true, true, true));
//		services.add(new ServiceBNCDTO("7", "2470", "106", null, true, true, true));
//		services.add(new ServiceBNCDTO("8", "2480", "106", null, true, true, true));
//		services.add(new ServiceBNCDTO("9", "2500", "106", null, true, true, true));
//		services.add(new ServiceBNCDTO("10", "0", "0", null, true, true, true));
//		services.add(new ServiceBNCDTO("11", "0", "0", null, true, true, true));
//		services.add(new ServiceBNCDTO("12", "0", "0", null, true, true, true));
//		dto.setServices(services);
//		
//		return dto;		
//	}
//		
//	@Test
//	public void createBNCJson() {
//
//		String json = bncService.createBNCJson(null);
//		assertEquals("", json);
//		
//		IrdBNCDTO dto = new IrdBNCDTO();
//		json = bncService.createBNCJson(dto);
//		assertEquals("{}", json);
//		
//		dto = setupDTO();
//		
//		json = bncService.createBNCJson(dto);
//		assertEquals(JSON_EXAMPLE, json);				
//	}
//	
//	
//	private static final String JSON_EXAMPLE = "{\"affiliateName\":\"HBOLA\",\"locationName\":\"HBOLA\",\"modelNumber\":\"DSR7412\",\"presetId\":5,\"decimalUnitAddress\":\"0001142161408\",\"secondaryDecimalUnitAdress\":\"0001142161409\",\"name\":\"test 7412 0001142161408\",\"tiers\":[{\"number\":241},{\"number\":242,\"operation\":\"delete\"},{\"number\":243}],\"services\":[{\"transcoderID\":1,\"vcNumber\":2410,\"vcTable\":106,\"operation\":\"delete\",\"hdEnable\":false,\"sdEnable\":true,\"passthruEnable\":true},{\"transcoderID\":2,\"vcNumber\":2420,\"vcTable\":106,\"hdEnable\":true,\"sdEnable\":false,\"passthruEnable\":true},{\"transcoderID\":3,\"vcNumber\":2430,\"vcTable\":106,\"hdEnable\":true,\"sdEnable\":true,\"passthruEnable\":false},{\"transcoderID\":4,\"vcNumber\":2440,\"vcTable\":106,\"hdEnable\":true,\"sdEnable\":true,\"passthruEnable\":true},{\"transcoderID\":5,\"vcNumber\":2450,\"vcTable\":106,\"hdEnable\":true,\"sdEnable\":true,\"passthruEnable\":true},{\"transcoderID\":6,\"vcNumber\":2460,\"vcTable\":106,\"hdEnable\":true,\"sdEnable\":true,\"passthruEnable\":true},{\"transcoderID\":7,\"vcNumber\":2470,\"vcTable\":106,\"hdEnable\":true,\"sdEnable\":true,\"passthruEnable\":true},{\"transcoderID\":8,\"vcNumber\":2480,\"vcTable\":106,\"hdEnable\":true,\"sdEnable\":true,\"passthruEnable\":true},{\"transcoderID\":9,\"vcNumber\":2500,\"vcTable\":106,\"hdEnable\":true,\"sdEnable\":true,\"passthruEnable\":true},{\"transcoderID\":10,\"vcNumber\":0,\"vcTable\":0,\"hdEnable\":true,\"sdEnable\":true,\"passthruEnable\":true},{\"transcoderID\":11,\"vcNumber\":0,\"vcTable\":0,\"hdEnable\":true,\"sdEnable\":true,\"passthruEnable\":true},{\"transcoderID\":12,\"vcNumber\":0,\"vcTable\":0,\"hdEnable\":true,\"sdEnable\":true,\"passthruEnable\":true}]}";
//}
