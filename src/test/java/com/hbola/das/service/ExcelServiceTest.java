package com.hbola.das.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.AnnotationConfigWebContextLoader;
import org.springframework.test.context.web.WebAppConfiguration;

import com.hbola.das.config.TestSerivceConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)   
@WebAppConfiguration
@ContextConfiguration(loader = AnnotationConfigWebContextLoader.class, classes={TestSerivceConfiguration.class}) 
public class ExcelServiceTest {
	
	@Autowired
	ExcelServiceI excelService;
	
	@Test
	public void extract() throws Exception {//test is commented out becuase the the build on the build server will fail unless the xls file is in the right place. just uncomment out as needed for testing locally
	
//		String filePath = "C:/arris.xls";//this is in the docs folder in source control
//		FileInputStream fileInputStream = new FileInputStream(new File(filePath));
//		
//		List<Properties> list = excelService.extract(fileInputStream);
//		
//		assertNotNull(list);
//		assertEquals(9, list.size());
//		
//		assertEquals("2054447023000184", list.get(0).getProperty("serialNumber"));
//		assertEquals("0001141961776085", list.get(5).getProperty("unitAddress"));
//		assertEquals("0001141962832017", list.get(8).getProperty("unitAddress"));
		
		System.out.println("hi");
		
	}
	
}
