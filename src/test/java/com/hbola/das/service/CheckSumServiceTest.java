package com.hbola.das.service;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.AnnotationConfigWebContextLoader;
import org.springframework.test.context.web.WebAppConfiguration;

import com.hbola.das.config.TestSerivceConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)   
@WebAppConfiguration
@ContextConfiguration(loader = AnnotationConfigWebContextLoader.class, classes={TestSerivceConfiguration.class}) 
public class CheckSumServiceTest {

	@Autowired
	CheckSumServiceI checkSumService;
	
	@Test
	public void crc8() {
		
		String value = checkSumService.crc8(null);
		assertEquals("000", value);
		
		value = checkSumService.crc8("");
		assertEquals("000", value);
		
		value = checkSumService.crc8("junk");
		assertEquals("000", value);
		
		value = checkSumService.crc8("-1");
		assertEquals("163", value);
		
		value = checkSumService.crc8("0000316233301");
		assertEquals("110", value);
		
		value = checkSumService.crc8("0000316233302");
		assertEquals("088", value);
		
		value = checkSumService.crc8("0000316233303");
		assertEquals("195", value);
		
		value = checkSumService.crc8("0000316233304");
		assertEquals("045", value);
	}
	
}
