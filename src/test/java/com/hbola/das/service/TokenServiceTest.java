package com.hbola.das.service;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.AnnotationConfigWebContextLoader;
import org.springframework.test.context.web.WebAppConfiguration;

import com.hbola.das.config.TestSerivceConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)   
@WebAppConfiguration
@ContextConfiguration(loader = AnnotationConfigWebContextLoader.class, classes={TestSerivceConfiguration.class}) 
public class TokenServiceTest {

	@Autowired
	TokenServiceImpl tokenService;
	
	@Test
	public void generateToken() throws Exception {
		
		String token = tokenService.generateToken(null, null);
		assertTrue(token.startsWith("eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9."));
				
		token = tokenService.generateToken(null, "engineer");
		assertTrue(token.startsWith("eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9."));
				
		token = tokenService.generateToken("alex", null);
		assertTrue(token.startsWith("eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9."));
		
		token = tokenService.generateToken("alex", "engineer");
		assertTrue(token.contains("eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9."));		
	}
		
	@Test
	public void verify() throws Exception {
	
		String token = tokenService.generateToken("alex", "engineer");
		
		boolean isVerified = tokenService.verify(null);
		assertFalse(isVerified);
		
		isVerified = tokenService.verify("");
		assertFalse(isVerified);
		
		isVerified = tokenService.verify("junk!@#");
		assertFalse(isVerified);
		
		isVerified = tokenService.verify(token);
		assertTrue(isVerified);		
	}
	
	
}
