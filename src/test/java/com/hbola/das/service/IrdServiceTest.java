package com.hbola.das.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.multimap.ArrayListValuedHashMap;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.AnnotationConfigWebContextLoader;
import org.springframework.test.context.web.WebAppConfiguration;

import com.hbola.das.config.TestSerivceConfiguration;
import com.hbola.das.dto.IrdEditDTO;
import com.hbola.das.dto.IrdSummaryDTO;
import com.hbola.das.dto.OperationDTO;
import com.hbola.das.dto.ServiceDTO;
import com.hbola.das.dto.TranscoderDTO;

@RunWith(SpringJUnit4ClassRunner.class)   
@WebAppConfiguration
@ContextConfiguration(loader = AnnotationConfigWebContextLoader.class, classes={TestSerivceConfiguration.class}) 
public class IrdServiceTest { 


	@Autowired
	private IrdServiceI irdService;
	
	@Test 
	public void processACPsWithoutServices() {
		
		List<TranscoderDTO> transcoders = irdService.processACPsWithoutServices(0, null);
		assertTrue(transcoders == null);
		
		transcoders = new ArrayList<TranscoderDTO>();
		transcoders = irdService.processACPsWithoutServices(0, transcoders);
		assertTrue(transcoders.isEmpty());
		
		TranscoderDTO transcoder0 = new TranscoderDTO();
		transcoder0.setUnitAddress("1111111111111");
		transcoder0.setDecoderServicesIdentity(0);
		transcoders.add(transcoder0);		
		TranscoderDTO transcoder1 = new TranscoderDTO();
		transcoder1.setUnitAddress("1234567890123");
		transcoder1.setDecoderServicesIdentity(123);
		transcoders.add(transcoder1);
		TranscoderDTO transcoder2 = new TranscoderDTO();
		transcoder2.setUnitAddress("0987654321098");
		transcoder2.setDecoderServicesIdentity(124);
		transcoders.add(transcoder2);
		TranscoderDTO transcoder3 = new TranscoderDTO();
		transcoder3.setUnitAddress("2222222222222");
		transcoder3.setDecoderServicesIdentity(0);
		transcoders.add(transcoder3);
		TranscoderDTO transcoder4 = new TranscoderDTO();
		transcoder4.setUnitAddress("0987654321098");
		transcoder4.setDecoderServicesIdentity(124);
		transcoders.add(transcoder4);
		TranscoderDTO transcoder5 = new TranscoderDTO();
		transcoder5.setUnitAddress("3333333333333");
		transcoder5.setDecoderServicesIdentity(0);
		transcoders.add(transcoder5);

		
		transcoders = irdService.processACPsWithoutServices(0, transcoders);
		assertEquals(transcoders.get(0).getService().getTierName(), "Blackout");
		assertEquals(transcoders.get(0).getOperation().getId(), 0);
		assertEquals(transcoders.get(0).getUnitAddress(), "1111111111111");
		assertEquals(transcoders.get(1).getService(), null);
		assertEquals(transcoders.get(1).getUnitAddress(), "1234567890123");
		assertEquals(transcoders.get(3).getService().getTierName(), "Blackout");
		assertEquals(transcoders.get(0).getOperation().getId(), 0);
		assertEquals(transcoders.get(3).getUnitAddress(), "2222222222222");
		assertEquals(transcoders.get(4).getService(), null);
		assertEquals(transcoders.get(4).getUnitAddress(), "0987654321098");
		assertEquals(transcoders.get(5).getService().getTierName(), "Blackout");
		assertEquals(transcoders.get(0).getOperation().getId(), 0);
		assertEquals(transcoders.get(5).getUnitAddress(), "3333333333333");
		
	}
	
	@Test 
	public void setupMDUAsForMigrated() {
		
		List<TranscoderDTO> transcoders = irdService.setupMDUAsForMigrated(null);
		assertNotNull(transcoders);
		assertTrue(transcoders.isEmpty());
		
		IrdEditDTO dto = new IrdEditDTO();
		transcoders = irdService.setupMDUAsForMigrated(dto);
		assertNotNull(transcoders);
		assertTrue(transcoders.isEmpty());
		
		TranscoderDTO transcoderEmpty = new TranscoderDTO();
		transcoderEmpty.setUnitAddress(null);
		transcoders.add(transcoderEmpty);
		dto.setTranscoders(transcoders);
		transcoders = irdService.setupMDUAsForMigrated(dto);
		assertNotNull(transcoders);
		assertTrue(transcoders.isEmpty());
		
		transcoderEmpty = new TranscoderDTO();
		transcoderEmpty.setUnitAddress("");
		transcoders.add(transcoderEmpty);
		dto.setTranscoders(transcoders);
		transcoders = irdService.setupMDUAsForMigrated(dto);
		assertNotNull(transcoders);
		assertTrue(transcoders.isEmpty());
		
		transcoders.clear();
		dto.setIRTUnitAddress("0000316443824");
		TranscoderDTO transcoder1 = new TranscoderDTO();
		transcoder1.setUnitAddress("0000316443824");
		transcoder1.setDecoderServicesIdentity(123);
		transcoders.add(transcoder1);
		TranscoderDTO transcoder2 = new TranscoderDTO();
		transcoder2.setUnitAddress("0000316443826");
		transcoder2.setDecoderServicesIdentity(124);
		transcoders.add(transcoder2);
		TranscoderDTO transcoder3 = new TranscoderDTO();
		transcoder3.setUnitAddress("0000316443828");
		transcoder3.setDecoderServicesIdentity(125);
		transcoders.add(transcoder3);
		dto.setTranscoders(transcoders);		
		
		transcoders = irdService.setupMDUAsForMigrated(dto);
		assertTrue(transcoders.size() == 16);
		
		for (int i = 0; i < transcoders.size(); i++) {
			assertNotNull(transcoders.get(i));
			assertNotNull(transcoders.get(i).getUnitAddress());
			assertTrue(!transcoders.get(i).getUnitAddress().isEmpty());
		}
		
		assertTrue(transcoders.get(0).getUnitAddress().equalsIgnoreCase("0000316443824188"));
		assertEquals(123, transcoders.get(0).getDecoderServicesIdentity());
		
		assertTrue(transcoders.get(1).getUnitAddress().equalsIgnoreCase("0000316443825039"));
		assertEquals(0, transcoders.get(1).getDecoderServicesIdentity());
		
		assertTrue(transcoders.get(2).getUnitAddress().equalsIgnoreCase("0000316443826017"));
		assertEquals(124, transcoders.get(2).getDecoderServicesIdentity());
		
		assertTrue(transcoders.get(3).getUnitAddress().equalsIgnoreCase("0000316443827138"));
		assertEquals(0, transcoders.get(3).getDecoderServicesIdentity());
		
		assertTrue(transcoders.get(4).getUnitAddress().equalsIgnoreCase("0000316443828125"));
		assertEquals(125, transcoders.get(4).getDecoderServicesIdentity());

		assertTrue(transcoders.get(5).getUnitAddress().equalsIgnoreCase("0000316443829230"));
		assertEquals(0, transcoders.get(5).getDecoderServicesIdentity());

		assertTrue(transcoders.get(6).getUnitAddress().equalsIgnoreCase("0000316443830208"));
		assertEquals(0, transcoders.get(6).getDecoderServicesIdentity());

		assertTrue(transcoders.get(7).getUnitAddress().equalsIgnoreCase("0000316443831075"));
		assertEquals(0, transcoders.get(7).getDecoderServicesIdentity());

		assertTrue(transcoders.get(8).getUnitAddress().equalsIgnoreCase("0000316443832165"));
		assertEquals(0, transcoders.get(8).getDecoderServicesIdentity());
		
		assertTrue(transcoders.get(9).getUnitAddress().equalsIgnoreCase("0000316443833062"));
		assertEquals(0, transcoders.get(9).getDecoderServicesIdentity());

		assertTrue(transcoders.get(10).getUnitAddress().equalsIgnoreCase("0000316443834008"));
		assertEquals(0, transcoders.get(10).getDecoderServicesIdentity());

		assertTrue(transcoders.get(11).getUnitAddress().equalsIgnoreCase("0000316443835147"));
		assertEquals(0, transcoders.get(11).getDecoderServicesIdentity());

		assertTrue(transcoders.get(12).getUnitAddress().equalsIgnoreCase("0000316443836100"));
		assertEquals(0, transcoders.get(12).getDecoderServicesIdentity());

		assertTrue(transcoders.get(13).getUnitAddress().equalsIgnoreCase("0000316443837255"));
		assertEquals(0, transcoders.get(13).getDecoderServicesIdentity());

		assertTrue(transcoders.get(14).getUnitAddress().equalsIgnoreCase("0000316443838201"));
		assertEquals(0, transcoders.get(14).getDecoderServicesIdentity());

		assertTrue(transcoders.get(15).getUnitAddress().equalsIgnoreCase("0000316443839082"));
		assertEquals(0, transcoders.get(15).getDecoderServicesIdentity());
		
		
		//check if this will automagically add the apc0 !!!
		transcoders.clear();
		dto.setIRTUnitAddress(null);
		dto.setUnitAddress("0000316440001");
		transcoder1 = new TranscoderDTO();
		transcoder1.setUnitAddress("0000316440002");
		transcoder1.setDecoderServicesIdentity(123);
		transcoders.add(transcoder1);
		transcoder2 = new TranscoderDTO();
		transcoder2.setUnitAddress("0000316440010");
		transcoder2.setDecoderServicesIdentity(124);
		transcoders.add(transcoder2);
		transcoder3 = new TranscoderDTO();
		transcoder3.setUnitAddress("0000316440016");
		transcoder3.setDecoderServicesIdentity(125);
		transcoders.add(transcoder3);
		dto.setTranscoders(transcoders);		
		
		transcoders = irdService.setupMDUAsForMigrated(dto);
		assertTrue(transcoders.size() == 16);
		
		for (int i = 0; i < transcoders.size(); i++) {
			assertNotNull(transcoders.get(i));
			assertNotNull(transcoders.get(i).getUnitAddress());
			assertTrue(!transcoders.get(i).getUnitAddress().isEmpty());
		}
		
		assertTrue(transcoders.get(0).getUnitAddress().equalsIgnoreCase("0000316440001204"));
		assertEquals(0, transcoders.get(0).getDecoderServicesIdentity());
		
		assertTrue(transcoders.get(1).getUnitAddress().equalsIgnoreCase("0000316440002250"));
		assertEquals(123, transcoders.get(1).getDecoderServicesIdentity());
		
		assertTrue(transcoders.get(2).getUnitAddress().equalsIgnoreCase("0000316440003097"));
		assertEquals(0, transcoders.get(2).getDecoderServicesIdentity());
		
		assertTrue(transcoders.get(3).getUnitAddress().equalsIgnoreCase("0000316440004150"));
		assertEquals(0, transcoders.get(3).getDecoderServicesIdentity());
		
		assertTrue(transcoders.get(4).getUnitAddress().equalsIgnoreCase("0000316440005013"));
		assertEquals(0, transcoders.get(4).getDecoderServicesIdentity());

		assertTrue(transcoders.get(5).getUnitAddress().equalsIgnoreCase("0000316440006059"));
		assertEquals(0, transcoders.get(5).getDecoderServicesIdentity());

		assertTrue(transcoders.get(6).getUnitAddress().equalsIgnoreCase("0000316440007160"));
		assertEquals(0, transcoders.get(6).getDecoderServicesIdentity());

		assertTrue(transcoders.get(7).getUnitAddress().equalsIgnoreCase("0000316440008078"));
		assertEquals(0, transcoders.get(7).getDecoderServicesIdentity());

		assertTrue(transcoders.get(8).getUnitAddress().equalsIgnoreCase("0000316440009213"));
		assertEquals(0, transcoders.get(8).getDecoderServicesIdentity());
		
		assertTrue(transcoders.get(9).getUnitAddress().equalsIgnoreCase("0000316440010227"));
		assertEquals(124, transcoders.get(9).getDecoderServicesIdentity());

		assertTrue(transcoders.get(10).getUnitAddress().equalsIgnoreCase("0000316440011120"));
		assertEquals(0, transcoders.get(10).getDecoderServicesIdentity());

		assertTrue(transcoders.get(11).getUnitAddress().equalsIgnoreCase("0000316440012143"));
		assertEquals(0, transcoders.get(11).getDecoderServicesIdentity());

		assertTrue(transcoders.get(12).getUnitAddress().equalsIgnoreCase("0000316440013020"));
		assertEquals(0, transcoders.get(12).getDecoderServicesIdentity());

		assertTrue(transcoders.get(13).getUnitAddress().equalsIgnoreCase("0000316440014034"));
		assertEquals(0, transcoders.get(13).getDecoderServicesIdentity());

		assertTrue(transcoders.get(14).getUnitAddress().equalsIgnoreCase("0000316440015185"));
		assertEquals(0, transcoders.get(14).getDecoderServicesIdentity());

		assertTrue(transcoders.get(15).getUnitAddress().equalsIgnoreCase("0000316440016101"));
		assertEquals(125, transcoders.get(15).getDecoderServicesIdentity());		
	}
	
	@Test
	public void setupMDUAs() {
		
		String[] unitAddresses = irdService.setupMDUAs(null);
		assertNotNull(unitAddresses);
		assertTrue(unitAddresses.length == 0);
		
		unitAddresses = irdService.setupMDUAs("");
		assertNotNull(unitAddresses);
		assertTrue(unitAddresses.length == 0);
		
		unitAddresses = irdService.setupMDUAs("junk");
		assertNotNull(unitAddresses);
		assertTrue(unitAddresses.length == 0);
		
		unitAddresses = irdService.setupMDUAs("000-03162-33301");
		assertNotNull(unitAddresses);
		assertTrue(unitAddresses.length == 16);
		assertEquals("000-03162-33301-110", unitAddresses[0]);
		assertEquals("000-03162-33302-088", unitAddresses[1]);
		assertEquals("000-03162-33303-195", unitAddresses[2]);
		assertEquals("000-03162-33304-045", unitAddresses[3]);
		assertEquals("000-03162-33305-182", unitAddresses[4]);
		assertEquals("000-03162-33306-128", unitAddresses[5]);
		assertEquals("000-03162-33307-027", unitAddresses[6]);
		assertEquals("000-03162-33308-236", unitAddresses[7]);
		assertEquals("000-03162-33309-119", unitAddresses[8]);
		assertEquals("000-03162-33310-065", unitAddresses[9]);
		assertEquals("000-03162-33311-218", unitAddresses[10]);
		assertEquals("000-03162-33312-098", unitAddresses[11]);
		assertEquals("000-03162-33313-249", unitAddresses[12]);
		assertEquals("000-03162-33314-207", unitAddresses[13]);
		assertEquals("000-03162-33315-084", unitAddresses[14]);
		assertEquals("000-03162-33316-163", unitAddresses[15]);
		
		unitAddresses = irdService.setupMDUAs("000-03162-33301-123");
		assertEquals("000-03162-33301-110", unitAddresses[0]);
		assertEquals("000-03162-33302-088", unitAddresses[1]);
		assertEquals("000-03162-33303-195", unitAddresses[2]);
		assertEquals("000-03162-33304-045", unitAddresses[3]);
		assertEquals("000-03162-33305-182", unitAddresses[4]);
		assertEquals("000-03162-33306-128", unitAddresses[5]);
		assertEquals("000-03162-33307-027", unitAddresses[6]);
		assertEquals("000-03162-33308-236", unitAddresses[7]);
		assertEquals("000-03162-33309-119", unitAddresses[8]);
		assertEquals("000-03162-33310-065", unitAddresses[9]);
		assertEquals("000-03162-33311-218", unitAddresses[10]);
		assertEquals("000-03162-33312-098", unitAddresses[11]);
		assertEquals("000-03162-33313-249", unitAddresses[12]);
		assertEquals("000-03162-33314-207", unitAddresses[13]);
		assertEquals("000-03162-33315-084", unitAddresses[14]);
		assertEquals("000-03162-33316-163", unitAddresses[15]);

		unitAddresses = irdService.setupMDUAs("0000316443824");
		assertEquals("000-03164-43824-188", unitAddresses[0]);
		assertEquals("000-03164-43825-039", unitAddresses[1]);
		assertEquals("000-03164-43826-017", unitAddresses[2]);
		assertEquals("000-03164-43827-138", unitAddresses[3]);
		assertEquals("000-03164-43828-125", unitAddresses[4]);
		assertEquals("000-03164-43829-230", unitAddresses[5]);
		assertEquals("000-03164-43830-208", unitAddresses[6]);
		assertEquals("000-03164-43831-075", unitAddresses[7]);
		assertEquals("000-03164-43832-165", unitAddresses[8]);
		assertEquals("000-03164-43833-062", unitAddresses[9]);
		assertEquals("000-03164-43834-008", unitAddresses[10]);
		assertEquals("000-03164-43835-147", unitAddresses[11]);
		assertEquals("000-03164-43836-100", unitAddresses[12]);
		assertEquals("000-03164-43837-255", unitAddresses[13]);
		assertEquals("000-03164-43838-201", unitAddresses[14]);
		assertEquals("000-03164-43839-082", unitAddresses[15]);
		
		unitAddresses = irdService.setupMDUAs("0001141951503");
		for (int i = 0; i < unitAddresses.length; i++) {
			System.out.println(unitAddresses[i]);
		}
	}
	
	@Test
	public void insertTranscoderBlackouts() {
		
		//test
		List<TranscoderDTO> results = irdService.insertTranscoderBlackouts(null);
		assertNotNull(results);
		assertTrue(results.isEmpty());
		
		//test
		List<TranscoderDTO> list0 = new ArrayList<TranscoderDTO>();
		list0.add(null);
		list0.add(null);
		results = irdService.insertTranscoderBlackouts(null);
		assertNotNull(results);
		assertTrue(results.isEmpty());
		
		//test
		TranscoderDTO dto = new TranscoderDTO(1, null, null, null, false, false, false, 0, false, "123");
		TranscoderDTO dto1 = new TranscoderDTO(2, null, null, null, false, false, false, 0, false, "456");
		TranscoderDTO dto2 = new TranscoderDTO(4, null, null, null, false, false, false, 0, false, "789");
		
		List<TranscoderDTO> list1 = new ArrayList<TranscoderDTO>();
		list1.add(dto);
		list1.add(dto1);
		list1.add(dto2);
		
		results = irdService.insertTranscoderBlackouts(list1);
		assertEquals(4, results.size());
		TranscoderDTO result = results.get(0);
		assertEquals(1, result.getTranscoderId());
		
		result = results.get(1);
		assertEquals(2, result.getTranscoderId());
		
		result = results.get(2);
		assertEquals(3, result.getTranscoderId());
		assertEquals("Blackout", result.getService().getTierName());
		
		result = results.get(3);
		assertEquals(4, result.getTranscoderId());
		
		//test
		TranscoderDTO dto3 = new TranscoderDTO(7, null, null, null, false, false, false, 0, false, "789");
		List<TranscoderDTO> list2 = new ArrayList<TranscoderDTO>();
		list2.add(dto);
		list2.add(dto1);
		list2.add(dto2);
		list2.add(dto3);
		
		results = irdService.insertTranscoderBlackouts(list2);
		assertEquals(7, results.size());
		
		result = results.get(0);
		assertEquals(1, result.getTranscoderId());
		
		result = results.get(1);
		assertEquals(2, result.getTranscoderId());
		
		result = results.get(2);
		assertEquals(3, result.getTranscoderId());
		assertEquals("Blackout", result.getService().getTierName());
		
		result = results.get(3);
		assertEquals(4, result.getTranscoderId());
		
		result = results.get(4);
		assertEquals(5, result.getTranscoderId());
		assertEquals("Blackout", result.getService().getTierName());
		
		result = results.get(5);
		assertEquals(6, result.getTranscoderId());
		assertEquals("Blackout", result.getService().getTierName());
		
		result = results.get(6);
		assertEquals(7, result.getTranscoderId());	
	}
	
	@Test
	public void removeTranscoderBlackouts() {
		
		//test
		List<TranscoderDTO> resultList = irdService.removeTranscoderBlackout(null);
		assertNotNull(resultList);
		assertTrue(resultList.isEmpty());
		
		//test
		List<TranscoderDTO> nullElesList = new ArrayList<TranscoderDTO>();
		nullElesList.add(null);
		nullElesList.add(null);
		resultList = irdService.removeTranscoderBlackout(nullElesList);
		assertNotNull(resultList);
		assertEquals(null, resultList.get(0));
		assertEquals(null, resultList.get(1));
		
		//test
		List<TranscoderDTO> list = new ArrayList<TranscoderDTO>();
		list.add(new TranscoderDTO(1, 1, null, null, null, false, false, false, 0, false, "123"));
		list.add(new TranscoderDTO(1, 2, null, null, null, false, false, false, 0, false, "456"));
		list.add(irdService.getBlackoutTranscoder(3));
		list.add(new TranscoderDTO(1, 4, null, null, null, false, false, false, 0, false, "789"));
		list.add(irdService.getBlackoutTranscoder(5));
		list.add(irdService.getBlackoutTranscoder(6));
		list.add(new TranscoderDTO(1, 7, null, null, null, false, false, false, 0, false, "789"));
		
		resultList = irdService.removeTranscoderBlackout(list);
		assertNotNull(resultList);
		assertEquals(4, resultList.size());
		
		TranscoderDTO dto = resultList.get(0);
		assertEquals(1, dto.getTranscoderId());
		
		dto = resultList.get(1);
		assertEquals(2, dto.getTranscoderId());
		
		dto = resultList.get(2);
		assertEquals(4, dto.getTranscoderId());
		
		dto = resultList.get(3);
		assertEquals(7, dto.getTranscoderId());
		
		//test
		List<TranscoderDTO> list3 = new ArrayList<TranscoderDTO>();
		list3.add(irdService.getBlackoutTranscoder(1));
		list3.add(irdService.getBlackoutTranscoder(2));
		list3.add(irdService.getBlackoutTranscoder(3));
		resultList = irdService.removeTranscoderBlackout(list3);
		assertNotNull(resultList);
		assertTrue(resultList.isEmpty());
		
	}
	
	@Test
	public void addTranscodersToIrds() {
		
		List<IrdSummaryDTO> newIrds = irdService.addTranscodersToIrdsWithUnitaddresses(null);
		assertNotNull(newIrds);
		assertTrue(newIrds.isEmpty());
		
		Map<String, Object> map = new HashMap<String, Object>();
		newIrds = irdService.addTranscodersToIrdsWithUnitaddresses(map);
		assertTrue(newIrds.isEmpty());
		
		List<String> unitAddresses = new ArrayList<String>();
    	unitAddresses.add("UnitAddress");
    	unitAddresses.add("SecondaryUnitAddress");
		
    	int decoderId = 11111;
    	
    	List<IrdSummaryDTO> irds = new ArrayList<IrdSummaryDTO>();
		irds.add(new IrdSummaryDTO(decoderId,
				"ModelID", 
				"DecoSerial",
				unitAddresses, 
				"CableCode - CableName", 
				"Location", 
				"CountryName", 
				"EncoderName", 
				false, 
				0, 
				"OwnershipName", 
				/*new HashMap<String, String>(),*/new ArrayList<String>(),
				new ArrayList<TranscoderDTO>()));
		
		MultiValuedMap<String, TranscoderDTO> tdtoMap = new ArrayListValuedHashMap<>();
		
		
		List<TranscoderDTO> transcoders = new ArrayList<TranscoderDTO>();
		
		TranscoderDTO tdto = new TranscoderDTO(decoderId, 1, 
										new ServiceDTO(10, "TierNumber", "TierName", "", 0), 
										"Comments: this is transcoder1", 
										new OperationDTO(10, "OperationName", true, "", ""),
										true, false, false, 1, true, "UnitAddress");
		
		TranscoderDTO tdto1 = new TranscoderDTO(decoderId, 2, 
										new ServiceDTO(10, "TierNumber", "TierName", "", 0), 
										"Comments: this is transcoder2", 
										new OperationDTO(10, "OperationName", true, "", ""),
										true, false, false, 2, true, "UnitAddress");	
		
		tdtoMap.put(Integer.toString(decoderId), tdto);
		tdtoMap.put(Integer.toString(decoderId), tdto1);
		
		
		map.put("irds", irds);
        map.put("transcodersMap", tdtoMap);
		
		newIrds = irdService.addTranscodersToIrdsWithUnitaddresses(map);
		
		assertNotNull(newIrds);
		assertTrue(!newIrds.isEmpty());
		
		IrdSummaryDTO dto = newIrds.get(0);
		List<TranscoderDTO> newTranscoders = dto.getTranscoders();
		
		assertNotNull(newTranscoders);
		assertTrue(!newTranscoders.isEmpty());
		
		TranscoderDTO tDTO = newTranscoders.get(0);
		assertEquals("Comments: this is transcoder1", tDTO.getComments());
		
		tDTO = newTranscoders.get(1);
		assertEquals("Comments: this is transcoder2", tDTO.getComments());
		
		
		
	}
	
}